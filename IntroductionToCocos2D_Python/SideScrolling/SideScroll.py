######################################################################
## A Cocos2D version of the "Think Like a Programmer" Chapter 17
## Pygame tutorial at
## http://openbookproject.net/thinkcs/python/english3e/pygame.html
##
## Conversion to Cocos2D and additional explanation and annotations
## by Erik M. Buck 2016
######################################################################

# Begin by importing the cocos package:
from cocos import *
from cocos.layer import *
from cocos.director import *
from cocos.actions import *
from cocos.sprite import *
import pyglet


windowWidth = 1024
windowHeight = 768
gravity = -0.1       # Acceleration per update interval

# Create a window. To do this, we initialize the Director:
window = director.init(
   windowWidth,
   windowHeight,
   caption="Endless Scroll")


# load images with pyglet
background_image = pyglet.resource.image('jungle_panorama.png')

background_layer = ScrollableLayer()
background_sprite = Sprite(background_image)
background_sprite.position = ((background_image.width  - windowWidth) * 0.5, 0)
restart_threshold = background_image.width - windowWidth

print(background_image.width)

background_layer.add(background_sprite)

scroller = ScrollingManager()
scroller.add(background_layer)

class SideScrollAction(Action):
    """ 
    This class exists to forward the step(dt) method call to the 
    receiver's target object. It is a hook that enables targets to
    perform logic each time the display is updated.
    """
    def start(self):
        self.x = 0
    
    def step(self, dt):
        """ """
        self.target.set_focus(self.x, 0)
        self.x += 0.5
        self.x = self.x % restart_threshold


scroller.do(SideScrollAction())

# Then we create an Scene that contains the HelloWorld layer as a child:
main_scene = scene.Scene (scroller)

# And finally we run the scene:
director.run (main_scene)
