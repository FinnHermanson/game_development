from cocos import *
from cocos.layer import *
from cocos.director import *
from cocos.actions import *
from cocos.sprite import *
import pyglet
import random

# Create a window. To do this, we initialize the Director:
window = director.init(
   1024,
   768,
   caption="Whack-A-Duke")

small_image = pyglet.resource.image('explosionSmall.png')
small_grid = pyglet.image.ImageGrid(small_image, 5, 5)
small_textures = pyglet.image.TextureGrid(small_grid)
small_textures_list = small_textures[:]
frame_period = 0.05
small_animation = pyglet.image.Animation.from_image_sequence(
    small_textures_list, frame_period, loop=False)
duration = len(small_textures_list) * frame_period
default_opacity = 128

duke_spritesheet_image = pyglet.resource.image('duke_spritesheet.png')

# use ImageGrid to divide your sprite sheet into smaller regions
grid = pyglet.image.ImageGrid(duke_spritesheet_image, 1, 10)

# convert to TextureGrid for memory efficiency
textures = pyglet.image.TextureGrid(grid)
textures_list = textures[:]
textures_list.append(textures_list[0]) # duplicate first frame

# create pyglet animation objects from the sequences
duke_animation = pyglet.image.Animation.from_image_sequence(
   textures_list, 0.075, loop=True)

class TouchSprite(Sprite):

    def __init__(self, image):
        super( TouchSprite, self ).__init__(image)

        window.push_handlers(self.on_mouse_press)

    def does_contain_point(self, pos):
        return (
           (abs(pos[0] - self.position[0]) < self.image.width/2) and
           (abs(pos[1] - self.position[1]) < self.image.width/2))

    def on_processed_touch(self, x, y, buttons, modifiers):
        pass
    
    def on_mouse_press(self, x, y, buttons, modifiers):
        px, py = director.get_virtual_coordinates (x, y)
        if self.does_contain_point((px, py)):
            self.on_processed_touch(px, py, buttons, modifiers)

class DukeSprite(TouchSprite):

    def does_contain_point(self, pos):
        return (
           (abs(pos[0] - self.position[0]) < 35) and
           (abs(pos[1] - self.position[1]) < 50))
    
    def on_processed_touch(self, x, y, buttons, modifiers):
        self.image = small_animation
        self.do(Delay(duration) + CallFuncS(DukeSprite.kill))

class GameLayerAction(Action):
   def step(self, dt):
        """ """
        dukes = self.target.get_children()
        if 0 == len(dukes):
            self.target.addDukes()

class GameLayer(Layer):
    def __init__(self):
        super( GameLayer, self ).__init__()
        self.addDukes()
        self.do(GameLayerAction())

    def addDukes(self):
        for i in range(0,10):
            posX = random.random() * 1000
            posY = random.random() * 750
            new_duke = DukeSprite(duke_animation)
            new_duke.position = (posX, posY)
            self.add(new_duke)

game_layer = GameLayer ()
main_scene = scene.Scene (game_layer)
director.run (main_scene)
