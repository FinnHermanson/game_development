######################################################################
## A version of "hello_world.py" copy/pasted from online tutorial:
## http://python.cocos2d.org/doc/programming_guide/quickstart.html#hello-world
##
## Additional explanation and annotations by Erik M. Buck 2016
######################################################################

# Begin by importing the cocos package:
import cocos

# We need to initialize and create a window. To do this, we initialize
# the Director:
cocos.director.director.init()

# Create a label to display text in a fancy font

# Notes:A Layer - A memory buffer
#           This layer is just a text rendered into an image sent to the GPU
#       If statements kill memory
label = cocos.text.Label('Hello, world',
                 font_name='Times New Roman',
                 font_size=32)

# The label position will be the center of a 640 x 480 window:
label.position = 320,240

# Then we create an Scene that contains the HelloWorld layer as a child:
main_scene = cocos.scene.Scene (label)

#label.do(cocos.actions.MoveBy((-320,-240), 2.5))
#label.do(cocos.actions.RotateBy(360.0,2.5))
motion = cocos.actions.RotateBy(360.0, 2.5) + cocos.actions.RotateBy(-360.0, 2.5)
label.do(motion)

# And finally we run the scene:
cocos.director.director.run (main_scene)
