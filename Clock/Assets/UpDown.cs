﻿using UnityEngine;
using System.Collections;

public class UpDown : MonoBehaviour {
	public float angularOffset;

	float floorAngle;

	// Use this for initialization
	void Start () {
		floorAngle = 0;
	}

	// Update is called once per frame
	void Update () {
		Vector3 position = this.transform.localPosition;
		float angleRad = Mathf.Deg2Rad * (floorAngle + angularOffset);
		position.y = Mathf.Sin(angleRad);
		this.transform.localPosition = position;
		floorAngle += 2f;
	}
}
