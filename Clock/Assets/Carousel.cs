﻿using UnityEngine;
using System.Collections;

public class Carousel : MonoBehaviour {

	public Transform floor;

	float floorAngle;

	// Use this for initialization
	void Start () {
		floorAngle = 0;
	}
	
	// Update is called once per frame
	void Update () {
		floor.localRotation = Quaternion.Euler(0f, floorAngle, 0f);
		floorAngle += 0.5f;
	}
}
