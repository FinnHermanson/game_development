﻿using UnityEngine;

// Include the namespace required to use Unity UI
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class PlayerController : MonoBehaviour {

	public float speed;
	public Text countText;

	private Rigidbody rb;
	private int count;
	private string[] messages;

	public ParticleSystem BigExplosion;

	// At the start of the game..
	void Start (){
		rb = GetComponent<Rigidbody>();

		// Set the count to zero
		count = 0;
		messages = new string[10]{"Let your mind rest.", "Don't worry, nothing will blow up.", "This is supposed to be calming game.",
								  "Is there even a point to this game?","Is there an end?", 
								  "Who would waste time coding this?", "It's such a simple game", "Maybe change which order you get crowns", 
								  "You should quit this game.", "Why are the collectibles crowns?"};
	
		SetMessageText("Relax and collect");
	
	}


    // Each physics step..
    void FixedUpdate (){
		float moveHorizontal = Input.GetAxis("Horizontal");
		float moveVertical = Input.GetAxis("Vertical");

		Vector3 movement = new Vector3 (moveHorizontal, 0.0f, moveVertical);
		rb.AddForce (movement * speed);
	}


	void OnTriggerEnter(Collider other){
		if (other.gameObject.CompareTag("Pick Up")){
			other.gameObject.SetActive(false);
			count = count + 1;
			SetMessageText(messages[Random.Range(0, 10)]);
		}
	}

	void SetMessageText(string message){
		countText.text = message;
		if (count == 8){
			if (Random.Range(0,7) == 5){
				Instantiate(BigExplosion, transform.TransformPoint(0, -1, 0), BigExplosion.transform.rotation);
				countText.text = "Hmm, I guess you messed up. Game Over.";				
			}
			else {
				countText.text = "Relax and collect";
				SceneManager.LoadScene(SceneManager.GetActiveScene().name, LoadSceneMode.Single);
			}
		}
	}

}
