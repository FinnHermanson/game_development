﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Readme/Section[]
struct SectionU5BU5D_t3EF1EA79AB7297D95C599BBA1E7D1DF5D69B5364;
// System.Char[]
struct CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t4A961510635950236678F1B3B2436ECCAF28713A;
// System.Collections.Generic.List`1<TMPro.KerningPair>
struct List_1_tA4D79745FA03271B85D7DCF36F41C845E40F97A8;
// System.Func`2<TMPro.KerningPair,System.UInt32>
struct Func_2_t91771320D5745AD965BB19871562F93652AE024F;
// System.Int32[]
struct Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074;
// System.Single[]
struct SingleU5BU5D_t8C7CE2FD0C4CD3846DEB3BE6DD5564E32A8A27D5;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B;
// System.Void
struct Void_tDB81A15FA2AB53E2401A76B745D215397B29F783;
// TMPro.FastAction
struct FastAction_t270E4D4A1102DD4C6E4D0634A4CBBD052A5D91FB;
// TMPro.FastAction`1<System.Boolean>
struct FastAction_1_tCACDB1FC73FEE322BC04133C55C45D20EBBA8C8E;
// TMPro.FastAction`1<TMPro.TMP_ColorGradient>
struct FastAction_1_t1380158AFE3DBBC197AFE5E2C45A2B50E03B25F9;
// TMPro.FastAction`1<UnityEngine.Object>
struct FastAction_1_t4B3FBD9A28CE7B1C5AB15A8A9087AE92B7A28E09;
// TMPro.FastAction`2<System.Boolean,TMPro.TMP_FontAsset>
struct FastAction_2_tF681F2B4D7F3C436D0CF8815F6CDFDE0ADA898BD;
// TMPro.FastAction`2<System.Boolean,TMPro.TextMeshPro>
struct FastAction_2_tEB3735AAFBD827896E679E1D4C16C43B5BADAE54;
// TMPro.FastAction`2<System.Boolean,TMPro.TextMeshProUGUI>
struct FastAction_2_t7837C9EFA532E6A13072139CAC9CF3412D959945;
// TMPro.FastAction`2<System.Boolean,UnityEngine.Material>
struct FastAction_2_tA69AED04E5299BAE9D8184243189E851F4080707;
// TMPro.FastAction`2<System.Boolean,UnityEngine.Object>
struct FastAction_2_tF0254094C7F4F29971E256E3E0D897BF5EEA3A92;
// TMPro.FastAction`2<System.Object,TMPro.Compute_DT_EventArgs>
struct FastAction_2_tDD9D6A1F9834B71AC977D63B59F88637BC645E60;
// TMPro.FastAction`3<UnityEngine.GameObject,UnityEngine.Material,UnityEngine.Material>
struct FastAction_3_t2EB8ADF78F86E27B29B67AA5A8A75B91C797D3D4;
// TMPro.MaterialReference[]
struct MaterialReferenceU5BU5D_t01EC9C1C00A504C2EF9FBAF95DE26BB88E9B743B;
// TMPro.TMP_ColorGradient
struct TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7;
// TMPro.TMP_ColorGradient[]
struct TMP_ColorGradientU5BU5D_t0948D618AC4240E6F0CFE0125BB6A4E931DE847C;
// TMPro.TMP_FontAsset
struct TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C;
// TMPro.TMP_SpriteAsset
struct TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487;
// TMPro.TMP_Text
struct TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7;
// TMPro.TMP_TextElement
struct TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344;
// TMPro.TMP_TextInfo
struct TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181;
// TMPro.TextAlignmentOptions[]
struct TextAlignmentOptionsU5BU5D_t4AE8FA5E3D695ED64EBBCFBAF8C780A0EB0BD33B;
// TMPro.TextMeshProUGUI
struct TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438;
// UnityEngine.AnimationCurve
struct AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C;
// UnityEngine.CanvasGroup
struct CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90;
// UnityEngine.CharacterController
struct CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E;
// UnityEngine.Color32[]
struct Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983;
// UnityEngine.Color[]
struct ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.Gradient
struct Gradient_t35A694DDA1066524440E325E582B01E33DE66A3A;
// UnityEngine.Material
struct Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598;
// UnityEngine.ParticleSystem
struct ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D;
// UnityEngine.Renderer
struct Renderer_t0556D67DD582620D1F495627EDE30D03284151F4;
// UnityEngine.Rigidbody
struct Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5;
// UnityEngine.Texture2D
struct Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.UI.Text
struct Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030;
// UnityTemplateProjects.SimpleCameraController/CameraState
struct CameraState_t67204999380EA316F8E1B6C902D4330646DE6AE7;




#ifndef U3CMODULEU3E_T6CDDDF959E7E18A6744E43B613F41CDAC780256A_H
#define U3CMODULEU3E_T6CDDDF959E7E18A6744E43B613F41CDAC780256A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t6CDDDF959E7E18A6744E43B613F41CDAC780256A 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T6CDDDF959E7E18A6744E43B613F41CDAC780256A_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef SECTION_TDCE8FED365051651AC2BFB27F2230DDC5603C985_H
#define SECTION_TDCE8FED365051651AC2BFB27F2230DDC5603C985_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Readme/Section
struct  Section_tDCE8FED365051651AC2BFB27F2230DDC5603C985  : public RuntimeObject
{
public:
	// System.String Readme/Section::heading
	String_t* ___heading_0;
	// System.String Readme/Section::text
	String_t* ___text_1;
	// System.String Readme/Section::linkText
	String_t* ___linkText_2;
	// System.String Readme/Section::url
	String_t* ___url_3;

public:
	inline static int32_t get_offset_of_heading_0() { return static_cast<int32_t>(offsetof(Section_tDCE8FED365051651AC2BFB27F2230DDC5603C985, ___heading_0)); }
	inline String_t* get_heading_0() const { return ___heading_0; }
	inline String_t** get_address_of_heading_0() { return &___heading_0; }
	inline void set_heading_0(String_t* value)
	{
		___heading_0 = value;
		Il2CppCodeGenWriteBarrier((&___heading_0), value);
	}

	inline static int32_t get_offset_of_text_1() { return static_cast<int32_t>(offsetof(Section_tDCE8FED365051651AC2BFB27F2230DDC5603C985, ___text_1)); }
	inline String_t* get_text_1() const { return ___text_1; }
	inline String_t** get_address_of_text_1() { return &___text_1; }
	inline void set_text_1(String_t* value)
	{
		___text_1 = value;
		Il2CppCodeGenWriteBarrier((&___text_1), value);
	}

	inline static int32_t get_offset_of_linkText_2() { return static_cast<int32_t>(offsetof(Section_tDCE8FED365051651AC2BFB27F2230DDC5603C985, ___linkText_2)); }
	inline String_t* get_linkText_2() const { return ___linkText_2; }
	inline String_t** get_address_of_linkText_2() { return &___linkText_2; }
	inline void set_linkText_2(String_t* value)
	{
		___linkText_2 = value;
		Il2CppCodeGenWriteBarrier((&___linkText_2), value);
	}

	inline static int32_t get_offset_of_url_3() { return static_cast<int32_t>(offsetof(Section_tDCE8FED365051651AC2BFB27F2230DDC5603C985, ___url_3)); }
	inline String_t* get_url_3() const { return ___url_3; }
	inline String_t** get_address_of_url_3() { return &___url_3; }
	inline void set_url_3(String_t* value)
	{
		___url_3 = value;
		Il2CppCodeGenWriteBarrier((&___url_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECTION_TDCE8FED365051651AC2BFB27F2230DDC5603C985_H
#ifndef VALUETYPE_T1810BD84E0FDB5D3A7CD34286A5B22F343995C9C_H
#define VALUETYPE_T1810BD84E0FDB5D3A7CD34286A5B22F343995C9C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t1810BD84E0FDB5D3A7CD34286A5B22F343995C9C  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t1810BD84E0FDB5D3A7CD34286A5B22F343995C9C_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t1810BD84E0FDB5D3A7CD34286A5B22F343995C9C_marshaled_com
{
};
#endif // VALUETYPE_T1810BD84E0FDB5D3A7CD34286A5B22F343995C9C_H
#ifndef FACEINFO_T43812B1A171B1AE8E3591EADED098DE81264F6A5_H
#define FACEINFO_T43812B1A171B1AE8E3591EADED098DE81264F6A5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.FaceInfo
struct  FaceInfo_t43812B1A171B1AE8E3591EADED098DE81264F6A5  : public RuntimeObject
{
public:
	// System.String TMPro.FaceInfo::Name
	String_t* ___Name_0;
	// System.Single TMPro.FaceInfo::PointSize
	float ___PointSize_1;
	// System.Single TMPro.FaceInfo::Scale
	float ___Scale_2;
	// System.Int32 TMPro.FaceInfo::CharacterCount
	int32_t ___CharacterCount_3;
	// System.Single TMPro.FaceInfo::LineHeight
	float ___LineHeight_4;
	// System.Single TMPro.FaceInfo::Baseline
	float ___Baseline_5;
	// System.Single TMPro.FaceInfo::Ascender
	float ___Ascender_6;
	// System.Single TMPro.FaceInfo::CapHeight
	float ___CapHeight_7;
	// System.Single TMPro.FaceInfo::Descender
	float ___Descender_8;
	// System.Single TMPro.FaceInfo::CenterLine
	float ___CenterLine_9;
	// System.Single TMPro.FaceInfo::SuperscriptOffset
	float ___SuperscriptOffset_10;
	// System.Single TMPro.FaceInfo::SubscriptOffset
	float ___SubscriptOffset_11;
	// System.Single TMPro.FaceInfo::SubSize
	float ___SubSize_12;
	// System.Single TMPro.FaceInfo::Underline
	float ___Underline_13;
	// System.Single TMPro.FaceInfo::UnderlineThickness
	float ___UnderlineThickness_14;
	// System.Single TMPro.FaceInfo::strikethrough
	float ___strikethrough_15;
	// System.Single TMPro.FaceInfo::strikethroughThickness
	float ___strikethroughThickness_16;
	// System.Single TMPro.FaceInfo::TabWidth
	float ___TabWidth_17;
	// System.Single TMPro.FaceInfo::Padding
	float ___Padding_18;
	// System.Single TMPro.FaceInfo::AtlasWidth
	float ___AtlasWidth_19;
	// System.Single TMPro.FaceInfo::AtlasHeight
	float ___AtlasHeight_20;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(FaceInfo_t43812B1A171B1AE8E3591EADED098DE81264F6A5, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_PointSize_1() { return static_cast<int32_t>(offsetof(FaceInfo_t43812B1A171B1AE8E3591EADED098DE81264F6A5, ___PointSize_1)); }
	inline float get_PointSize_1() const { return ___PointSize_1; }
	inline float* get_address_of_PointSize_1() { return &___PointSize_1; }
	inline void set_PointSize_1(float value)
	{
		___PointSize_1 = value;
	}

	inline static int32_t get_offset_of_Scale_2() { return static_cast<int32_t>(offsetof(FaceInfo_t43812B1A171B1AE8E3591EADED098DE81264F6A5, ___Scale_2)); }
	inline float get_Scale_2() const { return ___Scale_2; }
	inline float* get_address_of_Scale_2() { return &___Scale_2; }
	inline void set_Scale_2(float value)
	{
		___Scale_2 = value;
	}

	inline static int32_t get_offset_of_CharacterCount_3() { return static_cast<int32_t>(offsetof(FaceInfo_t43812B1A171B1AE8E3591EADED098DE81264F6A5, ___CharacterCount_3)); }
	inline int32_t get_CharacterCount_3() const { return ___CharacterCount_3; }
	inline int32_t* get_address_of_CharacterCount_3() { return &___CharacterCount_3; }
	inline void set_CharacterCount_3(int32_t value)
	{
		___CharacterCount_3 = value;
	}

	inline static int32_t get_offset_of_LineHeight_4() { return static_cast<int32_t>(offsetof(FaceInfo_t43812B1A171B1AE8E3591EADED098DE81264F6A5, ___LineHeight_4)); }
	inline float get_LineHeight_4() const { return ___LineHeight_4; }
	inline float* get_address_of_LineHeight_4() { return &___LineHeight_4; }
	inline void set_LineHeight_4(float value)
	{
		___LineHeight_4 = value;
	}

	inline static int32_t get_offset_of_Baseline_5() { return static_cast<int32_t>(offsetof(FaceInfo_t43812B1A171B1AE8E3591EADED098DE81264F6A5, ___Baseline_5)); }
	inline float get_Baseline_5() const { return ___Baseline_5; }
	inline float* get_address_of_Baseline_5() { return &___Baseline_5; }
	inline void set_Baseline_5(float value)
	{
		___Baseline_5 = value;
	}

	inline static int32_t get_offset_of_Ascender_6() { return static_cast<int32_t>(offsetof(FaceInfo_t43812B1A171B1AE8E3591EADED098DE81264F6A5, ___Ascender_6)); }
	inline float get_Ascender_6() const { return ___Ascender_6; }
	inline float* get_address_of_Ascender_6() { return &___Ascender_6; }
	inline void set_Ascender_6(float value)
	{
		___Ascender_6 = value;
	}

	inline static int32_t get_offset_of_CapHeight_7() { return static_cast<int32_t>(offsetof(FaceInfo_t43812B1A171B1AE8E3591EADED098DE81264F6A5, ___CapHeight_7)); }
	inline float get_CapHeight_7() const { return ___CapHeight_7; }
	inline float* get_address_of_CapHeight_7() { return &___CapHeight_7; }
	inline void set_CapHeight_7(float value)
	{
		___CapHeight_7 = value;
	}

	inline static int32_t get_offset_of_Descender_8() { return static_cast<int32_t>(offsetof(FaceInfo_t43812B1A171B1AE8E3591EADED098DE81264F6A5, ___Descender_8)); }
	inline float get_Descender_8() const { return ___Descender_8; }
	inline float* get_address_of_Descender_8() { return &___Descender_8; }
	inline void set_Descender_8(float value)
	{
		___Descender_8 = value;
	}

	inline static int32_t get_offset_of_CenterLine_9() { return static_cast<int32_t>(offsetof(FaceInfo_t43812B1A171B1AE8E3591EADED098DE81264F6A5, ___CenterLine_9)); }
	inline float get_CenterLine_9() const { return ___CenterLine_9; }
	inline float* get_address_of_CenterLine_9() { return &___CenterLine_9; }
	inline void set_CenterLine_9(float value)
	{
		___CenterLine_9 = value;
	}

	inline static int32_t get_offset_of_SuperscriptOffset_10() { return static_cast<int32_t>(offsetof(FaceInfo_t43812B1A171B1AE8E3591EADED098DE81264F6A5, ___SuperscriptOffset_10)); }
	inline float get_SuperscriptOffset_10() const { return ___SuperscriptOffset_10; }
	inline float* get_address_of_SuperscriptOffset_10() { return &___SuperscriptOffset_10; }
	inline void set_SuperscriptOffset_10(float value)
	{
		___SuperscriptOffset_10 = value;
	}

	inline static int32_t get_offset_of_SubscriptOffset_11() { return static_cast<int32_t>(offsetof(FaceInfo_t43812B1A171B1AE8E3591EADED098DE81264F6A5, ___SubscriptOffset_11)); }
	inline float get_SubscriptOffset_11() const { return ___SubscriptOffset_11; }
	inline float* get_address_of_SubscriptOffset_11() { return &___SubscriptOffset_11; }
	inline void set_SubscriptOffset_11(float value)
	{
		___SubscriptOffset_11 = value;
	}

	inline static int32_t get_offset_of_SubSize_12() { return static_cast<int32_t>(offsetof(FaceInfo_t43812B1A171B1AE8E3591EADED098DE81264F6A5, ___SubSize_12)); }
	inline float get_SubSize_12() const { return ___SubSize_12; }
	inline float* get_address_of_SubSize_12() { return &___SubSize_12; }
	inline void set_SubSize_12(float value)
	{
		___SubSize_12 = value;
	}

	inline static int32_t get_offset_of_Underline_13() { return static_cast<int32_t>(offsetof(FaceInfo_t43812B1A171B1AE8E3591EADED098DE81264F6A5, ___Underline_13)); }
	inline float get_Underline_13() const { return ___Underline_13; }
	inline float* get_address_of_Underline_13() { return &___Underline_13; }
	inline void set_Underline_13(float value)
	{
		___Underline_13 = value;
	}

	inline static int32_t get_offset_of_UnderlineThickness_14() { return static_cast<int32_t>(offsetof(FaceInfo_t43812B1A171B1AE8E3591EADED098DE81264F6A5, ___UnderlineThickness_14)); }
	inline float get_UnderlineThickness_14() const { return ___UnderlineThickness_14; }
	inline float* get_address_of_UnderlineThickness_14() { return &___UnderlineThickness_14; }
	inline void set_UnderlineThickness_14(float value)
	{
		___UnderlineThickness_14 = value;
	}

	inline static int32_t get_offset_of_strikethrough_15() { return static_cast<int32_t>(offsetof(FaceInfo_t43812B1A171B1AE8E3591EADED098DE81264F6A5, ___strikethrough_15)); }
	inline float get_strikethrough_15() const { return ___strikethrough_15; }
	inline float* get_address_of_strikethrough_15() { return &___strikethrough_15; }
	inline void set_strikethrough_15(float value)
	{
		___strikethrough_15 = value;
	}

	inline static int32_t get_offset_of_strikethroughThickness_16() { return static_cast<int32_t>(offsetof(FaceInfo_t43812B1A171B1AE8E3591EADED098DE81264F6A5, ___strikethroughThickness_16)); }
	inline float get_strikethroughThickness_16() const { return ___strikethroughThickness_16; }
	inline float* get_address_of_strikethroughThickness_16() { return &___strikethroughThickness_16; }
	inline void set_strikethroughThickness_16(float value)
	{
		___strikethroughThickness_16 = value;
	}

	inline static int32_t get_offset_of_TabWidth_17() { return static_cast<int32_t>(offsetof(FaceInfo_t43812B1A171B1AE8E3591EADED098DE81264F6A5, ___TabWidth_17)); }
	inline float get_TabWidth_17() const { return ___TabWidth_17; }
	inline float* get_address_of_TabWidth_17() { return &___TabWidth_17; }
	inline void set_TabWidth_17(float value)
	{
		___TabWidth_17 = value;
	}

	inline static int32_t get_offset_of_Padding_18() { return static_cast<int32_t>(offsetof(FaceInfo_t43812B1A171B1AE8E3591EADED098DE81264F6A5, ___Padding_18)); }
	inline float get_Padding_18() const { return ___Padding_18; }
	inline float* get_address_of_Padding_18() { return &___Padding_18; }
	inline void set_Padding_18(float value)
	{
		___Padding_18 = value;
	}

	inline static int32_t get_offset_of_AtlasWidth_19() { return static_cast<int32_t>(offsetof(FaceInfo_t43812B1A171B1AE8E3591EADED098DE81264F6A5, ___AtlasWidth_19)); }
	inline float get_AtlasWidth_19() const { return ___AtlasWidth_19; }
	inline float* get_address_of_AtlasWidth_19() { return &___AtlasWidth_19; }
	inline void set_AtlasWidth_19(float value)
	{
		___AtlasWidth_19 = value;
	}

	inline static int32_t get_offset_of_AtlasHeight_20() { return static_cast<int32_t>(offsetof(FaceInfo_t43812B1A171B1AE8E3591EADED098DE81264F6A5, ___AtlasHeight_20)); }
	inline float get_AtlasHeight_20() const { return ___AtlasHeight_20; }
	inline float* get_address_of_AtlasHeight_20() { return &___AtlasHeight_20; }
	inline void set_AtlasHeight_20(float value)
	{
		___AtlasHeight_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACEINFO_T43812B1A171B1AE8E3591EADED098DE81264F6A5_H
#ifndef KERNINGTABLE_TAF8D2AABDC878598EFE90D838BAAD285FA8CE05F_H
#define KERNINGTABLE_TAF8D2AABDC878598EFE90D838BAAD285FA8CE05F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.KerningTable
struct  KerningTable_tAF8D2AABDC878598EFE90D838BAAD285FA8CE05F  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<TMPro.KerningPair> TMPro.KerningTable::kerningPairs
	List_1_tA4D79745FA03271B85D7DCF36F41C845E40F97A8 * ___kerningPairs_0;

public:
	inline static int32_t get_offset_of_kerningPairs_0() { return static_cast<int32_t>(offsetof(KerningTable_tAF8D2AABDC878598EFE90D838BAAD285FA8CE05F, ___kerningPairs_0)); }
	inline List_1_tA4D79745FA03271B85D7DCF36F41C845E40F97A8 * get_kerningPairs_0() const { return ___kerningPairs_0; }
	inline List_1_tA4D79745FA03271B85D7DCF36F41C845E40F97A8 ** get_address_of_kerningPairs_0() { return &___kerningPairs_0; }
	inline void set_kerningPairs_0(List_1_tA4D79745FA03271B85D7DCF36F41C845E40F97A8 * value)
	{
		___kerningPairs_0 = value;
		Il2CppCodeGenWriteBarrier((&___kerningPairs_0), value);
	}
};

struct KerningTable_tAF8D2AABDC878598EFE90D838BAAD285FA8CE05F_StaticFields
{
public:
	// System.Func`2<TMPro.KerningPair,System.UInt32> TMPro.KerningTable::<>f__am$cache0
	Func_2_t91771320D5745AD965BB19871562F93652AE024F * ___U3CU3Ef__amU24cache0_1;
	// System.Func`2<TMPro.KerningPair,System.UInt32> TMPro.KerningTable::<>f__am$cache1
	Func_2_t91771320D5745AD965BB19871562F93652AE024F * ___U3CU3Ef__amU24cache1_2;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_1() { return static_cast<int32_t>(offsetof(KerningTable_tAF8D2AABDC878598EFE90D838BAAD285FA8CE05F_StaticFields, ___U3CU3Ef__amU24cache0_1)); }
	inline Func_2_t91771320D5745AD965BB19871562F93652AE024F * get_U3CU3Ef__amU24cache0_1() const { return ___U3CU3Ef__amU24cache0_1; }
	inline Func_2_t91771320D5745AD965BB19871562F93652AE024F ** get_address_of_U3CU3Ef__amU24cache0_1() { return &___U3CU3Ef__amU24cache0_1; }
	inline void set_U3CU3Ef__amU24cache0_1(Func_2_t91771320D5745AD965BB19871562F93652AE024F * value)
	{
		___U3CU3Ef__amU24cache0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_2() { return static_cast<int32_t>(offsetof(KerningTable_tAF8D2AABDC878598EFE90D838BAAD285FA8CE05F_StaticFields, ___U3CU3Ef__amU24cache1_2)); }
	inline Func_2_t91771320D5745AD965BB19871562F93652AE024F * get_U3CU3Ef__amU24cache1_2() const { return ___U3CU3Ef__amU24cache1_2; }
	inline Func_2_t91771320D5745AD965BB19871562F93652AE024F ** get_address_of_U3CU3Ef__amU24cache1_2() { return &___U3CU3Ef__amU24cache1_2; }
	inline void set_U3CU3Ef__amU24cache1_2(Func_2_t91771320D5745AD965BB19871562F93652AE024F * value)
	{
		___U3CU3Ef__amU24cache1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KERNINGTABLE_TAF8D2AABDC878598EFE90D838BAAD285FA8CE05F_H
#ifndef U3CADDGLYPHPAIRADJUSTMENTRECORDU3EC__ANONSTOREY1_T343265D67166E73A018132EE036FCAAFDD7D56B4_H
#define U3CADDGLYPHPAIRADJUSTMENTRECORDU3EC__ANONSTOREY1_T343265D67166E73A018132EE036FCAAFDD7D56B4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.KerningTable/<AddGlyphPairAdjustmentRecord>c__AnonStorey1
struct  U3CAddGlyphPairAdjustmentRecordU3Ec__AnonStorey1_t343265D67166E73A018132EE036FCAAFDD7D56B4  : public RuntimeObject
{
public:
	// System.UInt32 TMPro.KerningTable/<AddGlyphPairAdjustmentRecord>c__AnonStorey1::first
	uint32_t ___first_0;
	// System.UInt32 TMPro.KerningTable/<AddGlyphPairAdjustmentRecord>c__AnonStorey1::second
	uint32_t ___second_1;

public:
	inline static int32_t get_offset_of_first_0() { return static_cast<int32_t>(offsetof(U3CAddGlyphPairAdjustmentRecordU3Ec__AnonStorey1_t343265D67166E73A018132EE036FCAAFDD7D56B4, ___first_0)); }
	inline uint32_t get_first_0() const { return ___first_0; }
	inline uint32_t* get_address_of_first_0() { return &___first_0; }
	inline void set_first_0(uint32_t value)
	{
		___first_0 = value;
	}

	inline static int32_t get_offset_of_second_1() { return static_cast<int32_t>(offsetof(U3CAddGlyphPairAdjustmentRecordU3Ec__AnonStorey1_t343265D67166E73A018132EE036FCAAFDD7D56B4, ___second_1)); }
	inline uint32_t get_second_1() const { return ___second_1; }
	inline uint32_t* get_address_of_second_1() { return &___second_1; }
	inline void set_second_1(uint32_t value)
	{
		___second_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CADDGLYPHPAIRADJUSTMENTRECORDU3EC__ANONSTOREY1_T343265D67166E73A018132EE036FCAAFDD7D56B4_H
#ifndef U3CADDKERNINGPAIRU3EC__ANONSTOREY0_T7580E224467E79EE3F9EF6CCD33D8198582777F8_H
#define U3CADDKERNINGPAIRU3EC__ANONSTOREY0_T7580E224467E79EE3F9EF6CCD33D8198582777F8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.KerningTable/<AddKerningPair>c__AnonStorey0
struct  U3CAddKerningPairU3Ec__AnonStorey0_t7580E224467E79EE3F9EF6CCD33D8198582777F8  : public RuntimeObject
{
public:
	// System.UInt32 TMPro.KerningTable/<AddKerningPair>c__AnonStorey0::first
	uint32_t ___first_0;
	// System.UInt32 TMPro.KerningTable/<AddKerningPair>c__AnonStorey0::second
	uint32_t ___second_1;

public:
	inline static int32_t get_offset_of_first_0() { return static_cast<int32_t>(offsetof(U3CAddKerningPairU3Ec__AnonStorey0_t7580E224467E79EE3F9EF6CCD33D8198582777F8, ___first_0)); }
	inline uint32_t get_first_0() const { return ___first_0; }
	inline uint32_t* get_address_of_first_0() { return &___first_0; }
	inline void set_first_0(uint32_t value)
	{
		___first_0 = value;
	}

	inline static int32_t get_offset_of_second_1() { return static_cast<int32_t>(offsetof(U3CAddKerningPairU3Ec__AnonStorey0_t7580E224467E79EE3F9EF6CCD33D8198582777F8, ___second_1)); }
	inline uint32_t get_second_1() const { return ___second_1; }
	inline uint32_t* get_address_of_second_1() { return &___second_1; }
	inline void set_second_1(uint32_t value)
	{
		___second_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CADDKERNINGPAIRU3EC__ANONSTOREY0_T7580E224467E79EE3F9EF6CCD33D8198582777F8_H
#ifndef U3CREMOVEKERNINGPAIRU3EC__ANONSTOREY2_T26315506D3EDB64F137B8DA549CF82E5B01FABD8_H
#define U3CREMOVEKERNINGPAIRU3EC__ANONSTOREY2_T26315506D3EDB64F137B8DA549CF82E5B01FABD8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.KerningTable/<RemoveKerningPair>c__AnonStorey2
struct  U3CRemoveKerningPairU3Ec__AnonStorey2_t26315506D3EDB64F137B8DA549CF82E5B01FABD8  : public RuntimeObject
{
public:
	// System.Int32 TMPro.KerningTable/<RemoveKerningPair>c__AnonStorey2::left
	int32_t ___left_0;
	// System.Int32 TMPro.KerningTable/<RemoveKerningPair>c__AnonStorey2::right
	int32_t ___right_1;

public:
	inline static int32_t get_offset_of_left_0() { return static_cast<int32_t>(offsetof(U3CRemoveKerningPairU3Ec__AnonStorey2_t26315506D3EDB64F137B8DA549CF82E5B01FABD8, ___left_0)); }
	inline int32_t get_left_0() const { return ___left_0; }
	inline int32_t* get_address_of_left_0() { return &___left_0; }
	inline void set_left_0(int32_t value)
	{
		___left_0 = value;
	}

	inline static int32_t get_offset_of_right_1() { return static_cast<int32_t>(offsetof(U3CRemoveKerningPairU3Ec__AnonStorey2_t26315506D3EDB64F137B8DA549CF82E5B01FABD8, ___right_1)); }
	inline int32_t get_right_1() const { return ___right_1; }
	inline int32_t* get_address_of_right_1() { return &___right_1; }
	inline void set_right_1(int32_t value)
	{
		___right_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREMOVEKERNINGPAIRU3EC__ANONSTOREY2_T26315506D3EDB64F137B8DA549CF82E5B01FABD8_H
#ifndef SHADERUTILITIES_T94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_H
#define SHADERUTILITIES_T94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.ShaderUtilities
struct  ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8  : public RuntimeObject
{
public:

public:
};

struct ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields
{
public:
	// System.Int32 TMPro.ShaderUtilities::ID_MainTex
	int32_t ___ID_MainTex_0;
	// System.Int32 TMPro.ShaderUtilities::ID_FaceTex
	int32_t ___ID_FaceTex_1;
	// System.Int32 TMPro.ShaderUtilities::ID_FaceColor
	int32_t ___ID_FaceColor_2;
	// System.Int32 TMPro.ShaderUtilities::ID_FaceDilate
	int32_t ___ID_FaceDilate_3;
	// System.Int32 TMPro.ShaderUtilities::ID_Shininess
	int32_t ___ID_Shininess_4;
	// System.Int32 TMPro.ShaderUtilities::ID_UnderlayColor
	int32_t ___ID_UnderlayColor_5;
	// System.Int32 TMPro.ShaderUtilities::ID_UnderlayOffsetX
	int32_t ___ID_UnderlayOffsetX_6;
	// System.Int32 TMPro.ShaderUtilities::ID_UnderlayOffsetY
	int32_t ___ID_UnderlayOffsetY_7;
	// System.Int32 TMPro.ShaderUtilities::ID_UnderlayDilate
	int32_t ___ID_UnderlayDilate_8;
	// System.Int32 TMPro.ShaderUtilities::ID_UnderlaySoftness
	int32_t ___ID_UnderlaySoftness_9;
	// System.Int32 TMPro.ShaderUtilities::ID_WeightNormal
	int32_t ___ID_WeightNormal_10;
	// System.Int32 TMPro.ShaderUtilities::ID_WeightBold
	int32_t ___ID_WeightBold_11;
	// System.Int32 TMPro.ShaderUtilities::ID_OutlineTex
	int32_t ___ID_OutlineTex_12;
	// System.Int32 TMPro.ShaderUtilities::ID_OutlineWidth
	int32_t ___ID_OutlineWidth_13;
	// System.Int32 TMPro.ShaderUtilities::ID_OutlineSoftness
	int32_t ___ID_OutlineSoftness_14;
	// System.Int32 TMPro.ShaderUtilities::ID_OutlineColor
	int32_t ___ID_OutlineColor_15;
	// System.Int32 TMPro.ShaderUtilities::ID_Padding
	int32_t ___ID_Padding_16;
	// System.Int32 TMPro.ShaderUtilities::ID_GradientScale
	int32_t ___ID_GradientScale_17;
	// System.Int32 TMPro.ShaderUtilities::ID_ScaleX
	int32_t ___ID_ScaleX_18;
	// System.Int32 TMPro.ShaderUtilities::ID_ScaleY
	int32_t ___ID_ScaleY_19;
	// System.Int32 TMPro.ShaderUtilities::ID_PerspectiveFilter
	int32_t ___ID_PerspectiveFilter_20;
	// System.Int32 TMPro.ShaderUtilities::ID_TextureWidth
	int32_t ___ID_TextureWidth_21;
	// System.Int32 TMPro.ShaderUtilities::ID_TextureHeight
	int32_t ___ID_TextureHeight_22;
	// System.Int32 TMPro.ShaderUtilities::ID_BevelAmount
	int32_t ___ID_BevelAmount_23;
	// System.Int32 TMPro.ShaderUtilities::ID_GlowColor
	int32_t ___ID_GlowColor_24;
	// System.Int32 TMPro.ShaderUtilities::ID_GlowOffset
	int32_t ___ID_GlowOffset_25;
	// System.Int32 TMPro.ShaderUtilities::ID_GlowPower
	int32_t ___ID_GlowPower_26;
	// System.Int32 TMPro.ShaderUtilities::ID_GlowOuter
	int32_t ___ID_GlowOuter_27;
	// System.Int32 TMPro.ShaderUtilities::ID_LightAngle
	int32_t ___ID_LightAngle_28;
	// System.Int32 TMPro.ShaderUtilities::ID_EnvMap
	int32_t ___ID_EnvMap_29;
	// System.Int32 TMPro.ShaderUtilities::ID_EnvMatrix
	int32_t ___ID_EnvMatrix_30;
	// System.Int32 TMPro.ShaderUtilities::ID_EnvMatrixRotation
	int32_t ___ID_EnvMatrixRotation_31;
	// System.Int32 TMPro.ShaderUtilities::ID_MaskCoord
	int32_t ___ID_MaskCoord_32;
	// System.Int32 TMPro.ShaderUtilities::ID_ClipRect
	int32_t ___ID_ClipRect_33;
	// System.Int32 TMPro.ShaderUtilities::ID_MaskSoftnessX
	int32_t ___ID_MaskSoftnessX_34;
	// System.Int32 TMPro.ShaderUtilities::ID_MaskSoftnessY
	int32_t ___ID_MaskSoftnessY_35;
	// System.Int32 TMPro.ShaderUtilities::ID_VertexOffsetX
	int32_t ___ID_VertexOffsetX_36;
	// System.Int32 TMPro.ShaderUtilities::ID_VertexOffsetY
	int32_t ___ID_VertexOffsetY_37;
	// System.Int32 TMPro.ShaderUtilities::ID_UseClipRect
	int32_t ___ID_UseClipRect_38;
	// System.Int32 TMPro.ShaderUtilities::ID_StencilID
	int32_t ___ID_StencilID_39;
	// System.Int32 TMPro.ShaderUtilities::ID_StencilOp
	int32_t ___ID_StencilOp_40;
	// System.Int32 TMPro.ShaderUtilities::ID_StencilComp
	int32_t ___ID_StencilComp_41;
	// System.Int32 TMPro.ShaderUtilities::ID_StencilReadMask
	int32_t ___ID_StencilReadMask_42;
	// System.Int32 TMPro.ShaderUtilities::ID_StencilWriteMask
	int32_t ___ID_StencilWriteMask_43;
	// System.Int32 TMPro.ShaderUtilities::ID_ShaderFlags
	int32_t ___ID_ShaderFlags_44;
	// System.Int32 TMPro.ShaderUtilities::ID_ScaleRatio_A
	int32_t ___ID_ScaleRatio_A_45;
	// System.Int32 TMPro.ShaderUtilities::ID_ScaleRatio_B
	int32_t ___ID_ScaleRatio_B_46;
	// System.Int32 TMPro.ShaderUtilities::ID_ScaleRatio_C
	int32_t ___ID_ScaleRatio_C_47;
	// System.String TMPro.ShaderUtilities::Keyword_Bevel
	String_t* ___Keyword_Bevel_48;
	// System.String TMPro.ShaderUtilities::Keyword_Glow
	String_t* ___Keyword_Glow_49;
	// System.String TMPro.ShaderUtilities::Keyword_Underlay
	String_t* ___Keyword_Underlay_50;
	// System.String TMPro.ShaderUtilities::Keyword_Ratios
	String_t* ___Keyword_Ratios_51;
	// System.String TMPro.ShaderUtilities::Keyword_MASK_SOFT
	String_t* ___Keyword_MASK_SOFT_52;
	// System.String TMPro.ShaderUtilities::Keyword_MASK_HARD
	String_t* ___Keyword_MASK_HARD_53;
	// System.String TMPro.ShaderUtilities::Keyword_MASK_TEX
	String_t* ___Keyword_MASK_TEX_54;
	// System.String TMPro.ShaderUtilities::Keyword_Outline
	String_t* ___Keyword_Outline_55;
	// System.String TMPro.ShaderUtilities::ShaderTag_ZTestMode
	String_t* ___ShaderTag_ZTestMode_56;
	// System.String TMPro.ShaderUtilities::ShaderTag_CullMode
	String_t* ___ShaderTag_CullMode_57;
	// System.Single TMPro.ShaderUtilities::m_clamp
	float ___m_clamp_58;
	// System.Boolean TMPro.ShaderUtilities::isInitialized
	bool ___isInitialized_59;

public:
	inline static int32_t get_offset_of_ID_MainTex_0() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_MainTex_0)); }
	inline int32_t get_ID_MainTex_0() const { return ___ID_MainTex_0; }
	inline int32_t* get_address_of_ID_MainTex_0() { return &___ID_MainTex_0; }
	inline void set_ID_MainTex_0(int32_t value)
	{
		___ID_MainTex_0 = value;
	}

	inline static int32_t get_offset_of_ID_FaceTex_1() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_FaceTex_1)); }
	inline int32_t get_ID_FaceTex_1() const { return ___ID_FaceTex_1; }
	inline int32_t* get_address_of_ID_FaceTex_1() { return &___ID_FaceTex_1; }
	inline void set_ID_FaceTex_1(int32_t value)
	{
		___ID_FaceTex_1 = value;
	}

	inline static int32_t get_offset_of_ID_FaceColor_2() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_FaceColor_2)); }
	inline int32_t get_ID_FaceColor_2() const { return ___ID_FaceColor_2; }
	inline int32_t* get_address_of_ID_FaceColor_2() { return &___ID_FaceColor_2; }
	inline void set_ID_FaceColor_2(int32_t value)
	{
		___ID_FaceColor_2 = value;
	}

	inline static int32_t get_offset_of_ID_FaceDilate_3() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_FaceDilate_3)); }
	inline int32_t get_ID_FaceDilate_3() const { return ___ID_FaceDilate_3; }
	inline int32_t* get_address_of_ID_FaceDilate_3() { return &___ID_FaceDilate_3; }
	inline void set_ID_FaceDilate_3(int32_t value)
	{
		___ID_FaceDilate_3 = value;
	}

	inline static int32_t get_offset_of_ID_Shininess_4() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_Shininess_4)); }
	inline int32_t get_ID_Shininess_4() const { return ___ID_Shininess_4; }
	inline int32_t* get_address_of_ID_Shininess_4() { return &___ID_Shininess_4; }
	inline void set_ID_Shininess_4(int32_t value)
	{
		___ID_Shininess_4 = value;
	}

	inline static int32_t get_offset_of_ID_UnderlayColor_5() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_UnderlayColor_5)); }
	inline int32_t get_ID_UnderlayColor_5() const { return ___ID_UnderlayColor_5; }
	inline int32_t* get_address_of_ID_UnderlayColor_5() { return &___ID_UnderlayColor_5; }
	inline void set_ID_UnderlayColor_5(int32_t value)
	{
		___ID_UnderlayColor_5 = value;
	}

	inline static int32_t get_offset_of_ID_UnderlayOffsetX_6() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_UnderlayOffsetX_6)); }
	inline int32_t get_ID_UnderlayOffsetX_6() const { return ___ID_UnderlayOffsetX_6; }
	inline int32_t* get_address_of_ID_UnderlayOffsetX_6() { return &___ID_UnderlayOffsetX_6; }
	inline void set_ID_UnderlayOffsetX_6(int32_t value)
	{
		___ID_UnderlayOffsetX_6 = value;
	}

	inline static int32_t get_offset_of_ID_UnderlayOffsetY_7() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_UnderlayOffsetY_7)); }
	inline int32_t get_ID_UnderlayOffsetY_7() const { return ___ID_UnderlayOffsetY_7; }
	inline int32_t* get_address_of_ID_UnderlayOffsetY_7() { return &___ID_UnderlayOffsetY_7; }
	inline void set_ID_UnderlayOffsetY_7(int32_t value)
	{
		___ID_UnderlayOffsetY_7 = value;
	}

	inline static int32_t get_offset_of_ID_UnderlayDilate_8() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_UnderlayDilate_8)); }
	inline int32_t get_ID_UnderlayDilate_8() const { return ___ID_UnderlayDilate_8; }
	inline int32_t* get_address_of_ID_UnderlayDilate_8() { return &___ID_UnderlayDilate_8; }
	inline void set_ID_UnderlayDilate_8(int32_t value)
	{
		___ID_UnderlayDilate_8 = value;
	}

	inline static int32_t get_offset_of_ID_UnderlaySoftness_9() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_UnderlaySoftness_9)); }
	inline int32_t get_ID_UnderlaySoftness_9() const { return ___ID_UnderlaySoftness_9; }
	inline int32_t* get_address_of_ID_UnderlaySoftness_9() { return &___ID_UnderlaySoftness_9; }
	inline void set_ID_UnderlaySoftness_9(int32_t value)
	{
		___ID_UnderlaySoftness_9 = value;
	}

	inline static int32_t get_offset_of_ID_WeightNormal_10() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_WeightNormal_10)); }
	inline int32_t get_ID_WeightNormal_10() const { return ___ID_WeightNormal_10; }
	inline int32_t* get_address_of_ID_WeightNormal_10() { return &___ID_WeightNormal_10; }
	inline void set_ID_WeightNormal_10(int32_t value)
	{
		___ID_WeightNormal_10 = value;
	}

	inline static int32_t get_offset_of_ID_WeightBold_11() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_WeightBold_11)); }
	inline int32_t get_ID_WeightBold_11() const { return ___ID_WeightBold_11; }
	inline int32_t* get_address_of_ID_WeightBold_11() { return &___ID_WeightBold_11; }
	inline void set_ID_WeightBold_11(int32_t value)
	{
		___ID_WeightBold_11 = value;
	}

	inline static int32_t get_offset_of_ID_OutlineTex_12() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_OutlineTex_12)); }
	inline int32_t get_ID_OutlineTex_12() const { return ___ID_OutlineTex_12; }
	inline int32_t* get_address_of_ID_OutlineTex_12() { return &___ID_OutlineTex_12; }
	inline void set_ID_OutlineTex_12(int32_t value)
	{
		___ID_OutlineTex_12 = value;
	}

	inline static int32_t get_offset_of_ID_OutlineWidth_13() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_OutlineWidth_13)); }
	inline int32_t get_ID_OutlineWidth_13() const { return ___ID_OutlineWidth_13; }
	inline int32_t* get_address_of_ID_OutlineWidth_13() { return &___ID_OutlineWidth_13; }
	inline void set_ID_OutlineWidth_13(int32_t value)
	{
		___ID_OutlineWidth_13 = value;
	}

	inline static int32_t get_offset_of_ID_OutlineSoftness_14() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_OutlineSoftness_14)); }
	inline int32_t get_ID_OutlineSoftness_14() const { return ___ID_OutlineSoftness_14; }
	inline int32_t* get_address_of_ID_OutlineSoftness_14() { return &___ID_OutlineSoftness_14; }
	inline void set_ID_OutlineSoftness_14(int32_t value)
	{
		___ID_OutlineSoftness_14 = value;
	}

	inline static int32_t get_offset_of_ID_OutlineColor_15() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_OutlineColor_15)); }
	inline int32_t get_ID_OutlineColor_15() const { return ___ID_OutlineColor_15; }
	inline int32_t* get_address_of_ID_OutlineColor_15() { return &___ID_OutlineColor_15; }
	inline void set_ID_OutlineColor_15(int32_t value)
	{
		___ID_OutlineColor_15 = value;
	}

	inline static int32_t get_offset_of_ID_Padding_16() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_Padding_16)); }
	inline int32_t get_ID_Padding_16() const { return ___ID_Padding_16; }
	inline int32_t* get_address_of_ID_Padding_16() { return &___ID_Padding_16; }
	inline void set_ID_Padding_16(int32_t value)
	{
		___ID_Padding_16 = value;
	}

	inline static int32_t get_offset_of_ID_GradientScale_17() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_GradientScale_17)); }
	inline int32_t get_ID_GradientScale_17() const { return ___ID_GradientScale_17; }
	inline int32_t* get_address_of_ID_GradientScale_17() { return &___ID_GradientScale_17; }
	inline void set_ID_GradientScale_17(int32_t value)
	{
		___ID_GradientScale_17 = value;
	}

	inline static int32_t get_offset_of_ID_ScaleX_18() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_ScaleX_18)); }
	inline int32_t get_ID_ScaleX_18() const { return ___ID_ScaleX_18; }
	inline int32_t* get_address_of_ID_ScaleX_18() { return &___ID_ScaleX_18; }
	inline void set_ID_ScaleX_18(int32_t value)
	{
		___ID_ScaleX_18 = value;
	}

	inline static int32_t get_offset_of_ID_ScaleY_19() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_ScaleY_19)); }
	inline int32_t get_ID_ScaleY_19() const { return ___ID_ScaleY_19; }
	inline int32_t* get_address_of_ID_ScaleY_19() { return &___ID_ScaleY_19; }
	inline void set_ID_ScaleY_19(int32_t value)
	{
		___ID_ScaleY_19 = value;
	}

	inline static int32_t get_offset_of_ID_PerspectiveFilter_20() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_PerspectiveFilter_20)); }
	inline int32_t get_ID_PerspectiveFilter_20() const { return ___ID_PerspectiveFilter_20; }
	inline int32_t* get_address_of_ID_PerspectiveFilter_20() { return &___ID_PerspectiveFilter_20; }
	inline void set_ID_PerspectiveFilter_20(int32_t value)
	{
		___ID_PerspectiveFilter_20 = value;
	}

	inline static int32_t get_offset_of_ID_TextureWidth_21() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_TextureWidth_21)); }
	inline int32_t get_ID_TextureWidth_21() const { return ___ID_TextureWidth_21; }
	inline int32_t* get_address_of_ID_TextureWidth_21() { return &___ID_TextureWidth_21; }
	inline void set_ID_TextureWidth_21(int32_t value)
	{
		___ID_TextureWidth_21 = value;
	}

	inline static int32_t get_offset_of_ID_TextureHeight_22() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_TextureHeight_22)); }
	inline int32_t get_ID_TextureHeight_22() const { return ___ID_TextureHeight_22; }
	inline int32_t* get_address_of_ID_TextureHeight_22() { return &___ID_TextureHeight_22; }
	inline void set_ID_TextureHeight_22(int32_t value)
	{
		___ID_TextureHeight_22 = value;
	}

	inline static int32_t get_offset_of_ID_BevelAmount_23() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_BevelAmount_23)); }
	inline int32_t get_ID_BevelAmount_23() const { return ___ID_BevelAmount_23; }
	inline int32_t* get_address_of_ID_BevelAmount_23() { return &___ID_BevelAmount_23; }
	inline void set_ID_BevelAmount_23(int32_t value)
	{
		___ID_BevelAmount_23 = value;
	}

	inline static int32_t get_offset_of_ID_GlowColor_24() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_GlowColor_24)); }
	inline int32_t get_ID_GlowColor_24() const { return ___ID_GlowColor_24; }
	inline int32_t* get_address_of_ID_GlowColor_24() { return &___ID_GlowColor_24; }
	inline void set_ID_GlowColor_24(int32_t value)
	{
		___ID_GlowColor_24 = value;
	}

	inline static int32_t get_offset_of_ID_GlowOffset_25() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_GlowOffset_25)); }
	inline int32_t get_ID_GlowOffset_25() const { return ___ID_GlowOffset_25; }
	inline int32_t* get_address_of_ID_GlowOffset_25() { return &___ID_GlowOffset_25; }
	inline void set_ID_GlowOffset_25(int32_t value)
	{
		___ID_GlowOffset_25 = value;
	}

	inline static int32_t get_offset_of_ID_GlowPower_26() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_GlowPower_26)); }
	inline int32_t get_ID_GlowPower_26() const { return ___ID_GlowPower_26; }
	inline int32_t* get_address_of_ID_GlowPower_26() { return &___ID_GlowPower_26; }
	inline void set_ID_GlowPower_26(int32_t value)
	{
		___ID_GlowPower_26 = value;
	}

	inline static int32_t get_offset_of_ID_GlowOuter_27() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_GlowOuter_27)); }
	inline int32_t get_ID_GlowOuter_27() const { return ___ID_GlowOuter_27; }
	inline int32_t* get_address_of_ID_GlowOuter_27() { return &___ID_GlowOuter_27; }
	inline void set_ID_GlowOuter_27(int32_t value)
	{
		___ID_GlowOuter_27 = value;
	}

	inline static int32_t get_offset_of_ID_LightAngle_28() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_LightAngle_28)); }
	inline int32_t get_ID_LightAngle_28() const { return ___ID_LightAngle_28; }
	inline int32_t* get_address_of_ID_LightAngle_28() { return &___ID_LightAngle_28; }
	inline void set_ID_LightAngle_28(int32_t value)
	{
		___ID_LightAngle_28 = value;
	}

	inline static int32_t get_offset_of_ID_EnvMap_29() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_EnvMap_29)); }
	inline int32_t get_ID_EnvMap_29() const { return ___ID_EnvMap_29; }
	inline int32_t* get_address_of_ID_EnvMap_29() { return &___ID_EnvMap_29; }
	inline void set_ID_EnvMap_29(int32_t value)
	{
		___ID_EnvMap_29 = value;
	}

	inline static int32_t get_offset_of_ID_EnvMatrix_30() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_EnvMatrix_30)); }
	inline int32_t get_ID_EnvMatrix_30() const { return ___ID_EnvMatrix_30; }
	inline int32_t* get_address_of_ID_EnvMatrix_30() { return &___ID_EnvMatrix_30; }
	inline void set_ID_EnvMatrix_30(int32_t value)
	{
		___ID_EnvMatrix_30 = value;
	}

	inline static int32_t get_offset_of_ID_EnvMatrixRotation_31() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_EnvMatrixRotation_31)); }
	inline int32_t get_ID_EnvMatrixRotation_31() const { return ___ID_EnvMatrixRotation_31; }
	inline int32_t* get_address_of_ID_EnvMatrixRotation_31() { return &___ID_EnvMatrixRotation_31; }
	inline void set_ID_EnvMatrixRotation_31(int32_t value)
	{
		___ID_EnvMatrixRotation_31 = value;
	}

	inline static int32_t get_offset_of_ID_MaskCoord_32() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_MaskCoord_32)); }
	inline int32_t get_ID_MaskCoord_32() const { return ___ID_MaskCoord_32; }
	inline int32_t* get_address_of_ID_MaskCoord_32() { return &___ID_MaskCoord_32; }
	inline void set_ID_MaskCoord_32(int32_t value)
	{
		___ID_MaskCoord_32 = value;
	}

	inline static int32_t get_offset_of_ID_ClipRect_33() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_ClipRect_33)); }
	inline int32_t get_ID_ClipRect_33() const { return ___ID_ClipRect_33; }
	inline int32_t* get_address_of_ID_ClipRect_33() { return &___ID_ClipRect_33; }
	inline void set_ID_ClipRect_33(int32_t value)
	{
		___ID_ClipRect_33 = value;
	}

	inline static int32_t get_offset_of_ID_MaskSoftnessX_34() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_MaskSoftnessX_34)); }
	inline int32_t get_ID_MaskSoftnessX_34() const { return ___ID_MaskSoftnessX_34; }
	inline int32_t* get_address_of_ID_MaskSoftnessX_34() { return &___ID_MaskSoftnessX_34; }
	inline void set_ID_MaskSoftnessX_34(int32_t value)
	{
		___ID_MaskSoftnessX_34 = value;
	}

	inline static int32_t get_offset_of_ID_MaskSoftnessY_35() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_MaskSoftnessY_35)); }
	inline int32_t get_ID_MaskSoftnessY_35() const { return ___ID_MaskSoftnessY_35; }
	inline int32_t* get_address_of_ID_MaskSoftnessY_35() { return &___ID_MaskSoftnessY_35; }
	inline void set_ID_MaskSoftnessY_35(int32_t value)
	{
		___ID_MaskSoftnessY_35 = value;
	}

	inline static int32_t get_offset_of_ID_VertexOffsetX_36() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_VertexOffsetX_36)); }
	inline int32_t get_ID_VertexOffsetX_36() const { return ___ID_VertexOffsetX_36; }
	inline int32_t* get_address_of_ID_VertexOffsetX_36() { return &___ID_VertexOffsetX_36; }
	inline void set_ID_VertexOffsetX_36(int32_t value)
	{
		___ID_VertexOffsetX_36 = value;
	}

	inline static int32_t get_offset_of_ID_VertexOffsetY_37() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_VertexOffsetY_37)); }
	inline int32_t get_ID_VertexOffsetY_37() const { return ___ID_VertexOffsetY_37; }
	inline int32_t* get_address_of_ID_VertexOffsetY_37() { return &___ID_VertexOffsetY_37; }
	inline void set_ID_VertexOffsetY_37(int32_t value)
	{
		___ID_VertexOffsetY_37 = value;
	}

	inline static int32_t get_offset_of_ID_UseClipRect_38() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_UseClipRect_38)); }
	inline int32_t get_ID_UseClipRect_38() const { return ___ID_UseClipRect_38; }
	inline int32_t* get_address_of_ID_UseClipRect_38() { return &___ID_UseClipRect_38; }
	inline void set_ID_UseClipRect_38(int32_t value)
	{
		___ID_UseClipRect_38 = value;
	}

	inline static int32_t get_offset_of_ID_StencilID_39() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_StencilID_39)); }
	inline int32_t get_ID_StencilID_39() const { return ___ID_StencilID_39; }
	inline int32_t* get_address_of_ID_StencilID_39() { return &___ID_StencilID_39; }
	inline void set_ID_StencilID_39(int32_t value)
	{
		___ID_StencilID_39 = value;
	}

	inline static int32_t get_offset_of_ID_StencilOp_40() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_StencilOp_40)); }
	inline int32_t get_ID_StencilOp_40() const { return ___ID_StencilOp_40; }
	inline int32_t* get_address_of_ID_StencilOp_40() { return &___ID_StencilOp_40; }
	inline void set_ID_StencilOp_40(int32_t value)
	{
		___ID_StencilOp_40 = value;
	}

	inline static int32_t get_offset_of_ID_StencilComp_41() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_StencilComp_41)); }
	inline int32_t get_ID_StencilComp_41() const { return ___ID_StencilComp_41; }
	inline int32_t* get_address_of_ID_StencilComp_41() { return &___ID_StencilComp_41; }
	inline void set_ID_StencilComp_41(int32_t value)
	{
		___ID_StencilComp_41 = value;
	}

	inline static int32_t get_offset_of_ID_StencilReadMask_42() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_StencilReadMask_42)); }
	inline int32_t get_ID_StencilReadMask_42() const { return ___ID_StencilReadMask_42; }
	inline int32_t* get_address_of_ID_StencilReadMask_42() { return &___ID_StencilReadMask_42; }
	inline void set_ID_StencilReadMask_42(int32_t value)
	{
		___ID_StencilReadMask_42 = value;
	}

	inline static int32_t get_offset_of_ID_StencilWriteMask_43() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_StencilWriteMask_43)); }
	inline int32_t get_ID_StencilWriteMask_43() const { return ___ID_StencilWriteMask_43; }
	inline int32_t* get_address_of_ID_StencilWriteMask_43() { return &___ID_StencilWriteMask_43; }
	inline void set_ID_StencilWriteMask_43(int32_t value)
	{
		___ID_StencilWriteMask_43 = value;
	}

	inline static int32_t get_offset_of_ID_ShaderFlags_44() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_ShaderFlags_44)); }
	inline int32_t get_ID_ShaderFlags_44() const { return ___ID_ShaderFlags_44; }
	inline int32_t* get_address_of_ID_ShaderFlags_44() { return &___ID_ShaderFlags_44; }
	inline void set_ID_ShaderFlags_44(int32_t value)
	{
		___ID_ShaderFlags_44 = value;
	}

	inline static int32_t get_offset_of_ID_ScaleRatio_A_45() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_ScaleRatio_A_45)); }
	inline int32_t get_ID_ScaleRatio_A_45() const { return ___ID_ScaleRatio_A_45; }
	inline int32_t* get_address_of_ID_ScaleRatio_A_45() { return &___ID_ScaleRatio_A_45; }
	inline void set_ID_ScaleRatio_A_45(int32_t value)
	{
		___ID_ScaleRatio_A_45 = value;
	}

	inline static int32_t get_offset_of_ID_ScaleRatio_B_46() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_ScaleRatio_B_46)); }
	inline int32_t get_ID_ScaleRatio_B_46() const { return ___ID_ScaleRatio_B_46; }
	inline int32_t* get_address_of_ID_ScaleRatio_B_46() { return &___ID_ScaleRatio_B_46; }
	inline void set_ID_ScaleRatio_B_46(int32_t value)
	{
		___ID_ScaleRatio_B_46 = value;
	}

	inline static int32_t get_offset_of_ID_ScaleRatio_C_47() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_ScaleRatio_C_47)); }
	inline int32_t get_ID_ScaleRatio_C_47() const { return ___ID_ScaleRatio_C_47; }
	inline int32_t* get_address_of_ID_ScaleRatio_C_47() { return &___ID_ScaleRatio_C_47; }
	inline void set_ID_ScaleRatio_C_47(int32_t value)
	{
		___ID_ScaleRatio_C_47 = value;
	}

	inline static int32_t get_offset_of_Keyword_Bevel_48() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___Keyword_Bevel_48)); }
	inline String_t* get_Keyword_Bevel_48() const { return ___Keyword_Bevel_48; }
	inline String_t** get_address_of_Keyword_Bevel_48() { return &___Keyword_Bevel_48; }
	inline void set_Keyword_Bevel_48(String_t* value)
	{
		___Keyword_Bevel_48 = value;
		Il2CppCodeGenWriteBarrier((&___Keyword_Bevel_48), value);
	}

	inline static int32_t get_offset_of_Keyword_Glow_49() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___Keyword_Glow_49)); }
	inline String_t* get_Keyword_Glow_49() const { return ___Keyword_Glow_49; }
	inline String_t** get_address_of_Keyword_Glow_49() { return &___Keyword_Glow_49; }
	inline void set_Keyword_Glow_49(String_t* value)
	{
		___Keyword_Glow_49 = value;
		Il2CppCodeGenWriteBarrier((&___Keyword_Glow_49), value);
	}

	inline static int32_t get_offset_of_Keyword_Underlay_50() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___Keyword_Underlay_50)); }
	inline String_t* get_Keyword_Underlay_50() const { return ___Keyword_Underlay_50; }
	inline String_t** get_address_of_Keyword_Underlay_50() { return &___Keyword_Underlay_50; }
	inline void set_Keyword_Underlay_50(String_t* value)
	{
		___Keyword_Underlay_50 = value;
		Il2CppCodeGenWriteBarrier((&___Keyword_Underlay_50), value);
	}

	inline static int32_t get_offset_of_Keyword_Ratios_51() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___Keyword_Ratios_51)); }
	inline String_t* get_Keyword_Ratios_51() const { return ___Keyword_Ratios_51; }
	inline String_t** get_address_of_Keyword_Ratios_51() { return &___Keyword_Ratios_51; }
	inline void set_Keyword_Ratios_51(String_t* value)
	{
		___Keyword_Ratios_51 = value;
		Il2CppCodeGenWriteBarrier((&___Keyword_Ratios_51), value);
	}

	inline static int32_t get_offset_of_Keyword_MASK_SOFT_52() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___Keyword_MASK_SOFT_52)); }
	inline String_t* get_Keyword_MASK_SOFT_52() const { return ___Keyword_MASK_SOFT_52; }
	inline String_t** get_address_of_Keyword_MASK_SOFT_52() { return &___Keyword_MASK_SOFT_52; }
	inline void set_Keyword_MASK_SOFT_52(String_t* value)
	{
		___Keyword_MASK_SOFT_52 = value;
		Il2CppCodeGenWriteBarrier((&___Keyword_MASK_SOFT_52), value);
	}

	inline static int32_t get_offset_of_Keyword_MASK_HARD_53() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___Keyword_MASK_HARD_53)); }
	inline String_t* get_Keyword_MASK_HARD_53() const { return ___Keyword_MASK_HARD_53; }
	inline String_t** get_address_of_Keyword_MASK_HARD_53() { return &___Keyword_MASK_HARD_53; }
	inline void set_Keyword_MASK_HARD_53(String_t* value)
	{
		___Keyword_MASK_HARD_53 = value;
		Il2CppCodeGenWriteBarrier((&___Keyword_MASK_HARD_53), value);
	}

	inline static int32_t get_offset_of_Keyword_MASK_TEX_54() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___Keyword_MASK_TEX_54)); }
	inline String_t* get_Keyword_MASK_TEX_54() const { return ___Keyword_MASK_TEX_54; }
	inline String_t** get_address_of_Keyword_MASK_TEX_54() { return &___Keyword_MASK_TEX_54; }
	inline void set_Keyword_MASK_TEX_54(String_t* value)
	{
		___Keyword_MASK_TEX_54 = value;
		Il2CppCodeGenWriteBarrier((&___Keyword_MASK_TEX_54), value);
	}

	inline static int32_t get_offset_of_Keyword_Outline_55() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___Keyword_Outline_55)); }
	inline String_t* get_Keyword_Outline_55() const { return ___Keyword_Outline_55; }
	inline String_t** get_address_of_Keyword_Outline_55() { return &___Keyword_Outline_55; }
	inline void set_Keyword_Outline_55(String_t* value)
	{
		___Keyword_Outline_55 = value;
		Il2CppCodeGenWriteBarrier((&___Keyword_Outline_55), value);
	}

	inline static int32_t get_offset_of_ShaderTag_ZTestMode_56() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ShaderTag_ZTestMode_56)); }
	inline String_t* get_ShaderTag_ZTestMode_56() const { return ___ShaderTag_ZTestMode_56; }
	inline String_t** get_address_of_ShaderTag_ZTestMode_56() { return &___ShaderTag_ZTestMode_56; }
	inline void set_ShaderTag_ZTestMode_56(String_t* value)
	{
		___ShaderTag_ZTestMode_56 = value;
		Il2CppCodeGenWriteBarrier((&___ShaderTag_ZTestMode_56), value);
	}

	inline static int32_t get_offset_of_ShaderTag_CullMode_57() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ShaderTag_CullMode_57)); }
	inline String_t* get_ShaderTag_CullMode_57() const { return ___ShaderTag_CullMode_57; }
	inline String_t** get_address_of_ShaderTag_CullMode_57() { return &___ShaderTag_CullMode_57; }
	inline void set_ShaderTag_CullMode_57(String_t* value)
	{
		___ShaderTag_CullMode_57 = value;
		Il2CppCodeGenWriteBarrier((&___ShaderTag_CullMode_57), value);
	}

	inline static int32_t get_offset_of_m_clamp_58() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___m_clamp_58)); }
	inline float get_m_clamp_58() const { return ___m_clamp_58; }
	inline float* get_address_of_m_clamp_58() { return &___m_clamp_58; }
	inline void set_m_clamp_58(float value)
	{
		___m_clamp_58 = value;
	}

	inline static int32_t get_offset_of_isInitialized_59() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___isInitialized_59)); }
	inline bool get_isInitialized_59() const { return ___isInitialized_59; }
	inline bool* get_address_of_isInitialized_59() { return &___isInitialized_59; }
	inline void set_isInitialized_59(bool value)
	{
		___isInitialized_59 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADERUTILITIES_T94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_H
#ifndef TMP_FONTUTILITIES_TEAE7AD89B734C4BFCC635A65A05ED0BEB690935F_H
#define TMP_FONTUTILITIES_TEAE7AD89B734C4BFCC635A65A05ED0BEB690935F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_FontUtilities
struct  TMP_FontUtilities_tEAE7AD89B734C4BFCC635A65A05ED0BEB690935F  : public RuntimeObject
{
public:

public:
};

struct TMP_FontUtilities_tEAE7AD89B734C4BFCC635A65A05ED0BEB690935F_StaticFields
{
public:
	// System.Collections.Generic.List`1<System.Int32> TMPro.TMP_FontUtilities::k_searchedFontAssets
	List_1_t4A961510635950236678F1B3B2436ECCAF28713A * ___k_searchedFontAssets_0;

public:
	inline static int32_t get_offset_of_k_searchedFontAssets_0() { return static_cast<int32_t>(offsetof(TMP_FontUtilities_tEAE7AD89B734C4BFCC635A65A05ED0BEB690935F_StaticFields, ___k_searchedFontAssets_0)); }
	inline List_1_t4A961510635950236678F1B3B2436ECCAF28713A * get_k_searchedFontAssets_0() const { return ___k_searchedFontAssets_0; }
	inline List_1_t4A961510635950236678F1B3B2436ECCAF28713A ** get_address_of_k_searchedFontAssets_0() { return &___k_searchedFontAssets_0; }
	inline void set_k_searchedFontAssets_0(List_1_t4A961510635950236678F1B3B2436ECCAF28713A * value)
	{
		___k_searchedFontAssets_0 = value;
		Il2CppCodeGenWriteBarrier((&___k_searchedFontAssets_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_FONTUTILITIES_TEAE7AD89B734C4BFCC635A65A05ED0BEB690935F_H
#ifndef TMP_TEXTELEMENT_TB9A6A361BB93487BD07DDDA37A368819DA46C344_H
#define TMP_TEXTELEMENT_TB9A6A361BB93487BD07DDDA37A368819DA46C344_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextElement
struct  TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344  : public RuntimeObject
{
public:
	// System.Int32 TMPro.TMP_TextElement::id
	int32_t ___id_0;
	// System.Single TMPro.TMP_TextElement::x
	float ___x_1;
	// System.Single TMPro.TMP_TextElement::y
	float ___y_2;
	// System.Single TMPro.TMP_TextElement::width
	float ___width_3;
	// System.Single TMPro.TMP_TextElement::height
	float ___height_4;
	// System.Single TMPro.TMP_TextElement::xOffset
	float ___xOffset_5;
	// System.Single TMPro.TMP_TextElement::yOffset
	float ___yOffset_6;
	// System.Single TMPro.TMP_TextElement::xAdvance
	float ___xAdvance_7;
	// System.Single TMPro.TMP_TextElement::scale
	float ___scale_8;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_width_3() { return static_cast<int32_t>(offsetof(TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344, ___width_3)); }
	inline float get_width_3() const { return ___width_3; }
	inline float* get_address_of_width_3() { return &___width_3; }
	inline void set_width_3(float value)
	{
		___width_3 = value;
	}

	inline static int32_t get_offset_of_height_4() { return static_cast<int32_t>(offsetof(TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344, ___height_4)); }
	inline float get_height_4() const { return ___height_4; }
	inline float* get_address_of_height_4() { return &___height_4; }
	inline void set_height_4(float value)
	{
		___height_4 = value;
	}

	inline static int32_t get_offset_of_xOffset_5() { return static_cast<int32_t>(offsetof(TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344, ___xOffset_5)); }
	inline float get_xOffset_5() const { return ___xOffset_5; }
	inline float* get_address_of_xOffset_5() { return &___xOffset_5; }
	inline void set_xOffset_5(float value)
	{
		___xOffset_5 = value;
	}

	inline static int32_t get_offset_of_yOffset_6() { return static_cast<int32_t>(offsetof(TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344, ___yOffset_6)); }
	inline float get_yOffset_6() const { return ___yOffset_6; }
	inline float* get_address_of_yOffset_6() { return &___yOffset_6; }
	inline void set_yOffset_6(float value)
	{
		___yOffset_6 = value;
	}

	inline static int32_t get_offset_of_xAdvance_7() { return static_cast<int32_t>(offsetof(TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344, ___xAdvance_7)); }
	inline float get_xAdvance_7() const { return ___xAdvance_7; }
	inline float* get_address_of_xAdvance_7() { return &___xAdvance_7; }
	inline void set_xAdvance_7(float value)
	{
		___xAdvance_7 = value;
	}

	inline static int32_t get_offset_of_scale_8() { return static_cast<int32_t>(offsetof(TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344, ___scale_8)); }
	inline float get_scale_8() const { return ___scale_8; }
	inline float* get_address_of_scale_8() { return &___scale_8; }
	inline void set_scale_8(float value)
	{
		___scale_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTELEMENT_TB9A6A361BB93487BD07DDDA37A368819DA46C344_H
#ifndef TMPRO_EVENTMANAGER_T0831C7F02A59F06755AFFF94AAF826EEE77E516D_H
#define TMPRO_EVENTMANAGER_T0831C7F02A59F06755AFFF94AAF826EEE77E516D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMPro_EventManager
struct  TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D  : public RuntimeObject
{
public:

public:
};

struct TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields
{
public:
	// TMPro.FastAction`2<System.Object,TMPro.Compute_DT_EventArgs> TMPro.TMPro_EventManager::COMPUTE_DT_EVENT
	FastAction_2_tDD9D6A1F9834B71AC977D63B59F88637BC645E60 * ___COMPUTE_DT_EVENT_0;
	// TMPro.FastAction`2<System.Boolean,UnityEngine.Material> TMPro.TMPro_EventManager::MATERIAL_PROPERTY_EVENT
	FastAction_2_tA69AED04E5299BAE9D8184243189E851F4080707 * ___MATERIAL_PROPERTY_EVENT_1;
	// TMPro.FastAction`2<System.Boolean,TMPro.TMP_FontAsset> TMPro.TMPro_EventManager::FONT_PROPERTY_EVENT
	FastAction_2_tF681F2B4D7F3C436D0CF8815F6CDFDE0ADA898BD * ___FONT_PROPERTY_EVENT_2;
	// TMPro.FastAction`2<System.Boolean,UnityEngine.Object> TMPro.TMPro_EventManager::SPRITE_ASSET_PROPERTY_EVENT
	FastAction_2_tF0254094C7F4F29971E256E3E0D897BF5EEA3A92 * ___SPRITE_ASSET_PROPERTY_EVENT_3;
	// TMPro.FastAction`2<System.Boolean,TMPro.TextMeshPro> TMPro.TMPro_EventManager::TEXTMESHPRO_PROPERTY_EVENT
	FastAction_2_tEB3735AAFBD827896E679E1D4C16C43B5BADAE54 * ___TEXTMESHPRO_PROPERTY_EVENT_4;
	// TMPro.FastAction`3<UnityEngine.GameObject,UnityEngine.Material,UnityEngine.Material> TMPro.TMPro_EventManager::DRAG_AND_DROP_MATERIAL_EVENT
	FastAction_3_t2EB8ADF78F86E27B29B67AA5A8A75B91C797D3D4 * ___DRAG_AND_DROP_MATERIAL_EVENT_5;
	// TMPro.FastAction`1<System.Boolean> TMPro.TMPro_EventManager::TEXT_STYLE_PROPERTY_EVENT
	FastAction_1_tCACDB1FC73FEE322BC04133C55C45D20EBBA8C8E * ___TEXT_STYLE_PROPERTY_EVENT_6;
	// TMPro.FastAction`1<TMPro.TMP_ColorGradient> TMPro.TMPro_EventManager::COLOR_GRADIENT_PROPERTY_EVENT
	FastAction_1_t1380158AFE3DBBC197AFE5E2C45A2B50E03B25F9 * ___COLOR_GRADIENT_PROPERTY_EVENT_7;
	// TMPro.FastAction TMPro.TMPro_EventManager::TMP_SETTINGS_PROPERTY_EVENT
	FastAction_t270E4D4A1102DD4C6E4D0634A4CBBD052A5D91FB * ___TMP_SETTINGS_PROPERTY_EVENT_8;
	// TMPro.FastAction TMPro.TMPro_EventManager::RESOURCE_LOAD_EVENT
	FastAction_t270E4D4A1102DD4C6E4D0634A4CBBD052A5D91FB * ___RESOURCE_LOAD_EVENT_9;
	// TMPro.FastAction`2<System.Boolean,TMPro.TextMeshProUGUI> TMPro.TMPro_EventManager::TEXTMESHPRO_UGUI_PROPERTY_EVENT
	FastAction_2_t7837C9EFA532E6A13072139CAC9CF3412D959945 * ___TEXTMESHPRO_UGUI_PROPERTY_EVENT_10;
	// TMPro.FastAction TMPro.TMPro_EventManager::OnPreRenderObject_Event
	FastAction_t270E4D4A1102DD4C6E4D0634A4CBBD052A5D91FB * ___OnPreRenderObject_Event_11;
	// TMPro.FastAction`1<UnityEngine.Object> TMPro.TMPro_EventManager::TEXT_CHANGED_EVENT
	FastAction_1_t4B3FBD9A28CE7B1C5AB15A8A9087AE92B7A28E09 * ___TEXT_CHANGED_EVENT_12;

public:
	inline static int32_t get_offset_of_COMPUTE_DT_EVENT_0() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields, ___COMPUTE_DT_EVENT_0)); }
	inline FastAction_2_tDD9D6A1F9834B71AC977D63B59F88637BC645E60 * get_COMPUTE_DT_EVENT_0() const { return ___COMPUTE_DT_EVENT_0; }
	inline FastAction_2_tDD9D6A1F9834B71AC977D63B59F88637BC645E60 ** get_address_of_COMPUTE_DT_EVENT_0() { return &___COMPUTE_DT_EVENT_0; }
	inline void set_COMPUTE_DT_EVENT_0(FastAction_2_tDD9D6A1F9834B71AC977D63B59F88637BC645E60 * value)
	{
		___COMPUTE_DT_EVENT_0 = value;
		Il2CppCodeGenWriteBarrier((&___COMPUTE_DT_EVENT_0), value);
	}

	inline static int32_t get_offset_of_MATERIAL_PROPERTY_EVENT_1() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields, ___MATERIAL_PROPERTY_EVENT_1)); }
	inline FastAction_2_tA69AED04E5299BAE9D8184243189E851F4080707 * get_MATERIAL_PROPERTY_EVENT_1() const { return ___MATERIAL_PROPERTY_EVENT_1; }
	inline FastAction_2_tA69AED04E5299BAE9D8184243189E851F4080707 ** get_address_of_MATERIAL_PROPERTY_EVENT_1() { return &___MATERIAL_PROPERTY_EVENT_1; }
	inline void set_MATERIAL_PROPERTY_EVENT_1(FastAction_2_tA69AED04E5299BAE9D8184243189E851F4080707 * value)
	{
		___MATERIAL_PROPERTY_EVENT_1 = value;
		Il2CppCodeGenWriteBarrier((&___MATERIAL_PROPERTY_EVENT_1), value);
	}

	inline static int32_t get_offset_of_FONT_PROPERTY_EVENT_2() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields, ___FONT_PROPERTY_EVENT_2)); }
	inline FastAction_2_tF681F2B4D7F3C436D0CF8815F6CDFDE0ADA898BD * get_FONT_PROPERTY_EVENT_2() const { return ___FONT_PROPERTY_EVENT_2; }
	inline FastAction_2_tF681F2B4D7F3C436D0CF8815F6CDFDE0ADA898BD ** get_address_of_FONT_PROPERTY_EVENT_2() { return &___FONT_PROPERTY_EVENT_2; }
	inline void set_FONT_PROPERTY_EVENT_2(FastAction_2_tF681F2B4D7F3C436D0CF8815F6CDFDE0ADA898BD * value)
	{
		___FONT_PROPERTY_EVENT_2 = value;
		Il2CppCodeGenWriteBarrier((&___FONT_PROPERTY_EVENT_2), value);
	}

	inline static int32_t get_offset_of_SPRITE_ASSET_PROPERTY_EVENT_3() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields, ___SPRITE_ASSET_PROPERTY_EVENT_3)); }
	inline FastAction_2_tF0254094C7F4F29971E256E3E0D897BF5EEA3A92 * get_SPRITE_ASSET_PROPERTY_EVENT_3() const { return ___SPRITE_ASSET_PROPERTY_EVENT_3; }
	inline FastAction_2_tF0254094C7F4F29971E256E3E0D897BF5EEA3A92 ** get_address_of_SPRITE_ASSET_PROPERTY_EVENT_3() { return &___SPRITE_ASSET_PROPERTY_EVENT_3; }
	inline void set_SPRITE_ASSET_PROPERTY_EVENT_3(FastAction_2_tF0254094C7F4F29971E256E3E0D897BF5EEA3A92 * value)
	{
		___SPRITE_ASSET_PROPERTY_EVENT_3 = value;
		Il2CppCodeGenWriteBarrier((&___SPRITE_ASSET_PROPERTY_EVENT_3), value);
	}

	inline static int32_t get_offset_of_TEXTMESHPRO_PROPERTY_EVENT_4() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields, ___TEXTMESHPRO_PROPERTY_EVENT_4)); }
	inline FastAction_2_tEB3735AAFBD827896E679E1D4C16C43B5BADAE54 * get_TEXTMESHPRO_PROPERTY_EVENT_4() const { return ___TEXTMESHPRO_PROPERTY_EVENT_4; }
	inline FastAction_2_tEB3735AAFBD827896E679E1D4C16C43B5BADAE54 ** get_address_of_TEXTMESHPRO_PROPERTY_EVENT_4() { return &___TEXTMESHPRO_PROPERTY_EVENT_4; }
	inline void set_TEXTMESHPRO_PROPERTY_EVENT_4(FastAction_2_tEB3735AAFBD827896E679E1D4C16C43B5BADAE54 * value)
	{
		___TEXTMESHPRO_PROPERTY_EVENT_4 = value;
		Il2CppCodeGenWriteBarrier((&___TEXTMESHPRO_PROPERTY_EVENT_4), value);
	}

	inline static int32_t get_offset_of_DRAG_AND_DROP_MATERIAL_EVENT_5() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields, ___DRAG_AND_DROP_MATERIAL_EVENT_5)); }
	inline FastAction_3_t2EB8ADF78F86E27B29B67AA5A8A75B91C797D3D4 * get_DRAG_AND_DROP_MATERIAL_EVENT_5() const { return ___DRAG_AND_DROP_MATERIAL_EVENT_5; }
	inline FastAction_3_t2EB8ADF78F86E27B29B67AA5A8A75B91C797D3D4 ** get_address_of_DRAG_AND_DROP_MATERIAL_EVENT_5() { return &___DRAG_AND_DROP_MATERIAL_EVENT_5; }
	inline void set_DRAG_AND_DROP_MATERIAL_EVENT_5(FastAction_3_t2EB8ADF78F86E27B29B67AA5A8A75B91C797D3D4 * value)
	{
		___DRAG_AND_DROP_MATERIAL_EVENT_5 = value;
		Il2CppCodeGenWriteBarrier((&___DRAG_AND_DROP_MATERIAL_EVENT_5), value);
	}

	inline static int32_t get_offset_of_TEXT_STYLE_PROPERTY_EVENT_6() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields, ___TEXT_STYLE_PROPERTY_EVENT_6)); }
	inline FastAction_1_tCACDB1FC73FEE322BC04133C55C45D20EBBA8C8E * get_TEXT_STYLE_PROPERTY_EVENT_6() const { return ___TEXT_STYLE_PROPERTY_EVENT_6; }
	inline FastAction_1_tCACDB1FC73FEE322BC04133C55C45D20EBBA8C8E ** get_address_of_TEXT_STYLE_PROPERTY_EVENT_6() { return &___TEXT_STYLE_PROPERTY_EVENT_6; }
	inline void set_TEXT_STYLE_PROPERTY_EVENT_6(FastAction_1_tCACDB1FC73FEE322BC04133C55C45D20EBBA8C8E * value)
	{
		___TEXT_STYLE_PROPERTY_EVENT_6 = value;
		Il2CppCodeGenWriteBarrier((&___TEXT_STYLE_PROPERTY_EVENT_6), value);
	}

	inline static int32_t get_offset_of_COLOR_GRADIENT_PROPERTY_EVENT_7() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields, ___COLOR_GRADIENT_PROPERTY_EVENT_7)); }
	inline FastAction_1_t1380158AFE3DBBC197AFE5E2C45A2B50E03B25F9 * get_COLOR_GRADIENT_PROPERTY_EVENT_7() const { return ___COLOR_GRADIENT_PROPERTY_EVENT_7; }
	inline FastAction_1_t1380158AFE3DBBC197AFE5E2C45A2B50E03B25F9 ** get_address_of_COLOR_GRADIENT_PROPERTY_EVENT_7() { return &___COLOR_GRADIENT_PROPERTY_EVENT_7; }
	inline void set_COLOR_GRADIENT_PROPERTY_EVENT_7(FastAction_1_t1380158AFE3DBBC197AFE5E2C45A2B50E03B25F9 * value)
	{
		___COLOR_GRADIENT_PROPERTY_EVENT_7 = value;
		Il2CppCodeGenWriteBarrier((&___COLOR_GRADIENT_PROPERTY_EVENT_7), value);
	}

	inline static int32_t get_offset_of_TMP_SETTINGS_PROPERTY_EVENT_8() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields, ___TMP_SETTINGS_PROPERTY_EVENT_8)); }
	inline FastAction_t270E4D4A1102DD4C6E4D0634A4CBBD052A5D91FB * get_TMP_SETTINGS_PROPERTY_EVENT_8() const { return ___TMP_SETTINGS_PROPERTY_EVENT_8; }
	inline FastAction_t270E4D4A1102DD4C6E4D0634A4CBBD052A5D91FB ** get_address_of_TMP_SETTINGS_PROPERTY_EVENT_8() { return &___TMP_SETTINGS_PROPERTY_EVENT_8; }
	inline void set_TMP_SETTINGS_PROPERTY_EVENT_8(FastAction_t270E4D4A1102DD4C6E4D0634A4CBBD052A5D91FB * value)
	{
		___TMP_SETTINGS_PROPERTY_EVENT_8 = value;
		Il2CppCodeGenWriteBarrier((&___TMP_SETTINGS_PROPERTY_EVENT_8), value);
	}

	inline static int32_t get_offset_of_RESOURCE_LOAD_EVENT_9() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields, ___RESOURCE_LOAD_EVENT_9)); }
	inline FastAction_t270E4D4A1102DD4C6E4D0634A4CBBD052A5D91FB * get_RESOURCE_LOAD_EVENT_9() const { return ___RESOURCE_LOAD_EVENT_9; }
	inline FastAction_t270E4D4A1102DD4C6E4D0634A4CBBD052A5D91FB ** get_address_of_RESOURCE_LOAD_EVENT_9() { return &___RESOURCE_LOAD_EVENT_9; }
	inline void set_RESOURCE_LOAD_EVENT_9(FastAction_t270E4D4A1102DD4C6E4D0634A4CBBD052A5D91FB * value)
	{
		___RESOURCE_LOAD_EVENT_9 = value;
		Il2CppCodeGenWriteBarrier((&___RESOURCE_LOAD_EVENT_9), value);
	}

	inline static int32_t get_offset_of_TEXTMESHPRO_UGUI_PROPERTY_EVENT_10() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields, ___TEXTMESHPRO_UGUI_PROPERTY_EVENT_10)); }
	inline FastAction_2_t7837C9EFA532E6A13072139CAC9CF3412D959945 * get_TEXTMESHPRO_UGUI_PROPERTY_EVENT_10() const { return ___TEXTMESHPRO_UGUI_PROPERTY_EVENT_10; }
	inline FastAction_2_t7837C9EFA532E6A13072139CAC9CF3412D959945 ** get_address_of_TEXTMESHPRO_UGUI_PROPERTY_EVENT_10() { return &___TEXTMESHPRO_UGUI_PROPERTY_EVENT_10; }
	inline void set_TEXTMESHPRO_UGUI_PROPERTY_EVENT_10(FastAction_2_t7837C9EFA532E6A13072139CAC9CF3412D959945 * value)
	{
		___TEXTMESHPRO_UGUI_PROPERTY_EVENT_10 = value;
		Il2CppCodeGenWriteBarrier((&___TEXTMESHPRO_UGUI_PROPERTY_EVENT_10), value);
	}

	inline static int32_t get_offset_of_OnPreRenderObject_Event_11() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields, ___OnPreRenderObject_Event_11)); }
	inline FastAction_t270E4D4A1102DD4C6E4D0634A4CBBD052A5D91FB * get_OnPreRenderObject_Event_11() const { return ___OnPreRenderObject_Event_11; }
	inline FastAction_t270E4D4A1102DD4C6E4D0634A4CBBD052A5D91FB ** get_address_of_OnPreRenderObject_Event_11() { return &___OnPreRenderObject_Event_11; }
	inline void set_OnPreRenderObject_Event_11(FastAction_t270E4D4A1102DD4C6E4D0634A4CBBD052A5D91FB * value)
	{
		___OnPreRenderObject_Event_11 = value;
		Il2CppCodeGenWriteBarrier((&___OnPreRenderObject_Event_11), value);
	}

	inline static int32_t get_offset_of_TEXT_CHANGED_EVENT_12() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields, ___TEXT_CHANGED_EVENT_12)); }
	inline FastAction_1_t4B3FBD9A28CE7B1C5AB15A8A9087AE92B7A28E09 * get_TEXT_CHANGED_EVENT_12() const { return ___TEXT_CHANGED_EVENT_12; }
	inline FastAction_1_t4B3FBD9A28CE7B1C5AB15A8A9087AE92B7A28E09 ** get_address_of_TEXT_CHANGED_EVENT_12() { return &___TEXT_CHANGED_EVENT_12; }
	inline void set_TEXT_CHANGED_EVENT_12(FastAction_1_t4B3FBD9A28CE7B1C5AB15A8A9087AE92B7A28E09 * value)
	{
		___TEXT_CHANGED_EVENT_12 = value;
		Il2CppCodeGenWriteBarrier((&___TEXT_CHANGED_EVENT_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMPRO_EVENTMANAGER_T0831C7F02A59F06755AFFF94AAF826EEE77E516D_H
#ifndef TMPRO_EXTENSIONMETHODS_T2A24D41EAD2F3568C5617941DE7558790B9B1835_H
#define TMPRO_EXTENSIONMETHODS_T2A24D41EAD2F3568C5617941DE7558790B9B1835_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMPro_ExtensionMethods
struct  TMPro_ExtensionMethods_t2A24D41EAD2F3568C5617941DE7558790B9B1835  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMPRO_EXTENSIONMETHODS_T2A24D41EAD2F3568C5617941DE7558790B9B1835_H
#ifndef CAMERASTATE_T67204999380EA316F8E1B6C902D4330646DE6AE7_H
#define CAMERASTATE_T67204999380EA316F8E1B6C902D4330646DE6AE7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityTemplateProjects.SimpleCameraController/CameraState
struct  CameraState_t67204999380EA316F8E1B6C902D4330646DE6AE7  : public RuntimeObject
{
public:
	// System.Single UnityTemplateProjects.SimpleCameraController/CameraState::yaw
	float ___yaw_0;
	// System.Single UnityTemplateProjects.SimpleCameraController/CameraState::pitch
	float ___pitch_1;
	// System.Single UnityTemplateProjects.SimpleCameraController/CameraState::roll
	float ___roll_2;
	// System.Single UnityTemplateProjects.SimpleCameraController/CameraState::x
	float ___x_3;
	// System.Single UnityTemplateProjects.SimpleCameraController/CameraState::y
	float ___y_4;
	// System.Single UnityTemplateProjects.SimpleCameraController/CameraState::z
	float ___z_5;

public:
	inline static int32_t get_offset_of_yaw_0() { return static_cast<int32_t>(offsetof(CameraState_t67204999380EA316F8E1B6C902D4330646DE6AE7, ___yaw_0)); }
	inline float get_yaw_0() const { return ___yaw_0; }
	inline float* get_address_of_yaw_0() { return &___yaw_0; }
	inline void set_yaw_0(float value)
	{
		___yaw_0 = value;
	}

	inline static int32_t get_offset_of_pitch_1() { return static_cast<int32_t>(offsetof(CameraState_t67204999380EA316F8E1B6C902D4330646DE6AE7, ___pitch_1)); }
	inline float get_pitch_1() const { return ___pitch_1; }
	inline float* get_address_of_pitch_1() { return &___pitch_1; }
	inline void set_pitch_1(float value)
	{
		___pitch_1 = value;
	}

	inline static int32_t get_offset_of_roll_2() { return static_cast<int32_t>(offsetof(CameraState_t67204999380EA316F8E1B6C902D4330646DE6AE7, ___roll_2)); }
	inline float get_roll_2() const { return ___roll_2; }
	inline float* get_address_of_roll_2() { return &___roll_2; }
	inline void set_roll_2(float value)
	{
		___roll_2 = value;
	}

	inline static int32_t get_offset_of_x_3() { return static_cast<int32_t>(offsetof(CameraState_t67204999380EA316F8E1B6C902D4330646DE6AE7, ___x_3)); }
	inline float get_x_3() const { return ___x_3; }
	inline float* get_address_of_x_3() { return &___x_3; }
	inline void set_x_3(float value)
	{
		___x_3 = value;
	}

	inline static int32_t get_offset_of_y_4() { return static_cast<int32_t>(offsetof(CameraState_t67204999380EA316F8E1B6C902D4330646DE6AE7, ___y_4)); }
	inline float get_y_4() const { return ___y_4; }
	inline float* get_address_of_y_4() { return &___y_4; }
	inline void set_y_4(float value)
	{
		___y_4 = value;
	}

	inline static int32_t get_offset_of_z_5() { return static_cast<int32_t>(offsetof(CameraState_t67204999380EA316F8E1B6C902D4330646DE6AE7, ___z_5)); }
	inline float get_z_5() const { return ___z_5; }
	inline float* get_address_of_z_5() { return &___z_5; }
	inline void set_z_5(float value)
	{
		___z_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERASTATE_T67204999380EA316F8E1B6C902D4330646DE6AE7_H
#ifndef U24ARRAYTYPEU3D12_TBFF9DDDDE6A030848A3B927BF2701B3B97F56317_H
#define U24ARRAYTYPEU3D12_TBFF9DDDDE6A030848A3B927BF2701B3B97F56317_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=12
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D12_tBFF9DDDDE6A030848A3B927BF2701B3B97F56317 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D12_tBFF9DDDDE6A030848A3B927BF2701B3B97F56317__padding[12];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D12_TBFF9DDDDE6A030848A3B927BF2701B3B97F56317_H
#ifndef U24ARRAYTYPEU3D40_TD3EB1EDB8340E3E2EFCDE746D3F6D7664617AA91_H
#define U24ARRAYTYPEU3D40_TD3EB1EDB8340E3E2EFCDE746D3F6D7664617AA91_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=40
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D40_tD3EB1EDB8340E3E2EFCDE746D3F6D7664617AA91 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D40_tD3EB1EDB8340E3E2EFCDE746D3F6D7664617AA91__padding[40];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D40_TD3EB1EDB8340E3E2EFCDE746D3F6D7664617AA91_H
#ifndef ENUM_T5AAC444DFCAA78411386665A25FE3CD3169879EF_H
#define ENUM_T5AAC444DFCAA78411386665A25FE3CD3169879EF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t5AAC444DFCAA78411386665A25FE3CD3169879EF  : public ValueType_t1810BD84E0FDB5D3A7CD34286A5B22F343995C9C
{
public:

public:
};

struct Enum_t5AAC444DFCAA78411386665A25FE3CD3169879EF_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t5AAC444DFCAA78411386665A25FE3CD3169879EF_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t5AAC444DFCAA78411386665A25FE3CD3169879EF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t5AAC444DFCAA78411386665A25FE3CD3169879EF_marshaled_com
{
};
#endif // ENUM_T5AAC444DFCAA78411386665A25FE3CD3169879EF_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef FONTASSETCREATIONSETTINGS_TC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4_H
#define FONTASSETCREATIONSETTINGS_TC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.FontAssetCreationSettings
struct  FontAssetCreationSettings_tC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4 
{
public:
	// System.String TMPro.FontAssetCreationSettings::sourceFontFileName
	String_t* ___sourceFontFileName_0;
	// System.String TMPro.FontAssetCreationSettings::sourceFontFileGUID
	String_t* ___sourceFontFileGUID_1;
	// System.Int32 TMPro.FontAssetCreationSettings::pointSizeSamplingMode
	int32_t ___pointSizeSamplingMode_2;
	// System.Int32 TMPro.FontAssetCreationSettings::pointSize
	int32_t ___pointSize_3;
	// System.Int32 TMPro.FontAssetCreationSettings::padding
	int32_t ___padding_4;
	// System.Int32 TMPro.FontAssetCreationSettings::packingMode
	int32_t ___packingMode_5;
	// System.Int32 TMPro.FontAssetCreationSettings::atlasWidth
	int32_t ___atlasWidth_6;
	// System.Int32 TMPro.FontAssetCreationSettings::atlasHeight
	int32_t ___atlasHeight_7;
	// System.Int32 TMPro.FontAssetCreationSettings::characterSetSelectionMode
	int32_t ___characterSetSelectionMode_8;
	// System.String TMPro.FontAssetCreationSettings::characterSequence
	String_t* ___characterSequence_9;
	// System.String TMPro.FontAssetCreationSettings::referencedFontAssetGUID
	String_t* ___referencedFontAssetGUID_10;
	// System.String TMPro.FontAssetCreationSettings::referencedTextAssetGUID
	String_t* ___referencedTextAssetGUID_11;
	// System.Int32 TMPro.FontAssetCreationSettings::fontStyle
	int32_t ___fontStyle_12;
	// System.Single TMPro.FontAssetCreationSettings::fontStyleModifier
	float ___fontStyleModifier_13;
	// System.Int32 TMPro.FontAssetCreationSettings::renderMode
	int32_t ___renderMode_14;
	// System.Boolean TMPro.FontAssetCreationSettings::includeFontFeatures
	bool ___includeFontFeatures_15;

public:
	inline static int32_t get_offset_of_sourceFontFileName_0() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_tC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4, ___sourceFontFileName_0)); }
	inline String_t* get_sourceFontFileName_0() const { return ___sourceFontFileName_0; }
	inline String_t** get_address_of_sourceFontFileName_0() { return &___sourceFontFileName_0; }
	inline void set_sourceFontFileName_0(String_t* value)
	{
		___sourceFontFileName_0 = value;
		Il2CppCodeGenWriteBarrier((&___sourceFontFileName_0), value);
	}

	inline static int32_t get_offset_of_sourceFontFileGUID_1() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_tC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4, ___sourceFontFileGUID_1)); }
	inline String_t* get_sourceFontFileGUID_1() const { return ___sourceFontFileGUID_1; }
	inline String_t** get_address_of_sourceFontFileGUID_1() { return &___sourceFontFileGUID_1; }
	inline void set_sourceFontFileGUID_1(String_t* value)
	{
		___sourceFontFileGUID_1 = value;
		Il2CppCodeGenWriteBarrier((&___sourceFontFileGUID_1), value);
	}

	inline static int32_t get_offset_of_pointSizeSamplingMode_2() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_tC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4, ___pointSizeSamplingMode_2)); }
	inline int32_t get_pointSizeSamplingMode_2() const { return ___pointSizeSamplingMode_2; }
	inline int32_t* get_address_of_pointSizeSamplingMode_2() { return &___pointSizeSamplingMode_2; }
	inline void set_pointSizeSamplingMode_2(int32_t value)
	{
		___pointSizeSamplingMode_2 = value;
	}

	inline static int32_t get_offset_of_pointSize_3() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_tC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4, ___pointSize_3)); }
	inline int32_t get_pointSize_3() const { return ___pointSize_3; }
	inline int32_t* get_address_of_pointSize_3() { return &___pointSize_3; }
	inline void set_pointSize_3(int32_t value)
	{
		___pointSize_3 = value;
	}

	inline static int32_t get_offset_of_padding_4() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_tC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4, ___padding_4)); }
	inline int32_t get_padding_4() const { return ___padding_4; }
	inline int32_t* get_address_of_padding_4() { return &___padding_4; }
	inline void set_padding_4(int32_t value)
	{
		___padding_4 = value;
	}

	inline static int32_t get_offset_of_packingMode_5() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_tC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4, ___packingMode_5)); }
	inline int32_t get_packingMode_5() const { return ___packingMode_5; }
	inline int32_t* get_address_of_packingMode_5() { return &___packingMode_5; }
	inline void set_packingMode_5(int32_t value)
	{
		___packingMode_5 = value;
	}

	inline static int32_t get_offset_of_atlasWidth_6() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_tC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4, ___atlasWidth_6)); }
	inline int32_t get_atlasWidth_6() const { return ___atlasWidth_6; }
	inline int32_t* get_address_of_atlasWidth_6() { return &___atlasWidth_6; }
	inline void set_atlasWidth_6(int32_t value)
	{
		___atlasWidth_6 = value;
	}

	inline static int32_t get_offset_of_atlasHeight_7() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_tC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4, ___atlasHeight_7)); }
	inline int32_t get_atlasHeight_7() const { return ___atlasHeight_7; }
	inline int32_t* get_address_of_atlasHeight_7() { return &___atlasHeight_7; }
	inline void set_atlasHeight_7(int32_t value)
	{
		___atlasHeight_7 = value;
	}

	inline static int32_t get_offset_of_characterSetSelectionMode_8() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_tC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4, ___characterSetSelectionMode_8)); }
	inline int32_t get_characterSetSelectionMode_8() const { return ___characterSetSelectionMode_8; }
	inline int32_t* get_address_of_characterSetSelectionMode_8() { return &___characterSetSelectionMode_8; }
	inline void set_characterSetSelectionMode_8(int32_t value)
	{
		___characterSetSelectionMode_8 = value;
	}

	inline static int32_t get_offset_of_characterSequence_9() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_tC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4, ___characterSequence_9)); }
	inline String_t* get_characterSequence_9() const { return ___characterSequence_9; }
	inline String_t** get_address_of_characterSequence_9() { return &___characterSequence_9; }
	inline void set_characterSequence_9(String_t* value)
	{
		___characterSequence_9 = value;
		Il2CppCodeGenWriteBarrier((&___characterSequence_9), value);
	}

	inline static int32_t get_offset_of_referencedFontAssetGUID_10() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_tC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4, ___referencedFontAssetGUID_10)); }
	inline String_t* get_referencedFontAssetGUID_10() const { return ___referencedFontAssetGUID_10; }
	inline String_t** get_address_of_referencedFontAssetGUID_10() { return &___referencedFontAssetGUID_10; }
	inline void set_referencedFontAssetGUID_10(String_t* value)
	{
		___referencedFontAssetGUID_10 = value;
		Il2CppCodeGenWriteBarrier((&___referencedFontAssetGUID_10), value);
	}

	inline static int32_t get_offset_of_referencedTextAssetGUID_11() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_tC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4, ___referencedTextAssetGUID_11)); }
	inline String_t* get_referencedTextAssetGUID_11() const { return ___referencedTextAssetGUID_11; }
	inline String_t** get_address_of_referencedTextAssetGUID_11() { return &___referencedTextAssetGUID_11; }
	inline void set_referencedTextAssetGUID_11(String_t* value)
	{
		___referencedTextAssetGUID_11 = value;
		Il2CppCodeGenWriteBarrier((&___referencedTextAssetGUID_11), value);
	}

	inline static int32_t get_offset_of_fontStyle_12() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_tC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4, ___fontStyle_12)); }
	inline int32_t get_fontStyle_12() const { return ___fontStyle_12; }
	inline int32_t* get_address_of_fontStyle_12() { return &___fontStyle_12; }
	inline void set_fontStyle_12(int32_t value)
	{
		___fontStyle_12 = value;
	}

	inline static int32_t get_offset_of_fontStyleModifier_13() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_tC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4, ___fontStyleModifier_13)); }
	inline float get_fontStyleModifier_13() const { return ___fontStyleModifier_13; }
	inline float* get_address_of_fontStyleModifier_13() { return &___fontStyleModifier_13; }
	inline void set_fontStyleModifier_13(float value)
	{
		___fontStyleModifier_13 = value;
	}

	inline static int32_t get_offset_of_renderMode_14() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_tC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4, ___renderMode_14)); }
	inline int32_t get_renderMode_14() const { return ___renderMode_14; }
	inline int32_t* get_address_of_renderMode_14() { return &___renderMode_14; }
	inline void set_renderMode_14(int32_t value)
	{
		___renderMode_14 = value;
	}

	inline static int32_t get_offset_of_includeFontFeatures_15() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_tC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4, ___includeFontFeatures_15)); }
	inline bool get_includeFontFeatures_15() const { return ___includeFontFeatures_15; }
	inline bool* get_address_of_includeFontFeatures_15() { return &___includeFontFeatures_15; }
	inline void set_includeFontFeatures_15(bool value)
	{
		___includeFontFeatures_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.FontAssetCreationSettings
struct FontAssetCreationSettings_tC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4_marshaled_pinvoke
{
	char* ___sourceFontFileName_0;
	char* ___sourceFontFileGUID_1;
	int32_t ___pointSizeSamplingMode_2;
	int32_t ___pointSize_3;
	int32_t ___padding_4;
	int32_t ___packingMode_5;
	int32_t ___atlasWidth_6;
	int32_t ___atlasHeight_7;
	int32_t ___characterSetSelectionMode_8;
	char* ___characterSequence_9;
	char* ___referencedFontAssetGUID_10;
	char* ___referencedTextAssetGUID_11;
	int32_t ___fontStyle_12;
	float ___fontStyleModifier_13;
	int32_t ___renderMode_14;
	int32_t ___includeFontFeatures_15;
};
// Native definition for COM marshalling of TMPro.FontAssetCreationSettings
struct FontAssetCreationSettings_tC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4_marshaled_com
{
	Il2CppChar* ___sourceFontFileName_0;
	Il2CppChar* ___sourceFontFileGUID_1;
	int32_t ___pointSizeSamplingMode_2;
	int32_t ___pointSize_3;
	int32_t ___padding_4;
	int32_t ___packingMode_5;
	int32_t ___atlasWidth_6;
	int32_t ___atlasHeight_7;
	int32_t ___characterSetSelectionMode_8;
	Il2CppChar* ___characterSequence_9;
	Il2CppChar* ___referencedFontAssetGUID_10;
	Il2CppChar* ___referencedTextAssetGUID_11;
	int32_t ___fontStyle_12;
	float ___fontStyleModifier_13;
	int32_t ___renderMode_14;
	int32_t ___includeFontFeatures_15;
};
#endif // FONTASSETCREATIONSETTINGS_TC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4_H
#ifndef GLYPHVALUERECORD_T978E7932FA4A5D424E6CFBB65E98B16D02BE18DD_H
#define GLYPHVALUERECORD_T978E7932FA4A5D424E6CFBB65E98B16D02BE18DD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.GlyphValueRecord
struct  GlyphValueRecord_t978E7932FA4A5D424E6CFBB65E98B16D02BE18DD 
{
public:
	// System.Single TMPro.GlyphValueRecord::xPlacement
	float ___xPlacement_0;
	// System.Single TMPro.GlyphValueRecord::yPlacement
	float ___yPlacement_1;
	// System.Single TMPro.GlyphValueRecord::xAdvance
	float ___xAdvance_2;
	// System.Single TMPro.GlyphValueRecord::yAdvance
	float ___yAdvance_3;

public:
	inline static int32_t get_offset_of_xPlacement_0() { return static_cast<int32_t>(offsetof(GlyphValueRecord_t978E7932FA4A5D424E6CFBB65E98B16D02BE18DD, ___xPlacement_0)); }
	inline float get_xPlacement_0() const { return ___xPlacement_0; }
	inline float* get_address_of_xPlacement_0() { return &___xPlacement_0; }
	inline void set_xPlacement_0(float value)
	{
		___xPlacement_0 = value;
	}

	inline static int32_t get_offset_of_yPlacement_1() { return static_cast<int32_t>(offsetof(GlyphValueRecord_t978E7932FA4A5D424E6CFBB65E98B16D02BE18DD, ___yPlacement_1)); }
	inline float get_yPlacement_1() const { return ___yPlacement_1; }
	inline float* get_address_of_yPlacement_1() { return &___yPlacement_1; }
	inline void set_yPlacement_1(float value)
	{
		___yPlacement_1 = value;
	}

	inline static int32_t get_offset_of_xAdvance_2() { return static_cast<int32_t>(offsetof(GlyphValueRecord_t978E7932FA4A5D424E6CFBB65E98B16D02BE18DD, ___xAdvance_2)); }
	inline float get_xAdvance_2() const { return ___xAdvance_2; }
	inline float* get_address_of_xAdvance_2() { return &___xAdvance_2; }
	inline void set_xAdvance_2(float value)
	{
		___xAdvance_2 = value;
	}

	inline static int32_t get_offset_of_yAdvance_3() { return static_cast<int32_t>(offsetof(GlyphValueRecord_t978E7932FA4A5D424E6CFBB65E98B16D02BE18DD, ___yAdvance_3)); }
	inline float get_yAdvance_3() const { return ___yAdvance_3; }
	inline float* get_address_of_yAdvance_3() { return &___yAdvance_3; }
	inline void set_yAdvance_3(float value)
	{
		___yAdvance_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLYPHVALUERECORD_T978E7932FA4A5D424E6CFBB65E98B16D02BE18DD_H
#ifndef KERNINGPAIRKEY_TE33705E4E3EF2AA3DB9BCAA0AD581A46A70135D3_H
#define KERNINGPAIRKEY_TE33705E4E3EF2AA3DB9BCAA0AD581A46A70135D3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.KerningPairKey
struct  KerningPairKey_tE33705E4E3EF2AA3DB9BCAA0AD581A46A70135D3 
{
public:
	// System.UInt32 TMPro.KerningPairKey::ascii_Left
	uint32_t ___ascii_Left_0;
	// System.UInt32 TMPro.KerningPairKey::ascii_Right
	uint32_t ___ascii_Right_1;
	// System.UInt32 TMPro.KerningPairKey::key
	uint32_t ___key_2;

public:
	inline static int32_t get_offset_of_ascii_Left_0() { return static_cast<int32_t>(offsetof(KerningPairKey_tE33705E4E3EF2AA3DB9BCAA0AD581A46A70135D3, ___ascii_Left_0)); }
	inline uint32_t get_ascii_Left_0() const { return ___ascii_Left_0; }
	inline uint32_t* get_address_of_ascii_Left_0() { return &___ascii_Left_0; }
	inline void set_ascii_Left_0(uint32_t value)
	{
		___ascii_Left_0 = value;
	}

	inline static int32_t get_offset_of_ascii_Right_1() { return static_cast<int32_t>(offsetof(KerningPairKey_tE33705E4E3EF2AA3DB9BCAA0AD581A46A70135D3, ___ascii_Right_1)); }
	inline uint32_t get_ascii_Right_1() const { return ___ascii_Right_1; }
	inline uint32_t* get_address_of_ascii_Right_1() { return &___ascii_Right_1; }
	inline void set_ascii_Right_1(uint32_t value)
	{
		___ascii_Right_1 = value;
	}

	inline static int32_t get_offset_of_key_2() { return static_cast<int32_t>(offsetof(KerningPairKey_tE33705E4E3EF2AA3DB9BCAA0AD581A46A70135D3, ___key_2)); }
	inline uint32_t get_key_2() const { return ___key_2; }
	inline uint32_t* get_address_of_key_2() { return &___key_2; }
	inline void set_key_2(uint32_t value)
	{
		___key_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KERNINGPAIRKEY_TE33705E4E3EF2AA3DB9BCAA0AD581A46A70135D3_H
#ifndef MATERIALREFERENCE_TFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F_H
#define MATERIALREFERENCE_TFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.MaterialReference
struct  MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F 
{
public:
	// System.Int32 TMPro.MaterialReference::index
	int32_t ___index_0;
	// TMPro.TMP_FontAsset TMPro.MaterialReference::fontAsset
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___fontAsset_1;
	// TMPro.TMP_SpriteAsset TMPro.MaterialReference::spriteAsset
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * ___spriteAsset_2;
	// UnityEngine.Material TMPro.MaterialReference::material
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material_3;
	// System.Boolean TMPro.MaterialReference::isDefaultMaterial
	bool ___isDefaultMaterial_4;
	// System.Boolean TMPro.MaterialReference::isFallbackMaterial
	bool ___isFallbackMaterial_5;
	// UnityEngine.Material TMPro.MaterialReference::fallbackMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___fallbackMaterial_6;
	// System.Single TMPro.MaterialReference::padding
	float ___padding_7;
	// System.Int32 TMPro.MaterialReference::referenceCount
	int32_t ___referenceCount_8;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_fontAsset_1() { return static_cast<int32_t>(offsetof(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F, ___fontAsset_1)); }
	inline TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * get_fontAsset_1() const { return ___fontAsset_1; }
	inline TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C ** get_address_of_fontAsset_1() { return &___fontAsset_1; }
	inline void set_fontAsset_1(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * value)
	{
		___fontAsset_1 = value;
		Il2CppCodeGenWriteBarrier((&___fontAsset_1), value);
	}

	inline static int32_t get_offset_of_spriteAsset_2() { return static_cast<int32_t>(offsetof(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F, ___spriteAsset_2)); }
	inline TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * get_spriteAsset_2() const { return ___spriteAsset_2; }
	inline TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 ** get_address_of_spriteAsset_2() { return &___spriteAsset_2; }
	inline void set_spriteAsset_2(TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * value)
	{
		___spriteAsset_2 = value;
		Il2CppCodeGenWriteBarrier((&___spriteAsset_2), value);
	}

	inline static int32_t get_offset_of_material_3() { return static_cast<int32_t>(offsetof(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F, ___material_3)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_material_3() const { return ___material_3; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_material_3() { return &___material_3; }
	inline void set_material_3(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___material_3 = value;
		Il2CppCodeGenWriteBarrier((&___material_3), value);
	}

	inline static int32_t get_offset_of_isDefaultMaterial_4() { return static_cast<int32_t>(offsetof(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F, ___isDefaultMaterial_4)); }
	inline bool get_isDefaultMaterial_4() const { return ___isDefaultMaterial_4; }
	inline bool* get_address_of_isDefaultMaterial_4() { return &___isDefaultMaterial_4; }
	inline void set_isDefaultMaterial_4(bool value)
	{
		___isDefaultMaterial_4 = value;
	}

	inline static int32_t get_offset_of_isFallbackMaterial_5() { return static_cast<int32_t>(offsetof(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F, ___isFallbackMaterial_5)); }
	inline bool get_isFallbackMaterial_5() const { return ___isFallbackMaterial_5; }
	inline bool* get_address_of_isFallbackMaterial_5() { return &___isFallbackMaterial_5; }
	inline void set_isFallbackMaterial_5(bool value)
	{
		___isFallbackMaterial_5 = value;
	}

	inline static int32_t get_offset_of_fallbackMaterial_6() { return static_cast<int32_t>(offsetof(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F, ___fallbackMaterial_6)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_fallbackMaterial_6() const { return ___fallbackMaterial_6; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_fallbackMaterial_6() { return &___fallbackMaterial_6; }
	inline void set_fallbackMaterial_6(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___fallbackMaterial_6 = value;
		Il2CppCodeGenWriteBarrier((&___fallbackMaterial_6), value);
	}

	inline static int32_t get_offset_of_padding_7() { return static_cast<int32_t>(offsetof(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F, ___padding_7)); }
	inline float get_padding_7() const { return ___padding_7; }
	inline float* get_address_of_padding_7() { return &___padding_7; }
	inline void set_padding_7(float value)
	{
		___padding_7 = value;
	}

	inline static int32_t get_offset_of_referenceCount_8() { return static_cast<int32_t>(offsetof(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F, ___referenceCount_8)); }
	inline int32_t get_referenceCount_8() const { return ___referenceCount_8; }
	inline int32_t* get_address_of_referenceCount_8() { return &___referenceCount_8; }
	inline void set_referenceCount_8(int32_t value)
	{
		___referenceCount_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.MaterialReference
struct MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F_marshaled_pinvoke
{
	int32_t ___index_0;
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___fontAsset_1;
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * ___spriteAsset_2;
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material_3;
	int32_t ___isDefaultMaterial_4;
	int32_t ___isFallbackMaterial_5;
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___fallbackMaterial_6;
	float ___padding_7;
	int32_t ___referenceCount_8;
};
// Native definition for COM marshalling of TMPro.MaterialReference
struct MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F_marshaled_com
{
	int32_t ___index_0;
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___fontAsset_1;
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * ___spriteAsset_2;
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material_3;
	int32_t ___isDefaultMaterial_4;
	int32_t ___isFallbackMaterial_5;
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___fallbackMaterial_6;
	float ___padding_7;
	int32_t ___referenceCount_8;
};
#endif // MATERIALREFERENCE_TFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F_H
#ifndef TMP_BASICXMLTAGSTACK_TC5F410B9A4A8A8DE6351FA5F0FBF92F8E2CC8BF8_H
#define TMP_BASICXMLTAGSTACK_TC5F410B9A4A8A8DE6351FA5F0FBF92F8E2CC8BF8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_BasicXmlTagStack
struct  TMP_BasicXmlTagStack_tC5F410B9A4A8A8DE6351FA5F0FBF92F8E2CC8BF8 
{
public:
	// System.Byte TMPro.TMP_BasicXmlTagStack::bold
	uint8_t ___bold_0;
	// System.Byte TMPro.TMP_BasicXmlTagStack::italic
	uint8_t ___italic_1;
	// System.Byte TMPro.TMP_BasicXmlTagStack::underline
	uint8_t ___underline_2;
	// System.Byte TMPro.TMP_BasicXmlTagStack::strikethrough
	uint8_t ___strikethrough_3;
	// System.Byte TMPro.TMP_BasicXmlTagStack::highlight
	uint8_t ___highlight_4;
	// System.Byte TMPro.TMP_BasicXmlTagStack::superscript
	uint8_t ___superscript_5;
	// System.Byte TMPro.TMP_BasicXmlTagStack::subscript
	uint8_t ___subscript_6;
	// System.Byte TMPro.TMP_BasicXmlTagStack::uppercase
	uint8_t ___uppercase_7;
	// System.Byte TMPro.TMP_BasicXmlTagStack::lowercase
	uint8_t ___lowercase_8;
	// System.Byte TMPro.TMP_BasicXmlTagStack::smallcaps
	uint8_t ___smallcaps_9;

public:
	inline static int32_t get_offset_of_bold_0() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_tC5F410B9A4A8A8DE6351FA5F0FBF92F8E2CC8BF8, ___bold_0)); }
	inline uint8_t get_bold_0() const { return ___bold_0; }
	inline uint8_t* get_address_of_bold_0() { return &___bold_0; }
	inline void set_bold_0(uint8_t value)
	{
		___bold_0 = value;
	}

	inline static int32_t get_offset_of_italic_1() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_tC5F410B9A4A8A8DE6351FA5F0FBF92F8E2CC8BF8, ___italic_1)); }
	inline uint8_t get_italic_1() const { return ___italic_1; }
	inline uint8_t* get_address_of_italic_1() { return &___italic_1; }
	inline void set_italic_1(uint8_t value)
	{
		___italic_1 = value;
	}

	inline static int32_t get_offset_of_underline_2() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_tC5F410B9A4A8A8DE6351FA5F0FBF92F8E2CC8BF8, ___underline_2)); }
	inline uint8_t get_underline_2() const { return ___underline_2; }
	inline uint8_t* get_address_of_underline_2() { return &___underline_2; }
	inline void set_underline_2(uint8_t value)
	{
		___underline_2 = value;
	}

	inline static int32_t get_offset_of_strikethrough_3() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_tC5F410B9A4A8A8DE6351FA5F0FBF92F8E2CC8BF8, ___strikethrough_3)); }
	inline uint8_t get_strikethrough_3() const { return ___strikethrough_3; }
	inline uint8_t* get_address_of_strikethrough_3() { return &___strikethrough_3; }
	inline void set_strikethrough_3(uint8_t value)
	{
		___strikethrough_3 = value;
	}

	inline static int32_t get_offset_of_highlight_4() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_tC5F410B9A4A8A8DE6351FA5F0FBF92F8E2CC8BF8, ___highlight_4)); }
	inline uint8_t get_highlight_4() const { return ___highlight_4; }
	inline uint8_t* get_address_of_highlight_4() { return &___highlight_4; }
	inline void set_highlight_4(uint8_t value)
	{
		___highlight_4 = value;
	}

	inline static int32_t get_offset_of_superscript_5() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_tC5F410B9A4A8A8DE6351FA5F0FBF92F8E2CC8BF8, ___superscript_5)); }
	inline uint8_t get_superscript_5() const { return ___superscript_5; }
	inline uint8_t* get_address_of_superscript_5() { return &___superscript_5; }
	inline void set_superscript_5(uint8_t value)
	{
		___superscript_5 = value;
	}

	inline static int32_t get_offset_of_subscript_6() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_tC5F410B9A4A8A8DE6351FA5F0FBF92F8E2CC8BF8, ___subscript_6)); }
	inline uint8_t get_subscript_6() const { return ___subscript_6; }
	inline uint8_t* get_address_of_subscript_6() { return &___subscript_6; }
	inline void set_subscript_6(uint8_t value)
	{
		___subscript_6 = value;
	}

	inline static int32_t get_offset_of_uppercase_7() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_tC5F410B9A4A8A8DE6351FA5F0FBF92F8E2CC8BF8, ___uppercase_7)); }
	inline uint8_t get_uppercase_7() const { return ___uppercase_7; }
	inline uint8_t* get_address_of_uppercase_7() { return &___uppercase_7; }
	inline void set_uppercase_7(uint8_t value)
	{
		___uppercase_7 = value;
	}

	inline static int32_t get_offset_of_lowercase_8() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_tC5F410B9A4A8A8DE6351FA5F0FBF92F8E2CC8BF8, ___lowercase_8)); }
	inline uint8_t get_lowercase_8() const { return ___lowercase_8; }
	inline uint8_t* get_address_of_lowercase_8() { return &___lowercase_8; }
	inline void set_lowercase_8(uint8_t value)
	{
		___lowercase_8 = value;
	}

	inline static int32_t get_offset_of_smallcaps_9() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_tC5F410B9A4A8A8DE6351FA5F0FBF92F8E2CC8BF8, ___smallcaps_9)); }
	inline uint8_t get_smallcaps_9() const { return ___smallcaps_9; }
	inline uint8_t* get_address_of_smallcaps_9() { return &___smallcaps_9; }
	inline void set_smallcaps_9(uint8_t value)
	{
		___smallcaps_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_BASICXMLTAGSTACK_TC5F410B9A4A8A8DE6351FA5F0FBF92F8E2CC8BF8_H
#ifndef TMP_GLYPH_TCAA5E0C262A3DAF50537C03D9EA90B51B05BA62C_H
#define TMP_GLYPH_TCAA5E0C262A3DAF50537C03D9EA90B51B05BA62C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Glyph
struct  TMP_Glyph_tCAA5E0C262A3DAF50537C03D9EA90B51B05BA62C  : public TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_GLYPH_TCAA5E0C262A3DAF50537C03D9EA90B51B05BA62C_H
#ifndef TMP_LINKINFO_T7F4B699290A975144DF7094667825BCD52594468_H
#define TMP_LINKINFO_T7F4B699290A975144DF7094667825BCD52594468_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_LinkInfo
struct  TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468 
{
public:
	// TMPro.TMP_Text TMPro.TMP_LinkInfo::textComponent
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * ___textComponent_0;
	// System.Int32 TMPro.TMP_LinkInfo::hashCode
	int32_t ___hashCode_1;
	// System.Int32 TMPro.TMP_LinkInfo::linkIdFirstCharacterIndex
	int32_t ___linkIdFirstCharacterIndex_2;
	// System.Int32 TMPro.TMP_LinkInfo::linkIdLength
	int32_t ___linkIdLength_3;
	// System.Int32 TMPro.TMP_LinkInfo::linkTextfirstCharacterIndex
	int32_t ___linkTextfirstCharacterIndex_4;
	// System.Int32 TMPro.TMP_LinkInfo::linkTextLength
	int32_t ___linkTextLength_5;
	// System.Char[] TMPro.TMP_LinkInfo::linkID
	CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* ___linkID_6;

public:
	inline static int32_t get_offset_of_textComponent_0() { return static_cast<int32_t>(offsetof(TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468, ___textComponent_0)); }
	inline TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * get_textComponent_0() const { return ___textComponent_0; }
	inline TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 ** get_address_of_textComponent_0() { return &___textComponent_0; }
	inline void set_textComponent_0(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * value)
	{
		___textComponent_0 = value;
		Il2CppCodeGenWriteBarrier((&___textComponent_0), value);
	}

	inline static int32_t get_offset_of_hashCode_1() { return static_cast<int32_t>(offsetof(TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468, ___hashCode_1)); }
	inline int32_t get_hashCode_1() const { return ___hashCode_1; }
	inline int32_t* get_address_of_hashCode_1() { return &___hashCode_1; }
	inline void set_hashCode_1(int32_t value)
	{
		___hashCode_1 = value;
	}

	inline static int32_t get_offset_of_linkIdFirstCharacterIndex_2() { return static_cast<int32_t>(offsetof(TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468, ___linkIdFirstCharacterIndex_2)); }
	inline int32_t get_linkIdFirstCharacterIndex_2() const { return ___linkIdFirstCharacterIndex_2; }
	inline int32_t* get_address_of_linkIdFirstCharacterIndex_2() { return &___linkIdFirstCharacterIndex_2; }
	inline void set_linkIdFirstCharacterIndex_2(int32_t value)
	{
		___linkIdFirstCharacterIndex_2 = value;
	}

	inline static int32_t get_offset_of_linkIdLength_3() { return static_cast<int32_t>(offsetof(TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468, ___linkIdLength_3)); }
	inline int32_t get_linkIdLength_3() const { return ___linkIdLength_3; }
	inline int32_t* get_address_of_linkIdLength_3() { return &___linkIdLength_3; }
	inline void set_linkIdLength_3(int32_t value)
	{
		___linkIdLength_3 = value;
	}

	inline static int32_t get_offset_of_linkTextfirstCharacterIndex_4() { return static_cast<int32_t>(offsetof(TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468, ___linkTextfirstCharacterIndex_4)); }
	inline int32_t get_linkTextfirstCharacterIndex_4() const { return ___linkTextfirstCharacterIndex_4; }
	inline int32_t* get_address_of_linkTextfirstCharacterIndex_4() { return &___linkTextfirstCharacterIndex_4; }
	inline void set_linkTextfirstCharacterIndex_4(int32_t value)
	{
		___linkTextfirstCharacterIndex_4 = value;
	}

	inline static int32_t get_offset_of_linkTextLength_5() { return static_cast<int32_t>(offsetof(TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468, ___linkTextLength_5)); }
	inline int32_t get_linkTextLength_5() const { return ___linkTextLength_5; }
	inline int32_t* get_address_of_linkTextLength_5() { return &___linkTextLength_5; }
	inline void set_linkTextLength_5(int32_t value)
	{
		___linkTextLength_5 = value;
	}

	inline static int32_t get_offset_of_linkID_6() { return static_cast<int32_t>(offsetof(TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468, ___linkID_6)); }
	inline CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* get_linkID_6() const { return ___linkID_6; }
	inline CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744** get_address_of_linkID_6() { return &___linkID_6; }
	inline void set_linkID_6(CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* value)
	{
		___linkID_6 = value;
		Il2CppCodeGenWriteBarrier((&___linkID_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.TMP_LinkInfo
struct TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468_marshaled_pinvoke
{
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * ___textComponent_0;
	int32_t ___hashCode_1;
	int32_t ___linkIdFirstCharacterIndex_2;
	int32_t ___linkIdLength_3;
	int32_t ___linkTextfirstCharacterIndex_4;
	int32_t ___linkTextLength_5;
	uint8_t* ___linkID_6;
};
// Native definition for COM marshalling of TMPro.TMP_LinkInfo
struct TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468_marshaled_com
{
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * ___textComponent_0;
	int32_t ___hashCode_1;
	int32_t ___linkIdFirstCharacterIndex_2;
	int32_t ___linkIdLength_3;
	int32_t ___linkTextfirstCharacterIndex_4;
	int32_t ___linkTextLength_5;
	uint8_t* ___linkID_6;
};
#endif // TMP_LINKINFO_T7F4B699290A975144DF7094667825BCD52594468_H
#ifndef TMP_PAGEINFO_T5D305B11116379997CA9649E8D87B3D7162ABB24_H
#define TMP_PAGEINFO_T5D305B11116379997CA9649E8D87B3D7162ABB24_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_PageInfo
struct  TMP_PageInfo_t5D305B11116379997CA9649E8D87B3D7162ABB24 
{
public:
	// System.Int32 TMPro.TMP_PageInfo::firstCharacterIndex
	int32_t ___firstCharacterIndex_0;
	// System.Int32 TMPro.TMP_PageInfo::lastCharacterIndex
	int32_t ___lastCharacterIndex_1;
	// System.Single TMPro.TMP_PageInfo::ascender
	float ___ascender_2;
	// System.Single TMPro.TMP_PageInfo::baseLine
	float ___baseLine_3;
	// System.Single TMPro.TMP_PageInfo::descender
	float ___descender_4;

public:
	inline static int32_t get_offset_of_firstCharacterIndex_0() { return static_cast<int32_t>(offsetof(TMP_PageInfo_t5D305B11116379997CA9649E8D87B3D7162ABB24, ___firstCharacterIndex_0)); }
	inline int32_t get_firstCharacterIndex_0() const { return ___firstCharacterIndex_0; }
	inline int32_t* get_address_of_firstCharacterIndex_0() { return &___firstCharacterIndex_0; }
	inline void set_firstCharacterIndex_0(int32_t value)
	{
		___firstCharacterIndex_0 = value;
	}

	inline static int32_t get_offset_of_lastCharacterIndex_1() { return static_cast<int32_t>(offsetof(TMP_PageInfo_t5D305B11116379997CA9649E8D87B3D7162ABB24, ___lastCharacterIndex_1)); }
	inline int32_t get_lastCharacterIndex_1() const { return ___lastCharacterIndex_1; }
	inline int32_t* get_address_of_lastCharacterIndex_1() { return &___lastCharacterIndex_1; }
	inline void set_lastCharacterIndex_1(int32_t value)
	{
		___lastCharacterIndex_1 = value;
	}

	inline static int32_t get_offset_of_ascender_2() { return static_cast<int32_t>(offsetof(TMP_PageInfo_t5D305B11116379997CA9649E8D87B3D7162ABB24, ___ascender_2)); }
	inline float get_ascender_2() const { return ___ascender_2; }
	inline float* get_address_of_ascender_2() { return &___ascender_2; }
	inline void set_ascender_2(float value)
	{
		___ascender_2 = value;
	}

	inline static int32_t get_offset_of_baseLine_3() { return static_cast<int32_t>(offsetof(TMP_PageInfo_t5D305B11116379997CA9649E8D87B3D7162ABB24, ___baseLine_3)); }
	inline float get_baseLine_3() const { return ___baseLine_3; }
	inline float* get_address_of_baseLine_3() { return &___baseLine_3; }
	inline void set_baseLine_3(float value)
	{
		___baseLine_3 = value;
	}

	inline static int32_t get_offset_of_descender_4() { return static_cast<int32_t>(offsetof(TMP_PageInfo_t5D305B11116379997CA9649E8D87B3D7162ABB24, ___descender_4)); }
	inline float get_descender_4() const { return ___descender_4; }
	inline float* get_address_of_descender_4() { return &___descender_4; }
	inline void set_descender_4(float value)
	{
		___descender_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_PAGEINFO_T5D305B11116379997CA9649E8D87B3D7162ABB24_H
#ifndef TMP_SPRITEINFO_T55432612FE0D00F32826D0F817E8462F66CBABBB_H
#define TMP_SPRITEINFO_T55432612FE0D00F32826D0F817E8462F66CBABBB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_SpriteInfo
struct  TMP_SpriteInfo_t55432612FE0D00F32826D0F817E8462F66CBABBB 
{
public:
	// System.Int32 TMPro.TMP_SpriteInfo::spriteIndex
	int32_t ___spriteIndex_0;
	// System.Int32 TMPro.TMP_SpriteInfo::characterIndex
	int32_t ___characterIndex_1;
	// System.Int32 TMPro.TMP_SpriteInfo::vertexIndex
	int32_t ___vertexIndex_2;

public:
	inline static int32_t get_offset_of_spriteIndex_0() { return static_cast<int32_t>(offsetof(TMP_SpriteInfo_t55432612FE0D00F32826D0F817E8462F66CBABBB, ___spriteIndex_0)); }
	inline int32_t get_spriteIndex_0() const { return ___spriteIndex_0; }
	inline int32_t* get_address_of_spriteIndex_0() { return &___spriteIndex_0; }
	inline void set_spriteIndex_0(int32_t value)
	{
		___spriteIndex_0 = value;
	}

	inline static int32_t get_offset_of_characterIndex_1() { return static_cast<int32_t>(offsetof(TMP_SpriteInfo_t55432612FE0D00F32826D0F817E8462F66CBABBB, ___characterIndex_1)); }
	inline int32_t get_characterIndex_1() const { return ___characterIndex_1; }
	inline int32_t* get_address_of_characterIndex_1() { return &___characterIndex_1; }
	inline void set_characterIndex_1(int32_t value)
	{
		___characterIndex_1 = value;
	}

	inline static int32_t get_offset_of_vertexIndex_2() { return static_cast<int32_t>(offsetof(TMP_SpriteInfo_t55432612FE0D00F32826D0F817E8462F66CBABBB, ___vertexIndex_2)); }
	inline int32_t get_vertexIndex_2() const { return ___vertexIndex_2; }
	inline int32_t* get_address_of_vertexIndex_2() { return &___vertexIndex_2; }
	inline void set_vertexIndex_2(int32_t value)
	{
		___vertexIndex_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_SPRITEINFO_T55432612FE0D00F32826D0F817E8462F66CBABBB_H
#ifndef TMP_WORDINFO_T856E4994B49881E370B28E1D0C35EEDA56120D90_H
#define TMP_WORDINFO_T856E4994B49881E370B28E1D0C35EEDA56120D90_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_WordInfo
struct  TMP_WordInfo_t856E4994B49881E370B28E1D0C35EEDA56120D90 
{
public:
	// TMPro.TMP_Text TMPro.TMP_WordInfo::textComponent
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * ___textComponent_0;
	// System.Int32 TMPro.TMP_WordInfo::firstCharacterIndex
	int32_t ___firstCharacterIndex_1;
	// System.Int32 TMPro.TMP_WordInfo::lastCharacterIndex
	int32_t ___lastCharacterIndex_2;
	// System.Int32 TMPro.TMP_WordInfo::characterCount
	int32_t ___characterCount_3;

public:
	inline static int32_t get_offset_of_textComponent_0() { return static_cast<int32_t>(offsetof(TMP_WordInfo_t856E4994B49881E370B28E1D0C35EEDA56120D90, ___textComponent_0)); }
	inline TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * get_textComponent_0() const { return ___textComponent_0; }
	inline TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 ** get_address_of_textComponent_0() { return &___textComponent_0; }
	inline void set_textComponent_0(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * value)
	{
		___textComponent_0 = value;
		Il2CppCodeGenWriteBarrier((&___textComponent_0), value);
	}

	inline static int32_t get_offset_of_firstCharacterIndex_1() { return static_cast<int32_t>(offsetof(TMP_WordInfo_t856E4994B49881E370B28E1D0C35EEDA56120D90, ___firstCharacterIndex_1)); }
	inline int32_t get_firstCharacterIndex_1() const { return ___firstCharacterIndex_1; }
	inline int32_t* get_address_of_firstCharacterIndex_1() { return &___firstCharacterIndex_1; }
	inline void set_firstCharacterIndex_1(int32_t value)
	{
		___firstCharacterIndex_1 = value;
	}

	inline static int32_t get_offset_of_lastCharacterIndex_2() { return static_cast<int32_t>(offsetof(TMP_WordInfo_t856E4994B49881E370B28E1D0C35EEDA56120D90, ___lastCharacterIndex_2)); }
	inline int32_t get_lastCharacterIndex_2() const { return ___lastCharacterIndex_2; }
	inline int32_t* get_address_of_lastCharacterIndex_2() { return &___lastCharacterIndex_2; }
	inline void set_lastCharacterIndex_2(int32_t value)
	{
		___lastCharacterIndex_2 = value;
	}

	inline static int32_t get_offset_of_characterCount_3() { return static_cast<int32_t>(offsetof(TMP_WordInfo_t856E4994B49881E370B28E1D0C35EEDA56120D90, ___characterCount_3)); }
	inline int32_t get_characterCount_3() const { return ___characterCount_3; }
	inline int32_t* get_address_of_characterCount_3() { return &___characterCount_3; }
	inline void set_characterCount_3(int32_t value)
	{
		___characterCount_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.TMP_WordInfo
struct TMP_WordInfo_t856E4994B49881E370B28E1D0C35EEDA56120D90_marshaled_pinvoke
{
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * ___textComponent_0;
	int32_t ___firstCharacterIndex_1;
	int32_t ___lastCharacterIndex_2;
	int32_t ___characterCount_3;
};
// Native definition for COM marshalling of TMPro.TMP_WordInfo
struct TMP_WordInfo_t856E4994B49881E370B28E1D0C35EEDA56120D90_marshaled_com
{
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * ___textComponent_0;
	int32_t ___firstCharacterIndex_1;
	int32_t ___lastCharacterIndex_2;
	int32_t ___characterCount_3;
};
#endif // TMP_WORDINFO_T856E4994B49881E370B28E1D0C35EEDA56120D90_H
#ifndef TMP_XMLTAGSTACK_1_TF92254A81B8213FD65E61782291A45206F461683_H
#define TMP_XMLTAGSTACK_1_TF92254A81B8213FD65E61782291A45206F461683_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_XmlTagStack`1<System.Int32>
struct  TMP_XmlTagStack_1_tF92254A81B8213FD65E61782291A45206F461683 
{
public:
	// T[] TMPro.TMP_XmlTagStack`1::itemStack
	Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* ___itemStack_0;
	// System.Int32 TMPro.TMP_XmlTagStack`1::index
	int32_t ___index_1;
	// System.Int32 TMPro.TMP_XmlTagStack`1::m_capacity
	int32_t ___m_capacity_2;
	// T TMPro.TMP_XmlTagStack`1::m_defaultItem
	int32_t ___m_defaultItem_3;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_tF92254A81B8213FD65E61782291A45206F461683, ___itemStack_0)); }
	inline Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* get_itemStack_0() const { return ___itemStack_0; }
	inline Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStack_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_tF92254A81B8213FD65E61782291A45206F461683, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_capacity_2() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_tF92254A81B8213FD65E61782291A45206F461683, ___m_capacity_2)); }
	inline int32_t get_m_capacity_2() const { return ___m_capacity_2; }
	inline int32_t* get_address_of_m_capacity_2() { return &___m_capacity_2; }
	inline void set_m_capacity_2(int32_t value)
	{
		___m_capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_defaultItem_3() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_tF92254A81B8213FD65E61782291A45206F461683, ___m_defaultItem_3)); }
	inline int32_t get_m_defaultItem_3() const { return ___m_defaultItem_3; }
	inline int32_t* get_address_of_m_defaultItem_3() { return &___m_defaultItem_3; }
	inline void set_m_defaultItem_3(int32_t value)
	{
		___m_defaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_XMLTAGSTACK_1_TF92254A81B8213FD65E61782291A45206F461683_H
#ifndef TMP_XMLTAGSTACK_1_T316D6C0717F14C09A6AC717AB85B452FC6ECBDE5_H
#define TMP_XMLTAGSTACK_1_T316D6C0717F14C09A6AC717AB85B452FC6ECBDE5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_XmlTagStack`1<System.Single>
struct  TMP_XmlTagStack_1_t316D6C0717F14C09A6AC717AB85B452FC6ECBDE5 
{
public:
	// T[] TMPro.TMP_XmlTagStack`1::itemStack
	SingleU5BU5D_t8C7CE2FD0C4CD3846DEB3BE6DD5564E32A8A27D5* ___itemStack_0;
	// System.Int32 TMPro.TMP_XmlTagStack`1::index
	int32_t ___index_1;
	// System.Int32 TMPro.TMP_XmlTagStack`1::m_capacity
	int32_t ___m_capacity_2;
	// T TMPro.TMP_XmlTagStack`1::m_defaultItem
	float ___m_defaultItem_3;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t316D6C0717F14C09A6AC717AB85B452FC6ECBDE5, ___itemStack_0)); }
	inline SingleU5BU5D_t8C7CE2FD0C4CD3846DEB3BE6DD5564E32A8A27D5* get_itemStack_0() const { return ___itemStack_0; }
	inline SingleU5BU5D_t8C7CE2FD0C4CD3846DEB3BE6DD5564E32A8A27D5** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(SingleU5BU5D_t8C7CE2FD0C4CD3846DEB3BE6DD5564E32A8A27D5* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStack_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t316D6C0717F14C09A6AC717AB85B452FC6ECBDE5, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_capacity_2() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t316D6C0717F14C09A6AC717AB85B452FC6ECBDE5, ___m_capacity_2)); }
	inline int32_t get_m_capacity_2() const { return ___m_capacity_2; }
	inline int32_t* get_address_of_m_capacity_2() { return &___m_capacity_2; }
	inline void set_m_capacity_2(int32_t value)
	{
		___m_capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_defaultItem_3() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t316D6C0717F14C09A6AC717AB85B452FC6ECBDE5, ___m_defaultItem_3)); }
	inline float get_m_defaultItem_3() const { return ___m_defaultItem_3; }
	inline float* get_address_of_m_defaultItem_3() { return &___m_defaultItem_3; }
	inline void set_m_defaultItem_3(float value)
	{
		___m_defaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_XMLTAGSTACK_1_T316D6C0717F14C09A6AC717AB85B452FC6ECBDE5_H
#ifndef TMP_XMLTAGSTACK_1_T357AC63D4EC290220DBE5C99C30F9FD6FEF05063_H
#define TMP_XMLTAGSTACK_1_T357AC63D4EC290220DBE5C99C30F9FD6FEF05063_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_XmlTagStack`1<TMPro.TMP_ColorGradient>
struct  TMP_XmlTagStack_1_t357AC63D4EC290220DBE5C99C30F9FD6FEF05063 
{
public:
	// T[] TMPro.TMP_XmlTagStack`1::itemStack
	TMP_ColorGradientU5BU5D_t0948D618AC4240E6F0CFE0125BB6A4E931DE847C* ___itemStack_0;
	// System.Int32 TMPro.TMP_XmlTagStack`1::index
	int32_t ___index_1;
	// System.Int32 TMPro.TMP_XmlTagStack`1::m_capacity
	int32_t ___m_capacity_2;
	// T TMPro.TMP_XmlTagStack`1::m_defaultItem
	TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7 * ___m_defaultItem_3;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t357AC63D4EC290220DBE5C99C30F9FD6FEF05063, ___itemStack_0)); }
	inline TMP_ColorGradientU5BU5D_t0948D618AC4240E6F0CFE0125BB6A4E931DE847C* get_itemStack_0() const { return ___itemStack_0; }
	inline TMP_ColorGradientU5BU5D_t0948D618AC4240E6F0CFE0125BB6A4E931DE847C** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(TMP_ColorGradientU5BU5D_t0948D618AC4240E6F0CFE0125BB6A4E931DE847C* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStack_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t357AC63D4EC290220DBE5C99C30F9FD6FEF05063, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_capacity_2() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t357AC63D4EC290220DBE5C99C30F9FD6FEF05063, ___m_capacity_2)); }
	inline int32_t get_m_capacity_2() const { return ___m_capacity_2; }
	inline int32_t* get_address_of_m_capacity_2() { return &___m_capacity_2; }
	inline void set_m_capacity_2(int32_t value)
	{
		___m_capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_defaultItem_3() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t357AC63D4EC290220DBE5C99C30F9FD6FEF05063, ___m_defaultItem_3)); }
	inline TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7 * get_m_defaultItem_3() const { return ___m_defaultItem_3; }
	inline TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7 ** get_address_of_m_defaultItem_3() { return &___m_defaultItem_3; }
	inline void set_m_defaultItem_3(TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7 * value)
	{
		___m_defaultItem_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_defaultItem_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_XMLTAGSTACK_1_T357AC63D4EC290220DBE5C99C30F9FD6FEF05063_H
#ifndef TAGATTRIBUTE_T76F6882B9F1A6E98098CFBB2D4B933BE36B543E5_H
#define TAGATTRIBUTE_T76F6882B9F1A6E98098CFBB2D4B933BE36B543E5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TagAttribute
struct  TagAttribute_t76F6882B9F1A6E98098CFBB2D4B933BE36B543E5 
{
public:
	// System.Int32 TMPro.TagAttribute::startIndex
	int32_t ___startIndex_0;
	// System.Int32 TMPro.TagAttribute::length
	int32_t ___length_1;
	// System.Int32 TMPro.TagAttribute::hashCode
	int32_t ___hashCode_2;

public:
	inline static int32_t get_offset_of_startIndex_0() { return static_cast<int32_t>(offsetof(TagAttribute_t76F6882B9F1A6E98098CFBB2D4B933BE36B543E5, ___startIndex_0)); }
	inline int32_t get_startIndex_0() const { return ___startIndex_0; }
	inline int32_t* get_address_of_startIndex_0() { return &___startIndex_0; }
	inline void set_startIndex_0(int32_t value)
	{
		___startIndex_0 = value;
	}

	inline static int32_t get_offset_of_length_1() { return static_cast<int32_t>(offsetof(TagAttribute_t76F6882B9F1A6E98098CFBB2D4B933BE36B543E5, ___length_1)); }
	inline int32_t get_length_1() const { return ___length_1; }
	inline int32_t* get_address_of_length_1() { return &___length_1; }
	inline void set_length_1(int32_t value)
	{
		___length_1 = value;
	}

	inline static int32_t get_offset_of_hashCode_2() { return static_cast<int32_t>(offsetof(TagAttribute_t76F6882B9F1A6E98098CFBB2D4B933BE36B543E5, ___hashCode_2)); }
	inline int32_t get_hashCode_2() const { return ___hashCode_2; }
	inline int32_t* get_address_of_hashCode_2() { return &___hashCode_2; }
	inline void set_hashCode_2(int32_t value)
	{
		___hashCode_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAGATTRIBUTE_T76F6882B9F1A6E98098CFBB2D4B933BE36B543E5_H
#ifndef COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#define COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifndef COLOR32_T23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23_H
#define COLOR32_T23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color32
struct  Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 UnityEngine.Color32::rgba
			int32_t ___rgba_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___rgba_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Byte UnityEngine.Color32::r
			uint8_t ___r_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___r_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___g_2_OffsetPadding[1];
			// System.Byte UnityEngine.Color32::g
			uint8_t ___g_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___g_2_OffsetPadding_forAlignmentOnly[1];
			uint8_t ___g_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___b_3_OffsetPadding[2];
			// System.Byte UnityEngine.Color32::b
			uint8_t ___b_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___b_3_OffsetPadding_forAlignmentOnly[2];
			uint8_t ___b_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___a_4_OffsetPadding[3];
			// System.Byte UnityEngine.Color32::a
			uint8_t ___a_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___a_4_OffsetPadding_forAlignmentOnly[3];
			uint8_t ___a_4_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_rgba_0() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___rgba_0)); }
	inline int32_t get_rgba_0() const { return ___rgba_0; }
	inline int32_t* get_address_of_rgba_0() { return &___rgba_0; }
	inline void set_rgba_0(int32_t value)
	{
		___rgba_0 = value;
	}

	inline static int32_t get_offset_of_r_1() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___r_1)); }
	inline uint8_t get_r_1() const { return ___r_1; }
	inline uint8_t* get_address_of_r_1() { return &___r_1; }
	inline void set_r_1(uint8_t value)
	{
		___r_1 = value;
	}

	inline static int32_t get_offset_of_g_2() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___g_2)); }
	inline uint8_t get_g_2() const { return ___g_2; }
	inline uint8_t* get_address_of_g_2() { return &___g_2; }
	inline void set_g_2(uint8_t value)
	{
		___g_2 = value;
	}

	inline static int32_t get_offset_of_b_3() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___b_3)); }
	inline uint8_t get_b_3() const { return ___b_3; }
	inline uint8_t* get_address_of_b_3() { return &___b_3; }
	inline void set_b_3(uint8_t value)
	{
		___b_3 = value;
	}

	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___a_4)); }
	inline uint8_t get_a_4() const { return ___a_4; }
	inline uint8_t* get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(uint8_t value)
	{
		___a_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR32_T23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23_H
#ifndef QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#define QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T2BDD5686E87BA22996C7749A9C7A7F86EB589380_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T2BDD5686E87BA22996C7749A9C7A7F86EB589380_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t2BDD5686E87BA22996C7749A9C7A7F86EB589380  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t2BDD5686E87BA22996C7749A9C7A7F86EB589380_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType=12 <PrivateImplementationDetails>::$field-7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46
	U24ArrayTypeU3D12_tBFF9DDDDE6A030848A3B927BF2701B3B97F56317  ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0;
	// <PrivateImplementationDetails>/$ArrayType=40 <PrivateImplementationDetails>::$field-9E6378168821DBABB7EE3D0154346480FAC8AEF1
	U24ArrayTypeU3D40_tD3EB1EDB8340E3E2EFCDE746D3F6D7664617AA91  ___U24fieldU2D9E6378168821DBABB7EE3D0154346480FAC8AEF1_1;

public:
	inline static int32_t get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t2BDD5686E87BA22996C7749A9C7A7F86EB589380_StaticFields, ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0)); }
	inline U24ArrayTypeU3D12_tBFF9DDDDE6A030848A3B927BF2701B3B97F56317  get_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() const { return ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0; }
	inline U24ArrayTypeU3D12_tBFF9DDDDE6A030848A3B927BF2701B3B97F56317 * get_address_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() { return &___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0; }
	inline void set_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(U24ArrayTypeU3D12_tBFF9DDDDE6A030848A3B927BF2701B3B97F56317  value)
	{
		___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D9E6378168821DBABB7EE3D0154346480FAC8AEF1_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t2BDD5686E87BA22996C7749A9C7A7F86EB589380_StaticFields, ___U24fieldU2D9E6378168821DBABB7EE3D0154346480FAC8AEF1_1)); }
	inline U24ArrayTypeU3D40_tD3EB1EDB8340E3E2EFCDE746D3F6D7664617AA91  get_U24fieldU2D9E6378168821DBABB7EE3D0154346480FAC8AEF1_1() const { return ___U24fieldU2D9E6378168821DBABB7EE3D0154346480FAC8AEF1_1; }
	inline U24ArrayTypeU3D40_tD3EB1EDB8340E3E2EFCDE746D3F6D7664617AA91 * get_address_of_U24fieldU2D9E6378168821DBABB7EE3D0154346480FAC8AEF1_1() { return &___U24fieldU2D9E6378168821DBABB7EE3D0154346480FAC8AEF1_1; }
	inline void set_U24fieldU2D9E6378168821DBABB7EE3D0154346480FAC8AEF1_1(U24ArrayTypeU3D40_tD3EB1EDB8340E3E2EFCDE746D3F6D7664617AA91  value)
	{
		___U24fieldU2D9E6378168821DBABB7EE3D0154346480FAC8AEF1_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T2BDD5686E87BA22996C7749A9C7A7F86EB589380_H
#ifndef COMPUTE_DISTANCETRANSFORM_EVENTTYPES_T7D7D951983E29A63EA0B4AE2EBC021B801291E50_H
#define COMPUTE_DISTANCETRANSFORM_EVENTTYPES_T7D7D951983E29A63EA0B4AE2EBC021B801291E50_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Compute_DistanceTransform_EventTypes
struct  Compute_DistanceTransform_EventTypes_t7D7D951983E29A63EA0B4AE2EBC021B801291E50 
{
public:
	// System.Int32 TMPro.Compute_DistanceTransform_EventTypes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Compute_DistanceTransform_EventTypes_t7D7D951983E29A63EA0B4AE2EBC021B801291E50, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPUTE_DISTANCETRANSFORM_EVENTTYPES_T7D7D951983E29A63EA0B4AE2EBC021B801291E50_H
#ifndef EXTENTS_TB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3_H
#define EXTENTS_TB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Extents
struct  Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3 
{
public:
	// UnityEngine.Vector2 TMPro.Extents::min
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___min_0;
	// UnityEngine.Vector2 TMPro.Extents::max
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3, ___min_0)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_min_0() const { return ___min_0; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3, ___max_1)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_max_1() const { return ___max_1; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___max_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENTS_TB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3_H
#ifndef FONTSTYLES_T31B880C817B2DF0BF3B60AC4D187A3E7BE5D8893_H
#define FONTSTYLES_T31B880C817B2DF0BF3B60AC4D187A3E7BE5D8893_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.FontStyles
struct  FontStyles_t31B880C817B2DF0BF3B60AC4D187A3E7BE5D8893 
{
public:
	// System.Int32 TMPro.FontStyles::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FontStyles_t31B880C817B2DF0BF3B60AC4D187A3E7BE5D8893, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FONTSTYLES_T31B880C817B2DF0BF3B60AC4D187A3E7BE5D8893_H
#ifndef KERNINGPAIR_T4FCF2540D9DBE27046ED4A8643B7AA644ED0DAAD_H
#define KERNINGPAIR_T4FCF2540D9DBE27046ED4A8643B7AA644ED0DAAD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.KerningPair
struct  KerningPair_t4FCF2540D9DBE27046ED4A8643B7AA644ED0DAAD  : public RuntimeObject
{
public:
	// System.UInt32 TMPro.KerningPair::m_FirstGlyph
	uint32_t ___m_FirstGlyph_0;
	// TMPro.GlyphValueRecord TMPro.KerningPair::m_FirstGlyphAdjustments
	GlyphValueRecord_t978E7932FA4A5D424E6CFBB65E98B16D02BE18DD  ___m_FirstGlyphAdjustments_1;
	// System.UInt32 TMPro.KerningPair::m_SecondGlyph
	uint32_t ___m_SecondGlyph_2;
	// TMPro.GlyphValueRecord TMPro.KerningPair::m_SecondGlyphAdjustments
	GlyphValueRecord_t978E7932FA4A5D424E6CFBB65E98B16D02BE18DD  ___m_SecondGlyphAdjustments_3;
	// System.Single TMPro.KerningPair::xOffset
	float ___xOffset_4;

public:
	inline static int32_t get_offset_of_m_FirstGlyph_0() { return static_cast<int32_t>(offsetof(KerningPair_t4FCF2540D9DBE27046ED4A8643B7AA644ED0DAAD, ___m_FirstGlyph_0)); }
	inline uint32_t get_m_FirstGlyph_0() const { return ___m_FirstGlyph_0; }
	inline uint32_t* get_address_of_m_FirstGlyph_0() { return &___m_FirstGlyph_0; }
	inline void set_m_FirstGlyph_0(uint32_t value)
	{
		___m_FirstGlyph_0 = value;
	}

	inline static int32_t get_offset_of_m_FirstGlyphAdjustments_1() { return static_cast<int32_t>(offsetof(KerningPair_t4FCF2540D9DBE27046ED4A8643B7AA644ED0DAAD, ___m_FirstGlyphAdjustments_1)); }
	inline GlyphValueRecord_t978E7932FA4A5D424E6CFBB65E98B16D02BE18DD  get_m_FirstGlyphAdjustments_1() const { return ___m_FirstGlyphAdjustments_1; }
	inline GlyphValueRecord_t978E7932FA4A5D424E6CFBB65E98B16D02BE18DD * get_address_of_m_FirstGlyphAdjustments_1() { return &___m_FirstGlyphAdjustments_1; }
	inline void set_m_FirstGlyphAdjustments_1(GlyphValueRecord_t978E7932FA4A5D424E6CFBB65E98B16D02BE18DD  value)
	{
		___m_FirstGlyphAdjustments_1 = value;
	}

	inline static int32_t get_offset_of_m_SecondGlyph_2() { return static_cast<int32_t>(offsetof(KerningPair_t4FCF2540D9DBE27046ED4A8643B7AA644ED0DAAD, ___m_SecondGlyph_2)); }
	inline uint32_t get_m_SecondGlyph_2() const { return ___m_SecondGlyph_2; }
	inline uint32_t* get_address_of_m_SecondGlyph_2() { return &___m_SecondGlyph_2; }
	inline void set_m_SecondGlyph_2(uint32_t value)
	{
		___m_SecondGlyph_2 = value;
	}

	inline static int32_t get_offset_of_m_SecondGlyphAdjustments_3() { return static_cast<int32_t>(offsetof(KerningPair_t4FCF2540D9DBE27046ED4A8643B7AA644ED0DAAD, ___m_SecondGlyphAdjustments_3)); }
	inline GlyphValueRecord_t978E7932FA4A5D424E6CFBB65E98B16D02BE18DD  get_m_SecondGlyphAdjustments_3() const { return ___m_SecondGlyphAdjustments_3; }
	inline GlyphValueRecord_t978E7932FA4A5D424E6CFBB65E98B16D02BE18DD * get_address_of_m_SecondGlyphAdjustments_3() { return &___m_SecondGlyphAdjustments_3; }
	inline void set_m_SecondGlyphAdjustments_3(GlyphValueRecord_t978E7932FA4A5D424E6CFBB65E98B16D02BE18DD  value)
	{
		___m_SecondGlyphAdjustments_3 = value;
	}

	inline static int32_t get_offset_of_xOffset_4() { return static_cast<int32_t>(offsetof(KerningPair_t4FCF2540D9DBE27046ED4A8643B7AA644ED0DAAD, ___xOffset_4)); }
	inline float get_xOffset_4() const { return ___xOffset_4; }
	inline float* get_address_of_xOffset_4() { return &___xOffset_4; }
	inline void set_xOffset_4(float value)
	{
		___xOffset_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KERNINGPAIR_T4FCF2540D9DBE27046ED4A8643B7AA644ED0DAAD_H
#ifndef MESH_EXTENTS_T4A76BB0AD89A24EA8AE86F4EC31081DB86F7E7E8_H
#define MESH_EXTENTS_T4A76BB0AD89A24EA8AE86F4EC31081DB86F7E7E8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Mesh_Extents
struct  Mesh_Extents_t4A76BB0AD89A24EA8AE86F4EC31081DB86F7E7E8 
{
public:
	// UnityEngine.Vector2 TMPro.Mesh_Extents::min
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___min_0;
	// UnityEngine.Vector2 TMPro.Mesh_Extents::max
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(Mesh_Extents_t4A76BB0AD89A24EA8AE86F4EC31081DB86F7E7E8, ___min_0)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_min_0() const { return ___min_0; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(Mesh_Extents_t4A76BB0AD89A24EA8AE86F4EC31081DB86F7E7E8, ___max_1)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_max_1() const { return ___max_1; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___max_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESH_EXTENTS_T4A76BB0AD89A24EA8AE86F4EC31081DB86F7E7E8_H
#ifndef TMP_MATH_T074FB253662A213E8905642B8B29473EC794FE44_H
#define TMP_MATH_T074FB253662A213E8905642B8B29473EC794FE44_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Math
struct  TMP_Math_t074FB253662A213E8905642B8B29473EC794FE44  : public RuntimeObject
{
public:

public:
};

struct TMP_Math_t074FB253662A213E8905642B8B29473EC794FE44_StaticFields
{
public:
	// UnityEngine.Vector2 TMPro.TMP_Math::MAX_16BIT
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___MAX_16BIT_6;
	// UnityEngine.Vector2 TMPro.TMP_Math::MIN_16BIT
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___MIN_16BIT_7;

public:
	inline static int32_t get_offset_of_MAX_16BIT_6() { return static_cast<int32_t>(offsetof(TMP_Math_t074FB253662A213E8905642B8B29473EC794FE44_StaticFields, ___MAX_16BIT_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_MAX_16BIT_6() const { return ___MAX_16BIT_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_MAX_16BIT_6() { return &___MAX_16BIT_6; }
	inline void set_MAX_16BIT_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___MAX_16BIT_6 = value;
	}

	inline static int32_t get_offset_of_MIN_16BIT_7() { return static_cast<int32_t>(offsetof(TMP_Math_t074FB253662A213E8905642B8B29473EC794FE44_StaticFields, ___MIN_16BIT_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_MIN_16BIT_7() const { return ___MIN_16BIT_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_MIN_16BIT_7() { return &___MIN_16BIT_7; }
	inline void set_MIN_16BIT_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___MIN_16BIT_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_MATH_T074FB253662A213E8905642B8B29473EC794FE44_H
#ifndef TMP_TEXTELEMENTTYPE_TBF2553FA730CC21CF99473E591C33DC52360D509_H
#define TMP_TEXTELEMENTTYPE_TBF2553FA730CC21CF99473E591C33DC52360D509_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextElementType
struct  TMP_TextElementType_tBF2553FA730CC21CF99473E591C33DC52360D509 
{
public:
	// System.Int32 TMPro.TMP_TextElementType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TMP_TextElementType_tBF2553FA730CC21CF99473E591C33DC52360D509, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTELEMENTTYPE_TBF2553FA730CC21CF99473E591C33DC52360D509_H
#ifndef TMP_VERTEX_T4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0_H
#define TMP_VERTEX_T4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Vertex
struct  TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0 
{
public:
	// UnityEngine.Vector3 TMPro.TMP_Vertex::position
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___position_0;
	// UnityEngine.Vector2 TMPro.TMP_Vertex::uv
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___uv_1;
	// UnityEngine.Vector2 TMPro.TMP_Vertex::uv2
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___uv2_2;
	// UnityEngine.Vector2 TMPro.TMP_Vertex::uv4
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___uv4_3;
	// UnityEngine.Color32 TMPro.TMP_Vertex::color
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___color_4;

public:
	inline static int32_t get_offset_of_position_0() { return static_cast<int32_t>(offsetof(TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0, ___position_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_position_0() const { return ___position_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_position_0() { return &___position_0; }
	inline void set_position_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___position_0 = value;
	}

	inline static int32_t get_offset_of_uv_1() { return static_cast<int32_t>(offsetof(TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0, ___uv_1)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_uv_1() const { return ___uv_1; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_uv_1() { return &___uv_1; }
	inline void set_uv_1(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___uv_1 = value;
	}

	inline static int32_t get_offset_of_uv2_2() { return static_cast<int32_t>(offsetof(TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0, ___uv2_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_uv2_2() const { return ___uv2_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_uv2_2() { return &___uv2_2; }
	inline void set_uv2_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___uv2_2 = value;
	}

	inline static int32_t get_offset_of_uv4_3() { return static_cast<int32_t>(offsetof(TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0, ___uv4_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_uv4_3() const { return ___uv4_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_uv4_3() { return &___uv4_3; }
	inline void set_uv4_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___uv4_3 = value;
	}

	inline static int32_t get_offset_of_color_4() { return static_cast<int32_t>(offsetof(TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0, ___color_4)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_color_4() const { return ___color_4; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_color_4() { return &___color_4; }
	inline void set_color_4(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___color_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_VERTEX_T4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0_H
#ifndef TMP_VERTEXDATAUPDATEFLAGS_TBEFA6E84F629CD5F4B1B040D55F7AB11B1AD6142_H
#define TMP_VERTEXDATAUPDATEFLAGS_TBEFA6E84F629CD5F4B1B040D55F7AB11B1AD6142_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_VertexDataUpdateFlags
struct  TMP_VertexDataUpdateFlags_tBEFA6E84F629CD5F4B1B040D55F7AB11B1AD6142 
{
public:
	// System.Int32 TMPro.TMP_VertexDataUpdateFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TMP_VertexDataUpdateFlags_tBEFA6E84F629CD5F4B1B040D55F7AB11B1AD6142, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_VERTEXDATAUPDATEFLAGS_TBEFA6E84F629CD5F4B1B040D55F7AB11B1AD6142_H
#ifndef TMP_XMLTAGSTACK_1_TE3F1A1DBF5C066777F7FD28E182B6A14FCCC8A99_H
#define TMP_XMLTAGSTACK_1_TE3F1A1DBF5C066777F7FD28E182B6A14FCCC8A99_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_XmlTagStack`1<TMPro.MaterialReference>
struct  TMP_XmlTagStack_1_tE3F1A1DBF5C066777F7FD28E182B6A14FCCC8A99 
{
public:
	// T[] TMPro.TMP_XmlTagStack`1::itemStack
	MaterialReferenceU5BU5D_t01EC9C1C00A504C2EF9FBAF95DE26BB88E9B743B* ___itemStack_0;
	// System.Int32 TMPro.TMP_XmlTagStack`1::index
	int32_t ___index_1;
	// System.Int32 TMPro.TMP_XmlTagStack`1::m_capacity
	int32_t ___m_capacity_2;
	// T TMPro.TMP_XmlTagStack`1::m_defaultItem
	MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F  ___m_defaultItem_3;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_tE3F1A1DBF5C066777F7FD28E182B6A14FCCC8A99, ___itemStack_0)); }
	inline MaterialReferenceU5BU5D_t01EC9C1C00A504C2EF9FBAF95DE26BB88E9B743B* get_itemStack_0() const { return ___itemStack_0; }
	inline MaterialReferenceU5BU5D_t01EC9C1C00A504C2EF9FBAF95DE26BB88E9B743B** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(MaterialReferenceU5BU5D_t01EC9C1C00A504C2EF9FBAF95DE26BB88E9B743B* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStack_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_tE3F1A1DBF5C066777F7FD28E182B6A14FCCC8A99, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_capacity_2() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_tE3F1A1DBF5C066777F7FD28E182B6A14FCCC8A99, ___m_capacity_2)); }
	inline int32_t get_m_capacity_2() const { return ___m_capacity_2; }
	inline int32_t* get_address_of_m_capacity_2() { return &___m_capacity_2; }
	inline void set_m_capacity_2(int32_t value)
	{
		___m_capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_defaultItem_3() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_tE3F1A1DBF5C066777F7FD28E182B6A14FCCC8A99, ___m_defaultItem_3)); }
	inline MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F  get_m_defaultItem_3() const { return ___m_defaultItem_3; }
	inline MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F * get_address_of_m_defaultItem_3() { return &___m_defaultItem_3; }
	inline void set_m_defaultItem_3(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F  value)
	{
		___m_defaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_XMLTAGSTACK_1_TE3F1A1DBF5C066777F7FD28E182B6A14FCCC8A99_H
#ifndef TMP_XMLTAGSTACK_1_T27223C90F10F186D303BFD74CFAA4A10F1C06314_H
#define TMP_XMLTAGSTACK_1_T27223C90F10F186D303BFD74CFAA4A10F1C06314_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32>
struct  TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314 
{
public:
	// T[] TMPro.TMP_XmlTagStack`1::itemStack
	Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* ___itemStack_0;
	// System.Int32 TMPro.TMP_XmlTagStack`1::index
	int32_t ___index_1;
	// System.Int32 TMPro.TMP_XmlTagStack`1::m_capacity
	int32_t ___m_capacity_2;
	// T TMPro.TMP_XmlTagStack`1::m_defaultItem
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___m_defaultItem_3;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314, ___itemStack_0)); }
	inline Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* get_itemStack_0() const { return ___itemStack_0; }
	inline Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStack_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_capacity_2() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314, ___m_capacity_2)); }
	inline int32_t get_m_capacity_2() const { return ___m_capacity_2; }
	inline int32_t* get_address_of_m_capacity_2() { return &___m_capacity_2; }
	inline void set_m_capacity_2(int32_t value)
	{
		___m_capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_defaultItem_3() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314, ___m_defaultItem_3)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_m_defaultItem_3() const { return ___m_defaultItem_3; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_m_defaultItem_3() { return &___m_defaultItem_3; }
	inline void set_m_defaultItem_3(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___m_defaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_XMLTAGSTACK_1_T27223C90F10F186D303BFD74CFAA4A10F1C06314_H
#ifndef TAGTYPE_T4B5E4B6EA37C80B2B56D26467FD3ED332288E03B_H
#define TAGTYPE_T4B5E4B6EA37C80B2B56D26467FD3ED332288E03B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TagType
struct  TagType_t4B5E4B6EA37C80B2B56D26467FD3ED332288E03B 
{
public:
	// System.Int32 TMPro.TagType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TagType_t4B5E4B6EA37C80B2B56D26467FD3ED332288E03B, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAGTYPE_T4B5E4B6EA37C80B2B56D26467FD3ED332288E03B_H
#ifndef TEXTALIGNMENTOPTIONS_T4BEB3BA6EE897B5127FFBABD7E36B1A024EE5337_H
#define TEXTALIGNMENTOPTIONS_T4BEB3BA6EE897B5127FFBABD7E36B1A024EE5337_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextAlignmentOptions
struct  TextAlignmentOptions_t4BEB3BA6EE897B5127FFBABD7E36B1A024EE5337 
{
public:
	// System.Int32 TMPro.TextAlignmentOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextAlignmentOptions_t4BEB3BA6EE897B5127FFBABD7E36B1A024EE5337, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTALIGNMENTOPTIONS_T4BEB3BA6EE897B5127FFBABD7E36B1A024EE5337_H
#ifndef VERTEXGRADIENT_TDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A_H
#define VERTEXGRADIENT_TDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.VertexGradient
struct  VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A 
{
public:
	// UnityEngine.Color TMPro.VertexGradient::topLeft
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___topLeft_0;
	// UnityEngine.Color TMPro.VertexGradient::topRight
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___topRight_1;
	// UnityEngine.Color TMPro.VertexGradient::bottomLeft
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___bottomLeft_2;
	// UnityEngine.Color TMPro.VertexGradient::bottomRight
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___bottomRight_3;

public:
	inline static int32_t get_offset_of_topLeft_0() { return static_cast<int32_t>(offsetof(VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A, ___topLeft_0)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_topLeft_0() const { return ___topLeft_0; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_topLeft_0() { return &___topLeft_0; }
	inline void set_topLeft_0(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___topLeft_0 = value;
	}

	inline static int32_t get_offset_of_topRight_1() { return static_cast<int32_t>(offsetof(VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A, ___topRight_1)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_topRight_1() const { return ___topRight_1; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_topRight_1() { return &___topRight_1; }
	inline void set_topRight_1(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___topRight_1 = value;
	}

	inline static int32_t get_offset_of_bottomLeft_2() { return static_cast<int32_t>(offsetof(VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A, ___bottomLeft_2)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_bottomLeft_2() const { return ___bottomLeft_2; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_bottomLeft_2() { return &___bottomLeft_2; }
	inline void set_bottomLeft_2(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___bottomLeft_2 = value;
	}

	inline static int32_t get_offset_of_bottomRight_3() { return static_cast<int32_t>(offsetof(VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A, ___bottomRight_3)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_bottomRight_3() const { return ___bottomRight_3; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_bottomRight_3() { return &___bottomRight_3; }
	inline void set_bottomRight_3(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___bottomRight_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXGRADIENT_TDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A_H
#ifndef CURSORLOCKMODE_TF9B28266D253124BE56C232B7ED2D9F7CC3D1E38_H
#define CURSORLOCKMODE_TF9B28266D253124BE56C232B7ED2D9F7CC3D1E38_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CursorLockMode
struct  CursorLockMode_tF9B28266D253124BE56C232B7ED2D9F7CC3D1E38 
{
public:
	// System.Int32 UnityEngine.CursorLockMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CursorLockMode_tF9B28266D253124BE56C232B7ED2D9F7CC3D1E38, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURSORLOCKMODE_TF9B28266D253124BE56C232B7ED2D9F7CC3D1E38_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef COMPUTE_DT_EVENTARGS_T17A56E99E621F8C211A13BE81BEAE18806DA7B11_H
#define COMPUTE_DT_EVENTARGS_T17A56E99E621F8C211A13BE81BEAE18806DA7B11_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Compute_DT_EventArgs
struct  Compute_DT_EventArgs_t17A56E99E621F8C211A13BE81BEAE18806DA7B11  : public RuntimeObject
{
public:
	// TMPro.Compute_DistanceTransform_EventTypes TMPro.Compute_DT_EventArgs::EventType
	int32_t ___EventType_0;
	// System.Single TMPro.Compute_DT_EventArgs::ProgressPercentage
	float ___ProgressPercentage_1;
	// UnityEngine.Color[] TMPro.Compute_DT_EventArgs::Colors
	ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* ___Colors_2;

public:
	inline static int32_t get_offset_of_EventType_0() { return static_cast<int32_t>(offsetof(Compute_DT_EventArgs_t17A56E99E621F8C211A13BE81BEAE18806DA7B11, ___EventType_0)); }
	inline int32_t get_EventType_0() const { return ___EventType_0; }
	inline int32_t* get_address_of_EventType_0() { return &___EventType_0; }
	inline void set_EventType_0(int32_t value)
	{
		___EventType_0 = value;
	}

	inline static int32_t get_offset_of_ProgressPercentage_1() { return static_cast<int32_t>(offsetof(Compute_DT_EventArgs_t17A56E99E621F8C211A13BE81BEAE18806DA7B11, ___ProgressPercentage_1)); }
	inline float get_ProgressPercentage_1() const { return ___ProgressPercentage_1; }
	inline float* get_address_of_ProgressPercentage_1() { return &___ProgressPercentage_1; }
	inline void set_ProgressPercentage_1(float value)
	{
		___ProgressPercentage_1 = value;
	}

	inline static int32_t get_offset_of_Colors_2() { return static_cast<int32_t>(offsetof(Compute_DT_EventArgs_t17A56E99E621F8C211A13BE81BEAE18806DA7B11, ___Colors_2)); }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* get_Colors_2() const { return ___Colors_2; }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399** get_address_of_Colors_2() { return &___Colors_2; }
	inline void set_Colors_2(ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* value)
	{
		___Colors_2 = value;
		Il2CppCodeGenWriteBarrier((&___Colors_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPUTE_DT_EVENTARGS_T17A56E99E621F8C211A13BE81BEAE18806DA7B11_H
#ifndef TMP_CHARACTERINFO_T15C146F0B08EE44A63EC777AC32151D061AFFAF1_H
#define TMP_CHARACTERINFO_T15C146F0B08EE44A63EC777AC32151D061AFFAF1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_CharacterInfo
struct  TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1 
{
public:
	// System.Char TMPro.TMP_CharacterInfo::character
	Il2CppChar ___character_0;
	// System.Int32 TMPro.TMP_CharacterInfo::index
	int32_t ___index_1;
	// TMPro.TMP_TextElementType TMPro.TMP_CharacterInfo::elementType
	int32_t ___elementType_2;
	// TMPro.TMP_TextElement TMPro.TMP_CharacterInfo::textElement
	TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344 * ___textElement_3;
	// TMPro.TMP_FontAsset TMPro.TMP_CharacterInfo::fontAsset
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___fontAsset_4;
	// TMPro.TMP_SpriteAsset TMPro.TMP_CharacterInfo::spriteAsset
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * ___spriteAsset_5;
	// System.Int32 TMPro.TMP_CharacterInfo::spriteIndex
	int32_t ___spriteIndex_6;
	// UnityEngine.Material TMPro.TMP_CharacterInfo::material
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material_7;
	// System.Int32 TMPro.TMP_CharacterInfo::materialReferenceIndex
	int32_t ___materialReferenceIndex_8;
	// System.Boolean TMPro.TMP_CharacterInfo::isUsingAlternateTypeface
	bool ___isUsingAlternateTypeface_9;
	// System.Single TMPro.TMP_CharacterInfo::pointSize
	float ___pointSize_10;
	// System.Int32 TMPro.TMP_CharacterInfo::lineNumber
	int32_t ___lineNumber_11;
	// System.Int32 TMPro.TMP_CharacterInfo::pageNumber
	int32_t ___pageNumber_12;
	// System.Int32 TMPro.TMP_CharacterInfo::vertexIndex
	int32_t ___vertexIndex_13;
	// TMPro.TMP_Vertex TMPro.TMP_CharacterInfo::vertex_TL
	TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  ___vertex_TL_14;
	// TMPro.TMP_Vertex TMPro.TMP_CharacterInfo::vertex_BL
	TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  ___vertex_BL_15;
	// TMPro.TMP_Vertex TMPro.TMP_CharacterInfo::vertex_TR
	TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  ___vertex_TR_16;
	// TMPro.TMP_Vertex TMPro.TMP_CharacterInfo::vertex_BR
	TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  ___vertex_BR_17;
	// UnityEngine.Vector3 TMPro.TMP_CharacterInfo::topLeft
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___topLeft_18;
	// UnityEngine.Vector3 TMPro.TMP_CharacterInfo::bottomLeft
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___bottomLeft_19;
	// UnityEngine.Vector3 TMPro.TMP_CharacterInfo::topRight
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___topRight_20;
	// UnityEngine.Vector3 TMPro.TMP_CharacterInfo::bottomRight
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___bottomRight_21;
	// System.Single TMPro.TMP_CharacterInfo::origin
	float ___origin_22;
	// System.Single TMPro.TMP_CharacterInfo::ascender
	float ___ascender_23;
	// System.Single TMPro.TMP_CharacterInfo::baseLine
	float ___baseLine_24;
	// System.Single TMPro.TMP_CharacterInfo::descender
	float ___descender_25;
	// System.Single TMPro.TMP_CharacterInfo::xAdvance
	float ___xAdvance_26;
	// System.Single TMPro.TMP_CharacterInfo::aspectRatio
	float ___aspectRatio_27;
	// System.Single TMPro.TMP_CharacterInfo::scale
	float ___scale_28;
	// UnityEngine.Color32 TMPro.TMP_CharacterInfo::color
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___color_29;
	// UnityEngine.Color32 TMPro.TMP_CharacterInfo::underlineColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___underlineColor_30;
	// UnityEngine.Color32 TMPro.TMP_CharacterInfo::strikethroughColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___strikethroughColor_31;
	// UnityEngine.Color32 TMPro.TMP_CharacterInfo::highlightColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___highlightColor_32;
	// TMPro.FontStyles TMPro.TMP_CharacterInfo::style
	int32_t ___style_33;
	// System.Boolean TMPro.TMP_CharacterInfo::isVisible
	bool ___isVisible_34;

public:
	inline static int32_t get_offset_of_character_0() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___character_0)); }
	inline Il2CppChar get_character_0() const { return ___character_0; }
	inline Il2CppChar* get_address_of_character_0() { return &___character_0; }
	inline void set_character_0(Il2CppChar value)
	{
		___character_0 = value;
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_elementType_2() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___elementType_2)); }
	inline int32_t get_elementType_2() const { return ___elementType_2; }
	inline int32_t* get_address_of_elementType_2() { return &___elementType_2; }
	inline void set_elementType_2(int32_t value)
	{
		___elementType_2 = value;
	}

	inline static int32_t get_offset_of_textElement_3() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___textElement_3)); }
	inline TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344 * get_textElement_3() const { return ___textElement_3; }
	inline TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344 ** get_address_of_textElement_3() { return &___textElement_3; }
	inline void set_textElement_3(TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344 * value)
	{
		___textElement_3 = value;
		Il2CppCodeGenWriteBarrier((&___textElement_3), value);
	}

	inline static int32_t get_offset_of_fontAsset_4() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___fontAsset_4)); }
	inline TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * get_fontAsset_4() const { return ___fontAsset_4; }
	inline TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C ** get_address_of_fontAsset_4() { return &___fontAsset_4; }
	inline void set_fontAsset_4(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * value)
	{
		___fontAsset_4 = value;
		Il2CppCodeGenWriteBarrier((&___fontAsset_4), value);
	}

	inline static int32_t get_offset_of_spriteAsset_5() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___spriteAsset_5)); }
	inline TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * get_spriteAsset_5() const { return ___spriteAsset_5; }
	inline TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 ** get_address_of_spriteAsset_5() { return &___spriteAsset_5; }
	inline void set_spriteAsset_5(TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * value)
	{
		___spriteAsset_5 = value;
		Il2CppCodeGenWriteBarrier((&___spriteAsset_5), value);
	}

	inline static int32_t get_offset_of_spriteIndex_6() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___spriteIndex_6)); }
	inline int32_t get_spriteIndex_6() const { return ___spriteIndex_6; }
	inline int32_t* get_address_of_spriteIndex_6() { return &___spriteIndex_6; }
	inline void set_spriteIndex_6(int32_t value)
	{
		___spriteIndex_6 = value;
	}

	inline static int32_t get_offset_of_material_7() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___material_7)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_material_7() const { return ___material_7; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_material_7() { return &___material_7; }
	inline void set_material_7(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___material_7 = value;
		Il2CppCodeGenWriteBarrier((&___material_7), value);
	}

	inline static int32_t get_offset_of_materialReferenceIndex_8() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___materialReferenceIndex_8)); }
	inline int32_t get_materialReferenceIndex_8() const { return ___materialReferenceIndex_8; }
	inline int32_t* get_address_of_materialReferenceIndex_8() { return &___materialReferenceIndex_8; }
	inline void set_materialReferenceIndex_8(int32_t value)
	{
		___materialReferenceIndex_8 = value;
	}

	inline static int32_t get_offset_of_isUsingAlternateTypeface_9() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___isUsingAlternateTypeface_9)); }
	inline bool get_isUsingAlternateTypeface_9() const { return ___isUsingAlternateTypeface_9; }
	inline bool* get_address_of_isUsingAlternateTypeface_9() { return &___isUsingAlternateTypeface_9; }
	inline void set_isUsingAlternateTypeface_9(bool value)
	{
		___isUsingAlternateTypeface_9 = value;
	}

	inline static int32_t get_offset_of_pointSize_10() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___pointSize_10)); }
	inline float get_pointSize_10() const { return ___pointSize_10; }
	inline float* get_address_of_pointSize_10() { return &___pointSize_10; }
	inline void set_pointSize_10(float value)
	{
		___pointSize_10 = value;
	}

	inline static int32_t get_offset_of_lineNumber_11() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___lineNumber_11)); }
	inline int32_t get_lineNumber_11() const { return ___lineNumber_11; }
	inline int32_t* get_address_of_lineNumber_11() { return &___lineNumber_11; }
	inline void set_lineNumber_11(int32_t value)
	{
		___lineNumber_11 = value;
	}

	inline static int32_t get_offset_of_pageNumber_12() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___pageNumber_12)); }
	inline int32_t get_pageNumber_12() const { return ___pageNumber_12; }
	inline int32_t* get_address_of_pageNumber_12() { return &___pageNumber_12; }
	inline void set_pageNumber_12(int32_t value)
	{
		___pageNumber_12 = value;
	}

	inline static int32_t get_offset_of_vertexIndex_13() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___vertexIndex_13)); }
	inline int32_t get_vertexIndex_13() const { return ___vertexIndex_13; }
	inline int32_t* get_address_of_vertexIndex_13() { return &___vertexIndex_13; }
	inline void set_vertexIndex_13(int32_t value)
	{
		___vertexIndex_13 = value;
	}

	inline static int32_t get_offset_of_vertex_TL_14() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___vertex_TL_14)); }
	inline TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  get_vertex_TL_14() const { return ___vertex_TL_14; }
	inline TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0 * get_address_of_vertex_TL_14() { return &___vertex_TL_14; }
	inline void set_vertex_TL_14(TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  value)
	{
		___vertex_TL_14 = value;
	}

	inline static int32_t get_offset_of_vertex_BL_15() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___vertex_BL_15)); }
	inline TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  get_vertex_BL_15() const { return ___vertex_BL_15; }
	inline TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0 * get_address_of_vertex_BL_15() { return &___vertex_BL_15; }
	inline void set_vertex_BL_15(TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  value)
	{
		___vertex_BL_15 = value;
	}

	inline static int32_t get_offset_of_vertex_TR_16() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___vertex_TR_16)); }
	inline TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  get_vertex_TR_16() const { return ___vertex_TR_16; }
	inline TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0 * get_address_of_vertex_TR_16() { return &___vertex_TR_16; }
	inline void set_vertex_TR_16(TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  value)
	{
		___vertex_TR_16 = value;
	}

	inline static int32_t get_offset_of_vertex_BR_17() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___vertex_BR_17)); }
	inline TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  get_vertex_BR_17() const { return ___vertex_BR_17; }
	inline TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0 * get_address_of_vertex_BR_17() { return &___vertex_BR_17; }
	inline void set_vertex_BR_17(TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  value)
	{
		___vertex_BR_17 = value;
	}

	inline static int32_t get_offset_of_topLeft_18() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___topLeft_18)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_topLeft_18() const { return ___topLeft_18; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_topLeft_18() { return &___topLeft_18; }
	inline void set_topLeft_18(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___topLeft_18 = value;
	}

	inline static int32_t get_offset_of_bottomLeft_19() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___bottomLeft_19)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_bottomLeft_19() const { return ___bottomLeft_19; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_bottomLeft_19() { return &___bottomLeft_19; }
	inline void set_bottomLeft_19(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___bottomLeft_19 = value;
	}

	inline static int32_t get_offset_of_topRight_20() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___topRight_20)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_topRight_20() const { return ___topRight_20; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_topRight_20() { return &___topRight_20; }
	inline void set_topRight_20(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___topRight_20 = value;
	}

	inline static int32_t get_offset_of_bottomRight_21() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___bottomRight_21)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_bottomRight_21() const { return ___bottomRight_21; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_bottomRight_21() { return &___bottomRight_21; }
	inline void set_bottomRight_21(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___bottomRight_21 = value;
	}

	inline static int32_t get_offset_of_origin_22() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___origin_22)); }
	inline float get_origin_22() const { return ___origin_22; }
	inline float* get_address_of_origin_22() { return &___origin_22; }
	inline void set_origin_22(float value)
	{
		___origin_22 = value;
	}

	inline static int32_t get_offset_of_ascender_23() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___ascender_23)); }
	inline float get_ascender_23() const { return ___ascender_23; }
	inline float* get_address_of_ascender_23() { return &___ascender_23; }
	inline void set_ascender_23(float value)
	{
		___ascender_23 = value;
	}

	inline static int32_t get_offset_of_baseLine_24() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___baseLine_24)); }
	inline float get_baseLine_24() const { return ___baseLine_24; }
	inline float* get_address_of_baseLine_24() { return &___baseLine_24; }
	inline void set_baseLine_24(float value)
	{
		___baseLine_24 = value;
	}

	inline static int32_t get_offset_of_descender_25() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___descender_25)); }
	inline float get_descender_25() const { return ___descender_25; }
	inline float* get_address_of_descender_25() { return &___descender_25; }
	inline void set_descender_25(float value)
	{
		___descender_25 = value;
	}

	inline static int32_t get_offset_of_xAdvance_26() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___xAdvance_26)); }
	inline float get_xAdvance_26() const { return ___xAdvance_26; }
	inline float* get_address_of_xAdvance_26() { return &___xAdvance_26; }
	inline void set_xAdvance_26(float value)
	{
		___xAdvance_26 = value;
	}

	inline static int32_t get_offset_of_aspectRatio_27() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___aspectRatio_27)); }
	inline float get_aspectRatio_27() const { return ___aspectRatio_27; }
	inline float* get_address_of_aspectRatio_27() { return &___aspectRatio_27; }
	inline void set_aspectRatio_27(float value)
	{
		___aspectRatio_27 = value;
	}

	inline static int32_t get_offset_of_scale_28() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___scale_28)); }
	inline float get_scale_28() const { return ___scale_28; }
	inline float* get_address_of_scale_28() { return &___scale_28; }
	inline void set_scale_28(float value)
	{
		___scale_28 = value;
	}

	inline static int32_t get_offset_of_color_29() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___color_29)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_color_29() const { return ___color_29; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_color_29() { return &___color_29; }
	inline void set_color_29(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___color_29 = value;
	}

	inline static int32_t get_offset_of_underlineColor_30() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___underlineColor_30)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_underlineColor_30() const { return ___underlineColor_30; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_underlineColor_30() { return &___underlineColor_30; }
	inline void set_underlineColor_30(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___underlineColor_30 = value;
	}

	inline static int32_t get_offset_of_strikethroughColor_31() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___strikethroughColor_31)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_strikethroughColor_31() const { return ___strikethroughColor_31; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_strikethroughColor_31() { return &___strikethroughColor_31; }
	inline void set_strikethroughColor_31(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___strikethroughColor_31 = value;
	}

	inline static int32_t get_offset_of_highlightColor_32() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___highlightColor_32)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_highlightColor_32() const { return ___highlightColor_32; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_highlightColor_32() { return &___highlightColor_32; }
	inline void set_highlightColor_32(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___highlightColor_32 = value;
	}

	inline static int32_t get_offset_of_style_33() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___style_33)); }
	inline int32_t get_style_33() const { return ___style_33; }
	inline int32_t* get_address_of_style_33() { return &___style_33; }
	inline void set_style_33(int32_t value)
	{
		___style_33 = value;
	}

	inline static int32_t get_offset_of_isVisible_34() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___isVisible_34)); }
	inline bool get_isVisible_34() const { return ___isVisible_34; }
	inline bool* get_address_of_isVisible_34() { return &___isVisible_34; }
	inline void set_isVisible_34(bool value)
	{
		___isVisible_34 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.TMP_CharacterInfo
struct TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1_marshaled_pinvoke
{
	uint8_t ___character_0;
	int32_t ___index_1;
	int32_t ___elementType_2;
	TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344 * ___textElement_3;
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___fontAsset_4;
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * ___spriteAsset_5;
	int32_t ___spriteIndex_6;
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material_7;
	int32_t ___materialReferenceIndex_8;
	int32_t ___isUsingAlternateTypeface_9;
	float ___pointSize_10;
	int32_t ___lineNumber_11;
	int32_t ___pageNumber_12;
	int32_t ___vertexIndex_13;
	TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  ___vertex_TL_14;
	TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  ___vertex_BL_15;
	TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  ___vertex_TR_16;
	TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  ___vertex_BR_17;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___topLeft_18;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___bottomLeft_19;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___topRight_20;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___bottomRight_21;
	float ___origin_22;
	float ___ascender_23;
	float ___baseLine_24;
	float ___descender_25;
	float ___xAdvance_26;
	float ___aspectRatio_27;
	float ___scale_28;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___color_29;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___underlineColor_30;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___strikethroughColor_31;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___highlightColor_32;
	int32_t ___style_33;
	int32_t ___isVisible_34;
};
// Native definition for COM marshalling of TMPro.TMP_CharacterInfo
struct TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1_marshaled_com
{
	uint8_t ___character_0;
	int32_t ___index_1;
	int32_t ___elementType_2;
	TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344 * ___textElement_3;
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___fontAsset_4;
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * ___spriteAsset_5;
	int32_t ___spriteIndex_6;
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material_7;
	int32_t ___materialReferenceIndex_8;
	int32_t ___isUsingAlternateTypeface_9;
	float ___pointSize_10;
	int32_t ___lineNumber_11;
	int32_t ___pageNumber_12;
	int32_t ___vertexIndex_13;
	TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  ___vertex_TL_14;
	TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  ___vertex_BL_15;
	TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  ___vertex_TR_16;
	TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  ___vertex_BR_17;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___topLeft_18;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___bottomLeft_19;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___topRight_20;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___bottomRight_21;
	float ___origin_22;
	float ___ascender_23;
	float ___baseLine_24;
	float ___descender_25;
	float ___xAdvance_26;
	float ___aspectRatio_27;
	float ___scale_28;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___color_29;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___underlineColor_30;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___strikethroughColor_31;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___highlightColor_32;
	int32_t ___style_33;
	int32_t ___isVisible_34;
};
#endif // TMP_CHARACTERINFO_T15C146F0B08EE44A63EC777AC32151D061AFFAF1_H
#ifndef TMP_LINEINFO_TE89A82D872E55C3DDF29C4C8D862358633D0B442_H
#define TMP_LINEINFO_TE89A82D872E55C3DDF29C4C8D862358633D0B442_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_LineInfo
struct  TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442 
{
public:
	// System.Int32 TMPro.TMP_LineInfo::controlCharacterCount
	int32_t ___controlCharacterCount_0;
	// System.Int32 TMPro.TMP_LineInfo::characterCount
	int32_t ___characterCount_1;
	// System.Int32 TMPro.TMP_LineInfo::visibleCharacterCount
	int32_t ___visibleCharacterCount_2;
	// System.Int32 TMPro.TMP_LineInfo::spaceCount
	int32_t ___spaceCount_3;
	// System.Int32 TMPro.TMP_LineInfo::wordCount
	int32_t ___wordCount_4;
	// System.Int32 TMPro.TMP_LineInfo::firstCharacterIndex
	int32_t ___firstCharacterIndex_5;
	// System.Int32 TMPro.TMP_LineInfo::firstVisibleCharacterIndex
	int32_t ___firstVisibleCharacterIndex_6;
	// System.Int32 TMPro.TMP_LineInfo::lastCharacterIndex
	int32_t ___lastCharacterIndex_7;
	// System.Int32 TMPro.TMP_LineInfo::lastVisibleCharacterIndex
	int32_t ___lastVisibleCharacterIndex_8;
	// System.Single TMPro.TMP_LineInfo::length
	float ___length_9;
	// System.Single TMPro.TMP_LineInfo::lineHeight
	float ___lineHeight_10;
	// System.Single TMPro.TMP_LineInfo::ascender
	float ___ascender_11;
	// System.Single TMPro.TMP_LineInfo::baseline
	float ___baseline_12;
	// System.Single TMPro.TMP_LineInfo::descender
	float ___descender_13;
	// System.Single TMPro.TMP_LineInfo::maxAdvance
	float ___maxAdvance_14;
	// System.Single TMPro.TMP_LineInfo::width
	float ___width_15;
	// System.Single TMPro.TMP_LineInfo::marginLeft
	float ___marginLeft_16;
	// System.Single TMPro.TMP_LineInfo::marginRight
	float ___marginRight_17;
	// TMPro.TextAlignmentOptions TMPro.TMP_LineInfo::alignment
	int32_t ___alignment_18;
	// TMPro.Extents TMPro.TMP_LineInfo::lineExtents
	Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3  ___lineExtents_19;

public:
	inline static int32_t get_offset_of_controlCharacterCount_0() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___controlCharacterCount_0)); }
	inline int32_t get_controlCharacterCount_0() const { return ___controlCharacterCount_0; }
	inline int32_t* get_address_of_controlCharacterCount_0() { return &___controlCharacterCount_0; }
	inline void set_controlCharacterCount_0(int32_t value)
	{
		___controlCharacterCount_0 = value;
	}

	inline static int32_t get_offset_of_characterCount_1() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___characterCount_1)); }
	inline int32_t get_characterCount_1() const { return ___characterCount_1; }
	inline int32_t* get_address_of_characterCount_1() { return &___characterCount_1; }
	inline void set_characterCount_1(int32_t value)
	{
		___characterCount_1 = value;
	}

	inline static int32_t get_offset_of_visibleCharacterCount_2() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___visibleCharacterCount_2)); }
	inline int32_t get_visibleCharacterCount_2() const { return ___visibleCharacterCount_2; }
	inline int32_t* get_address_of_visibleCharacterCount_2() { return &___visibleCharacterCount_2; }
	inline void set_visibleCharacterCount_2(int32_t value)
	{
		___visibleCharacterCount_2 = value;
	}

	inline static int32_t get_offset_of_spaceCount_3() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___spaceCount_3)); }
	inline int32_t get_spaceCount_3() const { return ___spaceCount_3; }
	inline int32_t* get_address_of_spaceCount_3() { return &___spaceCount_3; }
	inline void set_spaceCount_3(int32_t value)
	{
		___spaceCount_3 = value;
	}

	inline static int32_t get_offset_of_wordCount_4() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___wordCount_4)); }
	inline int32_t get_wordCount_4() const { return ___wordCount_4; }
	inline int32_t* get_address_of_wordCount_4() { return &___wordCount_4; }
	inline void set_wordCount_4(int32_t value)
	{
		___wordCount_4 = value;
	}

	inline static int32_t get_offset_of_firstCharacterIndex_5() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___firstCharacterIndex_5)); }
	inline int32_t get_firstCharacterIndex_5() const { return ___firstCharacterIndex_5; }
	inline int32_t* get_address_of_firstCharacterIndex_5() { return &___firstCharacterIndex_5; }
	inline void set_firstCharacterIndex_5(int32_t value)
	{
		___firstCharacterIndex_5 = value;
	}

	inline static int32_t get_offset_of_firstVisibleCharacterIndex_6() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___firstVisibleCharacterIndex_6)); }
	inline int32_t get_firstVisibleCharacterIndex_6() const { return ___firstVisibleCharacterIndex_6; }
	inline int32_t* get_address_of_firstVisibleCharacterIndex_6() { return &___firstVisibleCharacterIndex_6; }
	inline void set_firstVisibleCharacterIndex_6(int32_t value)
	{
		___firstVisibleCharacterIndex_6 = value;
	}

	inline static int32_t get_offset_of_lastCharacterIndex_7() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___lastCharacterIndex_7)); }
	inline int32_t get_lastCharacterIndex_7() const { return ___lastCharacterIndex_7; }
	inline int32_t* get_address_of_lastCharacterIndex_7() { return &___lastCharacterIndex_7; }
	inline void set_lastCharacterIndex_7(int32_t value)
	{
		___lastCharacterIndex_7 = value;
	}

	inline static int32_t get_offset_of_lastVisibleCharacterIndex_8() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___lastVisibleCharacterIndex_8)); }
	inline int32_t get_lastVisibleCharacterIndex_8() const { return ___lastVisibleCharacterIndex_8; }
	inline int32_t* get_address_of_lastVisibleCharacterIndex_8() { return &___lastVisibleCharacterIndex_8; }
	inline void set_lastVisibleCharacterIndex_8(int32_t value)
	{
		___lastVisibleCharacterIndex_8 = value;
	}

	inline static int32_t get_offset_of_length_9() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___length_9)); }
	inline float get_length_9() const { return ___length_9; }
	inline float* get_address_of_length_9() { return &___length_9; }
	inline void set_length_9(float value)
	{
		___length_9 = value;
	}

	inline static int32_t get_offset_of_lineHeight_10() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___lineHeight_10)); }
	inline float get_lineHeight_10() const { return ___lineHeight_10; }
	inline float* get_address_of_lineHeight_10() { return &___lineHeight_10; }
	inline void set_lineHeight_10(float value)
	{
		___lineHeight_10 = value;
	}

	inline static int32_t get_offset_of_ascender_11() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___ascender_11)); }
	inline float get_ascender_11() const { return ___ascender_11; }
	inline float* get_address_of_ascender_11() { return &___ascender_11; }
	inline void set_ascender_11(float value)
	{
		___ascender_11 = value;
	}

	inline static int32_t get_offset_of_baseline_12() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___baseline_12)); }
	inline float get_baseline_12() const { return ___baseline_12; }
	inline float* get_address_of_baseline_12() { return &___baseline_12; }
	inline void set_baseline_12(float value)
	{
		___baseline_12 = value;
	}

	inline static int32_t get_offset_of_descender_13() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___descender_13)); }
	inline float get_descender_13() const { return ___descender_13; }
	inline float* get_address_of_descender_13() { return &___descender_13; }
	inline void set_descender_13(float value)
	{
		___descender_13 = value;
	}

	inline static int32_t get_offset_of_maxAdvance_14() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___maxAdvance_14)); }
	inline float get_maxAdvance_14() const { return ___maxAdvance_14; }
	inline float* get_address_of_maxAdvance_14() { return &___maxAdvance_14; }
	inline void set_maxAdvance_14(float value)
	{
		___maxAdvance_14 = value;
	}

	inline static int32_t get_offset_of_width_15() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___width_15)); }
	inline float get_width_15() const { return ___width_15; }
	inline float* get_address_of_width_15() { return &___width_15; }
	inline void set_width_15(float value)
	{
		___width_15 = value;
	}

	inline static int32_t get_offset_of_marginLeft_16() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___marginLeft_16)); }
	inline float get_marginLeft_16() const { return ___marginLeft_16; }
	inline float* get_address_of_marginLeft_16() { return &___marginLeft_16; }
	inline void set_marginLeft_16(float value)
	{
		___marginLeft_16 = value;
	}

	inline static int32_t get_offset_of_marginRight_17() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___marginRight_17)); }
	inline float get_marginRight_17() const { return ___marginRight_17; }
	inline float* get_address_of_marginRight_17() { return &___marginRight_17; }
	inline void set_marginRight_17(float value)
	{
		___marginRight_17 = value;
	}

	inline static int32_t get_offset_of_alignment_18() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___alignment_18)); }
	inline int32_t get_alignment_18() const { return ___alignment_18; }
	inline int32_t* get_address_of_alignment_18() { return &___alignment_18; }
	inline void set_alignment_18(int32_t value)
	{
		___alignment_18 = value;
	}

	inline static int32_t get_offset_of_lineExtents_19() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___lineExtents_19)); }
	inline Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3  get_lineExtents_19() const { return ___lineExtents_19; }
	inline Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3 * get_address_of_lineExtents_19() { return &___lineExtents_19; }
	inline void set_lineExtents_19(Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3  value)
	{
		___lineExtents_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_LINEINFO_TE89A82D872E55C3DDF29C4C8D862358633D0B442_H
#ifndef TMP_XMLTAGSTACK_1_T79B32594D8EFF8CB8078E16347606D8C097A225C_H
#define TMP_XMLTAGSTACK_1_T79B32594D8EFF8CB8078E16347606D8C097A225C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_XmlTagStack`1<TMPro.TextAlignmentOptions>
struct  TMP_XmlTagStack_1_t79B32594D8EFF8CB8078E16347606D8C097A225C 
{
public:
	// T[] TMPro.TMP_XmlTagStack`1::itemStack
	TextAlignmentOptionsU5BU5D_t4AE8FA5E3D695ED64EBBCFBAF8C780A0EB0BD33B* ___itemStack_0;
	// System.Int32 TMPro.TMP_XmlTagStack`1::index
	int32_t ___index_1;
	// System.Int32 TMPro.TMP_XmlTagStack`1::m_capacity
	int32_t ___m_capacity_2;
	// T TMPro.TMP_XmlTagStack`1::m_defaultItem
	int32_t ___m_defaultItem_3;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t79B32594D8EFF8CB8078E16347606D8C097A225C, ___itemStack_0)); }
	inline TextAlignmentOptionsU5BU5D_t4AE8FA5E3D695ED64EBBCFBAF8C780A0EB0BD33B* get_itemStack_0() const { return ___itemStack_0; }
	inline TextAlignmentOptionsU5BU5D_t4AE8FA5E3D695ED64EBBCFBAF8C780A0EB0BD33B** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(TextAlignmentOptionsU5BU5D_t4AE8FA5E3D695ED64EBBCFBAF8C780A0EB0BD33B* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStack_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t79B32594D8EFF8CB8078E16347606D8C097A225C, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_capacity_2() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t79B32594D8EFF8CB8078E16347606D8C097A225C, ___m_capacity_2)); }
	inline int32_t get_m_capacity_2() const { return ___m_capacity_2; }
	inline int32_t* get_address_of_m_capacity_2() { return &___m_capacity_2; }
	inline void set_m_capacity_2(int32_t value)
	{
		___m_capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_defaultItem_3() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t79B32594D8EFF8CB8078E16347606D8C097A225C, ___m_defaultItem_3)); }
	inline int32_t get_m_defaultItem_3() const { return ___m_defaultItem_3; }
	inline int32_t* get_address_of_m_defaultItem_3() { return &___m_defaultItem_3; }
	inline void set_m_defaultItem_3(int32_t value)
	{
		___m_defaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_XMLTAGSTACK_1_T79B32594D8EFF8CB8078E16347606D8C097A225C_H
#ifndef XML_TAGATTRIBUTE_TE129B1DB312EBE65B24529E9CD5BCBE1DB75BF8E_H
#define XML_TAGATTRIBUTE_TE129B1DB312EBE65B24529E9CD5BCBE1DB75BF8E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.XML_TagAttribute
struct  XML_TagAttribute_tE129B1DB312EBE65B24529E9CD5BCBE1DB75BF8E 
{
public:
	// System.Int32 TMPro.XML_TagAttribute::nameHashCode
	int32_t ___nameHashCode_0;
	// TMPro.TagType TMPro.XML_TagAttribute::valueType
	int32_t ___valueType_1;
	// System.Int32 TMPro.XML_TagAttribute::valueStartIndex
	int32_t ___valueStartIndex_2;
	// System.Int32 TMPro.XML_TagAttribute::valueLength
	int32_t ___valueLength_3;
	// System.Int32 TMPro.XML_TagAttribute::valueHashCode
	int32_t ___valueHashCode_4;

public:
	inline static int32_t get_offset_of_nameHashCode_0() { return static_cast<int32_t>(offsetof(XML_TagAttribute_tE129B1DB312EBE65B24529E9CD5BCBE1DB75BF8E, ___nameHashCode_0)); }
	inline int32_t get_nameHashCode_0() const { return ___nameHashCode_0; }
	inline int32_t* get_address_of_nameHashCode_0() { return &___nameHashCode_0; }
	inline void set_nameHashCode_0(int32_t value)
	{
		___nameHashCode_0 = value;
	}

	inline static int32_t get_offset_of_valueType_1() { return static_cast<int32_t>(offsetof(XML_TagAttribute_tE129B1DB312EBE65B24529E9CD5BCBE1DB75BF8E, ___valueType_1)); }
	inline int32_t get_valueType_1() const { return ___valueType_1; }
	inline int32_t* get_address_of_valueType_1() { return &___valueType_1; }
	inline void set_valueType_1(int32_t value)
	{
		___valueType_1 = value;
	}

	inline static int32_t get_offset_of_valueStartIndex_2() { return static_cast<int32_t>(offsetof(XML_TagAttribute_tE129B1DB312EBE65B24529E9CD5BCBE1DB75BF8E, ___valueStartIndex_2)); }
	inline int32_t get_valueStartIndex_2() const { return ___valueStartIndex_2; }
	inline int32_t* get_address_of_valueStartIndex_2() { return &___valueStartIndex_2; }
	inline void set_valueStartIndex_2(int32_t value)
	{
		___valueStartIndex_2 = value;
	}

	inline static int32_t get_offset_of_valueLength_3() { return static_cast<int32_t>(offsetof(XML_TagAttribute_tE129B1DB312EBE65B24529E9CD5BCBE1DB75BF8E, ___valueLength_3)); }
	inline int32_t get_valueLength_3() const { return ___valueLength_3; }
	inline int32_t* get_address_of_valueLength_3() { return &___valueLength_3; }
	inline void set_valueLength_3(int32_t value)
	{
		___valueLength_3 = value;
	}

	inline static int32_t get_offset_of_valueHashCode_4() { return static_cast<int32_t>(offsetof(XML_TagAttribute_tE129B1DB312EBE65B24529E9CD5BCBE1DB75BF8E, ___valueHashCode_4)); }
	inline int32_t get_valueHashCode_4() const { return ___valueHashCode_4; }
	inline int32_t* get_address_of_valueHashCode_4() { return &___valueHashCode_4; }
	inline void set_valueHashCode_4(int32_t value)
	{
		___valueHashCode_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XML_TAGATTRIBUTE_TE129B1DB312EBE65B24529E9CD5BCBE1DB75BF8E_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#define SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_pinvoke : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_com : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifndef RAMPASSET_T6D58ED782B43A6FC5555C2A22E5F767100D34C8E_H
#define RAMPASSET_T6D58ED782B43A6FC5555C2A22E5F767100D34C8E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RampAsset
struct  RampAsset_t6D58ED782B43A6FC5555C2A22E5F767100D34C8E  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// UnityEngine.Gradient RampAsset::gradient
	Gradient_t35A694DDA1066524440E325E582B01E33DE66A3A * ___gradient_4;
	// System.Int32 RampAsset::size
	int32_t ___size_5;
	// System.Boolean RampAsset::up
	bool ___up_6;
	// System.Boolean RampAsset::overwriteExisting
	bool ___overwriteExisting_7;

public:
	inline static int32_t get_offset_of_gradient_4() { return static_cast<int32_t>(offsetof(RampAsset_t6D58ED782B43A6FC5555C2A22E5F767100D34C8E, ___gradient_4)); }
	inline Gradient_t35A694DDA1066524440E325E582B01E33DE66A3A * get_gradient_4() const { return ___gradient_4; }
	inline Gradient_t35A694DDA1066524440E325E582B01E33DE66A3A ** get_address_of_gradient_4() { return &___gradient_4; }
	inline void set_gradient_4(Gradient_t35A694DDA1066524440E325E582B01E33DE66A3A * value)
	{
		___gradient_4 = value;
		Il2CppCodeGenWriteBarrier((&___gradient_4), value);
	}

	inline static int32_t get_offset_of_size_5() { return static_cast<int32_t>(offsetof(RampAsset_t6D58ED782B43A6FC5555C2A22E5F767100D34C8E, ___size_5)); }
	inline int32_t get_size_5() const { return ___size_5; }
	inline int32_t* get_address_of_size_5() { return &___size_5; }
	inline void set_size_5(int32_t value)
	{
		___size_5 = value;
	}

	inline static int32_t get_offset_of_up_6() { return static_cast<int32_t>(offsetof(RampAsset_t6D58ED782B43A6FC5555C2A22E5F767100D34C8E, ___up_6)); }
	inline bool get_up_6() const { return ___up_6; }
	inline bool* get_address_of_up_6() { return &___up_6; }
	inline void set_up_6(bool value)
	{
		___up_6 = value;
	}

	inline static int32_t get_offset_of_overwriteExisting_7() { return static_cast<int32_t>(offsetof(RampAsset_t6D58ED782B43A6FC5555C2A22E5F767100D34C8E, ___overwriteExisting_7)); }
	inline bool get_overwriteExisting_7() const { return ___overwriteExisting_7; }
	inline bool* get_address_of_overwriteExisting_7() { return &___overwriteExisting_7; }
	inline void set_overwriteExisting_7(bool value)
	{
		___overwriteExisting_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAMPASSET_T6D58ED782B43A6FC5555C2A22E5F767100D34C8E_H
#ifndef README_T3B6022BA9CBD2C56BD12C781DB20C5B34B6C4E46_H
#define README_T3B6022BA9CBD2C56BD12C781DB20C5B34B6C4E46_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Readme
struct  Readme_t3B6022BA9CBD2C56BD12C781DB20C5B34B6C4E46  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// UnityEngine.Texture2D Readme::icon
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___icon_4;
	// System.String Readme::title
	String_t* ___title_5;
	// Readme/Section[] Readme::sections
	SectionU5BU5D_t3EF1EA79AB7297D95C599BBA1E7D1DF5D69B5364* ___sections_6;
	// System.Boolean Readme::loadedLayout
	bool ___loadedLayout_7;

public:
	inline static int32_t get_offset_of_icon_4() { return static_cast<int32_t>(offsetof(Readme_t3B6022BA9CBD2C56BD12C781DB20C5B34B6C4E46, ___icon_4)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_icon_4() const { return ___icon_4; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_icon_4() { return &___icon_4; }
	inline void set_icon_4(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___icon_4 = value;
		Il2CppCodeGenWriteBarrier((&___icon_4), value);
	}

	inline static int32_t get_offset_of_title_5() { return static_cast<int32_t>(offsetof(Readme_t3B6022BA9CBD2C56BD12C781DB20C5B34B6C4E46, ___title_5)); }
	inline String_t* get_title_5() const { return ___title_5; }
	inline String_t** get_address_of_title_5() { return &___title_5; }
	inline void set_title_5(String_t* value)
	{
		___title_5 = value;
		Il2CppCodeGenWriteBarrier((&___title_5), value);
	}

	inline static int32_t get_offset_of_sections_6() { return static_cast<int32_t>(offsetof(Readme_t3B6022BA9CBD2C56BD12C781DB20C5B34B6C4E46, ___sections_6)); }
	inline SectionU5BU5D_t3EF1EA79AB7297D95C599BBA1E7D1DF5D69B5364* get_sections_6() const { return ___sections_6; }
	inline SectionU5BU5D_t3EF1EA79AB7297D95C599BBA1E7D1DF5D69B5364** get_address_of_sections_6() { return &___sections_6; }
	inline void set_sections_6(SectionU5BU5D_t3EF1EA79AB7297D95C599BBA1E7D1DF5D69B5364* value)
	{
		___sections_6 = value;
		Il2CppCodeGenWriteBarrier((&___sections_6), value);
	}

	inline static int32_t get_offset_of_loadedLayout_7() { return static_cast<int32_t>(offsetof(Readme_t3B6022BA9CBD2C56BD12C781DB20C5B34B6C4E46, ___loadedLayout_7)); }
	inline bool get_loadedLayout_7() const { return ___loadedLayout_7; }
	inline bool* get_address_of_loadedLayout_7() { return &___loadedLayout_7; }
	inline void set_loadedLayout_7(bool value)
	{
		___loadedLayout_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // README_T3B6022BA9CBD2C56BD12C781DB20C5B34B6C4E46_H
#ifndef WORDWRAPSTATE_T415B8622774DD094A9CD7447D298B33B7365A557_H
#define WORDWRAPSTATE_T415B8622774DD094A9CD7447D298B33B7365A557_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.WordWrapState
struct  WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557 
{
public:
	// System.Int32 TMPro.WordWrapState::previous_WordBreak
	int32_t ___previous_WordBreak_0;
	// System.Int32 TMPro.WordWrapState::total_CharacterCount
	int32_t ___total_CharacterCount_1;
	// System.Int32 TMPro.WordWrapState::visible_CharacterCount
	int32_t ___visible_CharacterCount_2;
	// System.Int32 TMPro.WordWrapState::visible_SpriteCount
	int32_t ___visible_SpriteCount_3;
	// System.Int32 TMPro.WordWrapState::visible_LinkCount
	int32_t ___visible_LinkCount_4;
	// System.Int32 TMPro.WordWrapState::firstCharacterIndex
	int32_t ___firstCharacterIndex_5;
	// System.Int32 TMPro.WordWrapState::firstVisibleCharacterIndex
	int32_t ___firstVisibleCharacterIndex_6;
	// System.Int32 TMPro.WordWrapState::lastCharacterIndex
	int32_t ___lastCharacterIndex_7;
	// System.Int32 TMPro.WordWrapState::lastVisibleCharIndex
	int32_t ___lastVisibleCharIndex_8;
	// System.Int32 TMPro.WordWrapState::lineNumber
	int32_t ___lineNumber_9;
	// System.Single TMPro.WordWrapState::maxCapHeight
	float ___maxCapHeight_10;
	// System.Single TMPro.WordWrapState::maxAscender
	float ___maxAscender_11;
	// System.Single TMPro.WordWrapState::maxDescender
	float ___maxDescender_12;
	// System.Single TMPro.WordWrapState::maxLineAscender
	float ___maxLineAscender_13;
	// System.Single TMPro.WordWrapState::maxLineDescender
	float ___maxLineDescender_14;
	// System.Single TMPro.WordWrapState::previousLineAscender
	float ___previousLineAscender_15;
	// System.Single TMPro.WordWrapState::xAdvance
	float ___xAdvance_16;
	// System.Single TMPro.WordWrapState::preferredWidth
	float ___preferredWidth_17;
	// System.Single TMPro.WordWrapState::preferredHeight
	float ___preferredHeight_18;
	// System.Single TMPro.WordWrapState::previousLineScale
	float ___previousLineScale_19;
	// System.Int32 TMPro.WordWrapState::wordCount
	int32_t ___wordCount_20;
	// TMPro.FontStyles TMPro.WordWrapState::fontStyle
	int32_t ___fontStyle_21;
	// System.Single TMPro.WordWrapState::fontScale
	float ___fontScale_22;
	// System.Single TMPro.WordWrapState::fontScaleMultiplier
	float ___fontScaleMultiplier_23;
	// System.Single TMPro.WordWrapState::currentFontSize
	float ___currentFontSize_24;
	// System.Single TMPro.WordWrapState::baselineOffset
	float ___baselineOffset_25;
	// System.Single TMPro.WordWrapState::lineOffset
	float ___lineOffset_26;
	// TMPro.TMP_TextInfo TMPro.WordWrapState::textInfo
	TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 * ___textInfo_27;
	// TMPro.TMP_LineInfo TMPro.WordWrapState::lineInfo
	TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442  ___lineInfo_28;
	// UnityEngine.Color32 TMPro.WordWrapState::vertexColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___vertexColor_29;
	// UnityEngine.Color32 TMPro.WordWrapState::underlineColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___underlineColor_30;
	// UnityEngine.Color32 TMPro.WordWrapState::strikethroughColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___strikethroughColor_31;
	// UnityEngine.Color32 TMPro.WordWrapState::highlightColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___highlightColor_32;
	// TMPro.TMP_BasicXmlTagStack TMPro.WordWrapState::basicStyleStack
	TMP_BasicXmlTagStack_tC5F410B9A4A8A8DE6351FA5F0FBF92F8E2CC8BF8  ___basicStyleStack_33;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.WordWrapState::colorStack
	TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314  ___colorStack_34;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.WordWrapState::underlineColorStack
	TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314  ___underlineColorStack_35;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.WordWrapState::strikethroughColorStack
	TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314  ___strikethroughColorStack_36;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.WordWrapState::highlightColorStack
	TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314  ___highlightColorStack_37;
	// TMPro.TMP_XmlTagStack`1<TMPro.TMP_ColorGradient> TMPro.WordWrapState::colorGradientStack
	TMP_XmlTagStack_1_t357AC63D4EC290220DBE5C99C30F9FD6FEF05063  ___colorGradientStack_38;
	// TMPro.TMP_XmlTagStack`1<System.Single> TMPro.WordWrapState::sizeStack
	TMP_XmlTagStack_1_t316D6C0717F14C09A6AC717AB85B452FC6ECBDE5  ___sizeStack_39;
	// TMPro.TMP_XmlTagStack`1<System.Single> TMPro.WordWrapState::indentStack
	TMP_XmlTagStack_1_t316D6C0717F14C09A6AC717AB85B452FC6ECBDE5  ___indentStack_40;
	// TMPro.TMP_XmlTagStack`1<System.Int32> TMPro.WordWrapState::fontWeightStack
	TMP_XmlTagStack_1_tF92254A81B8213FD65E61782291A45206F461683  ___fontWeightStack_41;
	// TMPro.TMP_XmlTagStack`1<System.Int32> TMPro.WordWrapState::styleStack
	TMP_XmlTagStack_1_tF92254A81B8213FD65E61782291A45206F461683  ___styleStack_42;
	// TMPro.TMP_XmlTagStack`1<System.Single> TMPro.WordWrapState::baselineStack
	TMP_XmlTagStack_1_t316D6C0717F14C09A6AC717AB85B452FC6ECBDE5  ___baselineStack_43;
	// TMPro.TMP_XmlTagStack`1<System.Int32> TMPro.WordWrapState::actionStack
	TMP_XmlTagStack_1_tF92254A81B8213FD65E61782291A45206F461683  ___actionStack_44;
	// TMPro.TMP_XmlTagStack`1<TMPro.MaterialReference> TMPro.WordWrapState::materialReferenceStack
	TMP_XmlTagStack_1_tE3F1A1DBF5C066777F7FD28E182B6A14FCCC8A99  ___materialReferenceStack_45;
	// TMPro.TMP_XmlTagStack`1<TMPro.TextAlignmentOptions> TMPro.WordWrapState::lineJustificationStack
	TMP_XmlTagStack_1_t79B32594D8EFF8CB8078E16347606D8C097A225C  ___lineJustificationStack_46;
	// System.Int32 TMPro.WordWrapState::spriteAnimationID
	int32_t ___spriteAnimationID_47;
	// TMPro.TMP_FontAsset TMPro.WordWrapState::currentFontAsset
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___currentFontAsset_48;
	// TMPro.TMP_SpriteAsset TMPro.WordWrapState::currentSpriteAsset
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * ___currentSpriteAsset_49;
	// UnityEngine.Material TMPro.WordWrapState::currentMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___currentMaterial_50;
	// System.Int32 TMPro.WordWrapState::currentMaterialIndex
	int32_t ___currentMaterialIndex_51;
	// TMPro.Extents TMPro.WordWrapState::meshExtents
	Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3  ___meshExtents_52;
	// System.Boolean TMPro.WordWrapState::tagNoParsing
	bool ___tagNoParsing_53;
	// System.Boolean TMPro.WordWrapState::isNonBreakingSpace
	bool ___isNonBreakingSpace_54;

public:
	inline static int32_t get_offset_of_previous_WordBreak_0() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___previous_WordBreak_0)); }
	inline int32_t get_previous_WordBreak_0() const { return ___previous_WordBreak_0; }
	inline int32_t* get_address_of_previous_WordBreak_0() { return &___previous_WordBreak_0; }
	inline void set_previous_WordBreak_0(int32_t value)
	{
		___previous_WordBreak_0 = value;
	}

	inline static int32_t get_offset_of_total_CharacterCount_1() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___total_CharacterCount_1)); }
	inline int32_t get_total_CharacterCount_1() const { return ___total_CharacterCount_1; }
	inline int32_t* get_address_of_total_CharacterCount_1() { return &___total_CharacterCount_1; }
	inline void set_total_CharacterCount_1(int32_t value)
	{
		___total_CharacterCount_1 = value;
	}

	inline static int32_t get_offset_of_visible_CharacterCount_2() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___visible_CharacterCount_2)); }
	inline int32_t get_visible_CharacterCount_2() const { return ___visible_CharacterCount_2; }
	inline int32_t* get_address_of_visible_CharacterCount_2() { return &___visible_CharacterCount_2; }
	inline void set_visible_CharacterCount_2(int32_t value)
	{
		___visible_CharacterCount_2 = value;
	}

	inline static int32_t get_offset_of_visible_SpriteCount_3() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___visible_SpriteCount_3)); }
	inline int32_t get_visible_SpriteCount_3() const { return ___visible_SpriteCount_3; }
	inline int32_t* get_address_of_visible_SpriteCount_3() { return &___visible_SpriteCount_3; }
	inline void set_visible_SpriteCount_3(int32_t value)
	{
		___visible_SpriteCount_3 = value;
	}

	inline static int32_t get_offset_of_visible_LinkCount_4() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___visible_LinkCount_4)); }
	inline int32_t get_visible_LinkCount_4() const { return ___visible_LinkCount_4; }
	inline int32_t* get_address_of_visible_LinkCount_4() { return &___visible_LinkCount_4; }
	inline void set_visible_LinkCount_4(int32_t value)
	{
		___visible_LinkCount_4 = value;
	}

	inline static int32_t get_offset_of_firstCharacterIndex_5() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___firstCharacterIndex_5)); }
	inline int32_t get_firstCharacterIndex_5() const { return ___firstCharacterIndex_5; }
	inline int32_t* get_address_of_firstCharacterIndex_5() { return &___firstCharacterIndex_5; }
	inline void set_firstCharacterIndex_5(int32_t value)
	{
		___firstCharacterIndex_5 = value;
	}

	inline static int32_t get_offset_of_firstVisibleCharacterIndex_6() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___firstVisibleCharacterIndex_6)); }
	inline int32_t get_firstVisibleCharacterIndex_6() const { return ___firstVisibleCharacterIndex_6; }
	inline int32_t* get_address_of_firstVisibleCharacterIndex_6() { return &___firstVisibleCharacterIndex_6; }
	inline void set_firstVisibleCharacterIndex_6(int32_t value)
	{
		___firstVisibleCharacterIndex_6 = value;
	}

	inline static int32_t get_offset_of_lastCharacterIndex_7() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___lastCharacterIndex_7)); }
	inline int32_t get_lastCharacterIndex_7() const { return ___lastCharacterIndex_7; }
	inline int32_t* get_address_of_lastCharacterIndex_7() { return &___lastCharacterIndex_7; }
	inline void set_lastCharacterIndex_7(int32_t value)
	{
		___lastCharacterIndex_7 = value;
	}

	inline static int32_t get_offset_of_lastVisibleCharIndex_8() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___lastVisibleCharIndex_8)); }
	inline int32_t get_lastVisibleCharIndex_8() const { return ___lastVisibleCharIndex_8; }
	inline int32_t* get_address_of_lastVisibleCharIndex_8() { return &___lastVisibleCharIndex_8; }
	inline void set_lastVisibleCharIndex_8(int32_t value)
	{
		___lastVisibleCharIndex_8 = value;
	}

	inline static int32_t get_offset_of_lineNumber_9() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___lineNumber_9)); }
	inline int32_t get_lineNumber_9() const { return ___lineNumber_9; }
	inline int32_t* get_address_of_lineNumber_9() { return &___lineNumber_9; }
	inline void set_lineNumber_9(int32_t value)
	{
		___lineNumber_9 = value;
	}

	inline static int32_t get_offset_of_maxCapHeight_10() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___maxCapHeight_10)); }
	inline float get_maxCapHeight_10() const { return ___maxCapHeight_10; }
	inline float* get_address_of_maxCapHeight_10() { return &___maxCapHeight_10; }
	inline void set_maxCapHeight_10(float value)
	{
		___maxCapHeight_10 = value;
	}

	inline static int32_t get_offset_of_maxAscender_11() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___maxAscender_11)); }
	inline float get_maxAscender_11() const { return ___maxAscender_11; }
	inline float* get_address_of_maxAscender_11() { return &___maxAscender_11; }
	inline void set_maxAscender_11(float value)
	{
		___maxAscender_11 = value;
	}

	inline static int32_t get_offset_of_maxDescender_12() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___maxDescender_12)); }
	inline float get_maxDescender_12() const { return ___maxDescender_12; }
	inline float* get_address_of_maxDescender_12() { return &___maxDescender_12; }
	inline void set_maxDescender_12(float value)
	{
		___maxDescender_12 = value;
	}

	inline static int32_t get_offset_of_maxLineAscender_13() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___maxLineAscender_13)); }
	inline float get_maxLineAscender_13() const { return ___maxLineAscender_13; }
	inline float* get_address_of_maxLineAscender_13() { return &___maxLineAscender_13; }
	inline void set_maxLineAscender_13(float value)
	{
		___maxLineAscender_13 = value;
	}

	inline static int32_t get_offset_of_maxLineDescender_14() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___maxLineDescender_14)); }
	inline float get_maxLineDescender_14() const { return ___maxLineDescender_14; }
	inline float* get_address_of_maxLineDescender_14() { return &___maxLineDescender_14; }
	inline void set_maxLineDescender_14(float value)
	{
		___maxLineDescender_14 = value;
	}

	inline static int32_t get_offset_of_previousLineAscender_15() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___previousLineAscender_15)); }
	inline float get_previousLineAscender_15() const { return ___previousLineAscender_15; }
	inline float* get_address_of_previousLineAscender_15() { return &___previousLineAscender_15; }
	inline void set_previousLineAscender_15(float value)
	{
		___previousLineAscender_15 = value;
	}

	inline static int32_t get_offset_of_xAdvance_16() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___xAdvance_16)); }
	inline float get_xAdvance_16() const { return ___xAdvance_16; }
	inline float* get_address_of_xAdvance_16() { return &___xAdvance_16; }
	inline void set_xAdvance_16(float value)
	{
		___xAdvance_16 = value;
	}

	inline static int32_t get_offset_of_preferredWidth_17() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___preferredWidth_17)); }
	inline float get_preferredWidth_17() const { return ___preferredWidth_17; }
	inline float* get_address_of_preferredWidth_17() { return &___preferredWidth_17; }
	inline void set_preferredWidth_17(float value)
	{
		___preferredWidth_17 = value;
	}

	inline static int32_t get_offset_of_preferredHeight_18() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___preferredHeight_18)); }
	inline float get_preferredHeight_18() const { return ___preferredHeight_18; }
	inline float* get_address_of_preferredHeight_18() { return &___preferredHeight_18; }
	inline void set_preferredHeight_18(float value)
	{
		___preferredHeight_18 = value;
	}

	inline static int32_t get_offset_of_previousLineScale_19() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___previousLineScale_19)); }
	inline float get_previousLineScale_19() const { return ___previousLineScale_19; }
	inline float* get_address_of_previousLineScale_19() { return &___previousLineScale_19; }
	inline void set_previousLineScale_19(float value)
	{
		___previousLineScale_19 = value;
	}

	inline static int32_t get_offset_of_wordCount_20() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___wordCount_20)); }
	inline int32_t get_wordCount_20() const { return ___wordCount_20; }
	inline int32_t* get_address_of_wordCount_20() { return &___wordCount_20; }
	inline void set_wordCount_20(int32_t value)
	{
		___wordCount_20 = value;
	}

	inline static int32_t get_offset_of_fontStyle_21() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___fontStyle_21)); }
	inline int32_t get_fontStyle_21() const { return ___fontStyle_21; }
	inline int32_t* get_address_of_fontStyle_21() { return &___fontStyle_21; }
	inline void set_fontStyle_21(int32_t value)
	{
		___fontStyle_21 = value;
	}

	inline static int32_t get_offset_of_fontScale_22() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___fontScale_22)); }
	inline float get_fontScale_22() const { return ___fontScale_22; }
	inline float* get_address_of_fontScale_22() { return &___fontScale_22; }
	inline void set_fontScale_22(float value)
	{
		___fontScale_22 = value;
	}

	inline static int32_t get_offset_of_fontScaleMultiplier_23() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___fontScaleMultiplier_23)); }
	inline float get_fontScaleMultiplier_23() const { return ___fontScaleMultiplier_23; }
	inline float* get_address_of_fontScaleMultiplier_23() { return &___fontScaleMultiplier_23; }
	inline void set_fontScaleMultiplier_23(float value)
	{
		___fontScaleMultiplier_23 = value;
	}

	inline static int32_t get_offset_of_currentFontSize_24() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___currentFontSize_24)); }
	inline float get_currentFontSize_24() const { return ___currentFontSize_24; }
	inline float* get_address_of_currentFontSize_24() { return &___currentFontSize_24; }
	inline void set_currentFontSize_24(float value)
	{
		___currentFontSize_24 = value;
	}

	inline static int32_t get_offset_of_baselineOffset_25() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___baselineOffset_25)); }
	inline float get_baselineOffset_25() const { return ___baselineOffset_25; }
	inline float* get_address_of_baselineOffset_25() { return &___baselineOffset_25; }
	inline void set_baselineOffset_25(float value)
	{
		___baselineOffset_25 = value;
	}

	inline static int32_t get_offset_of_lineOffset_26() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___lineOffset_26)); }
	inline float get_lineOffset_26() const { return ___lineOffset_26; }
	inline float* get_address_of_lineOffset_26() { return &___lineOffset_26; }
	inline void set_lineOffset_26(float value)
	{
		___lineOffset_26 = value;
	}

	inline static int32_t get_offset_of_textInfo_27() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___textInfo_27)); }
	inline TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 * get_textInfo_27() const { return ___textInfo_27; }
	inline TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 ** get_address_of_textInfo_27() { return &___textInfo_27; }
	inline void set_textInfo_27(TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 * value)
	{
		___textInfo_27 = value;
		Il2CppCodeGenWriteBarrier((&___textInfo_27), value);
	}

	inline static int32_t get_offset_of_lineInfo_28() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___lineInfo_28)); }
	inline TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442  get_lineInfo_28() const { return ___lineInfo_28; }
	inline TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442 * get_address_of_lineInfo_28() { return &___lineInfo_28; }
	inline void set_lineInfo_28(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442  value)
	{
		___lineInfo_28 = value;
	}

	inline static int32_t get_offset_of_vertexColor_29() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___vertexColor_29)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_vertexColor_29() const { return ___vertexColor_29; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_vertexColor_29() { return &___vertexColor_29; }
	inline void set_vertexColor_29(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___vertexColor_29 = value;
	}

	inline static int32_t get_offset_of_underlineColor_30() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___underlineColor_30)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_underlineColor_30() const { return ___underlineColor_30; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_underlineColor_30() { return &___underlineColor_30; }
	inline void set_underlineColor_30(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___underlineColor_30 = value;
	}

	inline static int32_t get_offset_of_strikethroughColor_31() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___strikethroughColor_31)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_strikethroughColor_31() const { return ___strikethroughColor_31; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_strikethroughColor_31() { return &___strikethroughColor_31; }
	inline void set_strikethroughColor_31(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___strikethroughColor_31 = value;
	}

	inline static int32_t get_offset_of_highlightColor_32() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___highlightColor_32)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_highlightColor_32() const { return ___highlightColor_32; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_highlightColor_32() { return &___highlightColor_32; }
	inline void set_highlightColor_32(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___highlightColor_32 = value;
	}

	inline static int32_t get_offset_of_basicStyleStack_33() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___basicStyleStack_33)); }
	inline TMP_BasicXmlTagStack_tC5F410B9A4A8A8DE6351FA5F0FBF92F8E2CC8BF8  get_basicStyleStack_33() const { return ___basicStyleStack_33; }
	inline TMP_BasicXmlTagStack_tC5F410B9A4A8A8DE6351FA5F0FBF92F8E2CC8BF8 * get_address_of_basicStyleStack_33() { return &___basicStyleStack_33; }
	inline void set_basicStyleStack_33(TMP_BasicXmlTagStack_tC5F410B9A4A8A8DE6351FA5F0FBF92F8E2CC8BF8  value)
	{
		___basicStyleStack_33 = value;
	}

	inline static int32_t get_offset_of_colorStack_34() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___colorStack_34)); }
	inline TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314  get_colorStack_34() const { return ___colorStack_34; }
	inline TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314 * get_address_of_colorStack_34() { return &___colorStack_34; }
	inline void set_colorStack_34(TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314  value)
	{
		___colorStack_34 = value;
	}

	inline static int32_t get_offset_of_underlineColorStack_35() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___underlineColorStack_35)); }
	inline TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314  get_underlineColorStack_35() const { return ___underlineColorStack_35; }
	inline TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314 * get_address_of_underlineColorStack_35() { return &___underlineColorStack_35; }
	inline void set_underlineColorStack_35(TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314  value)
	{
		___underlineColorStack_35 = value;
	}

	inline static int32_t get_offset_of_strikethroughColorStack_36() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___strikethroughColorStack_36)); }
	inline TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314  get_strikethroughColorStack_36() const { return ___strikethroughColorStack_36; }
	inline TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314 * get_address_of_strikethroughColorStack_36() { return &___strikethroughColorStack_36; }
	inline void set_strikethroughColorStack_36(TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314  value)
	{
		___strikethroughColorStack_36 = value;
	}

	inline static int32_t get_offset_of_highlightColorStack_37() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___highlightColorStack_37)); }
	inline TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314  get_highlightColorStack_37() const { return ___highlightColorStack_37; }
	inline TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314 * get_address_of_highlightColorStack_37() { return &___highlightColorStack_37; }
	inline void set_highlightColorStack_37(TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314  value)
	{
		___highlightColorStack_37 = value;
	}

	inline static int32_t get_offset_of_colorGradientStack_38() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___colorGradientStack_38)); }
	inline TMP_XmlTagStack_1_t357AC63D4EC290220DBE5C99C30F9FD6FEF05063  get_colorGradientStack_38() const { return ___colorGradientStack_38; }
	inline TMP_XmlTagStack_1_t357AC63D4EC290220DBE5C99C30F9FD6FEF05063 * get_address_of_colorGradientStack_38() { return &___colorGradientStack_38; }
	inline void set_colorGradientStack_38(TMP_XmlTagStack_1_t357AC63D4EC290220DBE5C99C30F9FD6FEF05063  value)
	{
		___colorGradientStack_38 = value;
	}

	inline static int32_t get_offset_of_sizeStack_39() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___sizeStack_39)); }
	inline TMP_XmlTagStack_1_t316D6C0717F14C09A6AC717AB85B452FC6ECBDE5  get_sizeStack_39() const { return ___sizeStack_39; }
	inline TMP_XmlTagStack_1_t316D6C0717F14C09A6AC717AB85B452FC6ECBDE5 * get_address_of_sizeStack_39() { return &___sizeStack_39; }
	inline void set_sizeStack_39(TMP_XmlTagStack_1_t316D6C0717F14C09A6AC717AB85B452FC6ECBDE5  value)
	{
		___sizeStack_39 = value;
	}

	inline static int32_t get_offset_of_indentStack_40() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___indentStack_40)); }
	inline TMP_XmlTagStack_1_t316D6C0717F14C09A6AC717AB85B452FC6ECBDE5  get_indentStack_40() const { return ___indentStack_40; }
	inline TMP_XmlTagStack_1_t316D6C0717F14C09A6AC717AB85B452FC6ECBDE5 * get_address_of_indentStack_40() { return &___indentStack_40; }
	inline void set_indentStack_40(TMP_XmlTagStack_1_t316D6C0717F14C09A6AC717AB85B452FC6ECBDE5  value)
	{
		___indentStack_40 = value;
	}

	inline static int32_t get_offset_of_fontWeightStack_41() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___fontWeightStack_41)); }
	inline TMP_XmlTagStack_1_tF92254A81B8213FD65E61782291A45206F461683  get_fontWeightStack_41() const { return ___fontWeightStack_41; }
	inline TMP_XmlTagStack_1_tF92254A81B8213FD65E61782291A45206F461683 * get_address_of_fontWeightStack_41() { return &___fontWeightStack_41; }
	inline void set_fontWeightStack_41(TMP_XmlTagStack_1_tF92254A81B8213FD65E61782291A45206F461683  value)
	{
		___fontWeightStack_41 = value;
	}

	inline static int32_t get_offset_of_styleStack_42() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___styleStack_42)); }
	inline TMP_XmlTagStack_1_tF92254A81B8213FD65E61782291A45206F461683  get_styleStack_42() const { return ___styleStack_42; }
	inline TMP_XmlTagStack_1_tF92254A81B8213FD65E61782291A45206F461683 * get_address_of_styleStack_42() { return &___styleStack_42; }
	inline void set_styleStack_42(TMP_XmlTagStack_1_tF92254A81B8213FD65E61782291A45206F461683  value)
	{
		___styleStack_42 = value;
	}

	inline static int32_t get_offset_of_baselineStack_43() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___baselineStack_43)); }
	inline TMP_XmlTagStack_1_t316D6C0717F14C09A6AC717AB85B452FC6ECBDE5  get_baselineStack_43() const { return ___baselineStack_43; }
	inline TMP_XmlTagStack_1_t316D6C0717F14C09A6AC717AB85B452FC6ECBDE5 * get_address_of_baselineStack_43() { return &___baselineStack_43; }
	inline void set_baselineStack_43(TMP_XmlTagStack_1_t316D6C0717F14C09A6AC717AB85B452FC6ECBDE5  value)
	{
		___baselineStack_43 = value;
	}

	inline static int32_t get_offset_of_actionStack_44() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___actionStack_44)); }
	inline TMP_XmlTagStack_1_tF92254A81B8213FD65E61782291A45206F461683  get_actionStack_44() const { return ___actionStack_44; }
	inline TMP_XmlTagStack_1_tF92254A81B8213FD65E61782291A45206F461683 * get_address_of_actionStack_44() { return &___actionStack_44; }
	inline void set_actionStack_44(TMP_XmlTagStack_1_tF92254A81B8213FD65E61782291A45206F461683  value)
	{
		___actionStack_44 = value;
	}

	inline static int32_t get_offset_of_materialReferenceStack_45() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___materialReferenceStack_45)); }
	inline TMP_XmlTagStack_1_tE3F1A1DBF5C066777F7FD28E182B6A14FCCC8A99  get_materialReferenceStack_45() const { return ___materialReferenceStack_45; }
	inline TMP_XmlTagStack_1_tE3F1A1DBF5C066777F7FD28E182B6A14FCCC8A99 * get_address_of_materialReferenceStack_45() { return &___materialReferenceStack_45; }
	inline void set_materialReferenceStack_45(TMP_XmlTagStack_1_tE3F1A1DBF5C066777F7FD28E182B6A14FCCC8A99  value)
	{
		___materialReferenceStack_45 = value;
	}

	inline static int32_t get_offset_of_lineJustificationStack_46() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___lineJustificationStack_46)); }
	inline TMP_XmlTagStack_1_t79B32594D8EFF8CB8078E16347606D8C097A225C  get_lineJustificationStack_46() const { return ___lineJustificationStack_46; }
	inline TMP_XmlTagStack_1_t79B32594D8EFF8CB8078E16347606D8C097A225C * get_address_of_lineJustificationStack_46() { return &___lineJustificationStack_46; }
	inline void set_lineJustificationStack_46(TMP_XmlTagStack_1_t79B32594D8EFF8CB8078E16347606D8C097A225C  value)
	{
		___lineJustificationStack_46 = value;
	}

	inline static int32_t get_offset_of_spriteAnimationID_47() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___spriteAnimationID_47)); }
	inline int32_t get_spriteAnimationID_47() const { return ___spriteAnimationID_47; }
	inline int32_t* get_address_of_spriteAnimationID_47() { return &___spriteAnimationID_47; }
	inline void set_spriteAnimationID_47(int32_t value)
	{
		___spriteAnimationID_47 = value;
	}

	inline static int32_t get_offset_of_currentFontAsset_48() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___currentFontAsset_48)); }
	inline TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * get_currentFontAsset_48() const { return ___currentFontAsset_48; }
	inline TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C ** get_address_of_currentFontAsset_48() { return &___currentFontAsset_48; }
	inline void set_currentFontAsset_48(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * value)
	{
		___currentFontAsset_48 = value;
		Il2CppCodeGenWriteBarrier((&___currentFontAsset_48), value);
	}

	inline static int32_t get_offset_of_currentSpriteAsset_49() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___currentSpriteAsset_49)); }
	inline TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * get_currentSpriteAsset_49() const { return ___currentSpriteAsset_49; }
	inline TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 ** get_address_of_currentSpriteAsset_49() { return &___currentSpriteAsset_49; }
	inline void set_currentSpriteAsset_49(TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * value)
	{
		___currentSpriteAsset_49 = value;
		Il2CppCodeGenWriteBarrier((&___currentSpriteAsset_49), value);
	}

	inline static int32_t get_offset_of_currentMaterial_50() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___currentMaterial_50)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_currentMaterial_50() const { return ___currentMaterial_50; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_currentMaterial_50() { return &___currentMaterial_50; }
	inline void set_currentMaterial_50(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___currentMaterial_50 = value;
		Il2CppCodeGenWriteBarrier((&___currentMaterial_50), value);
	}

	inline static int32_t get_offset_of_currentMaterialIndex_51() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___currentMaterialIndex_51)); }
	inline int32_t get_currentMaterialIndex_51() const { return ___currentMaterialIndex_51; }
	inline int32_t* get_address_of_currentMaterialIndex_51() { return &___currentMaterialIndex_51; }
	inline void set_currentMaterialIndex_51(int32_t value)
	{
		___currentMaterialIndex_51 = value;
	}

	inline static int32_t get_offset_of_meshExtents_52() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___meshExtents_52)); }
	inline Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3  get_meshExtents_52() const { return ___meshExtents_52; }
	inline Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3 * get_address_of_meshExtents_52() { return &___meshExtents_52; }
	inline void set_meshExtents_52(Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3  value)
	{
		___meshExtents_52 = value;
	}

	inline static int32_t get_offset_of_tagNoParsing_53() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___tagNoParsing_53)); }
	inline bool get_tagNoParsing_53() const { return ___tagNoParsing_53; }
	inline bool* get_address_of_tagNoParsing_53() { return &___tagNoParsing_53; }
	inline void set_tagNoParsing_53(bool value)
	{
		___tagNoParsing_53 = value;
	}

	inline static int32_t get_offset_of_isNonBreakingSpace_54() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___isNonBreakingSpace_54)); }
	inline bool get_isNonBreakingSpace_54() const { return ___isNonBreakingSpace_54; }
	inline bool* get_address_of_isNonBreakingSpace_54() { return &___isNonBreakingSpace_54; }
	inline void set_isNonBreakingSpace_54(bool value)
	{
		___isNonBreakingSpace_54 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.WordWrapState
struct WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557_marshaled_pinvoke
{
	int32_t ___previous_WordBreak_0;
	int32_t ___total_CharacterCount_1;
	int32_t ___visible_CharacterCount_2;
	int32_t ___visible_SpriteCount_3;
	int32_t ___visible_LinkCount_4;
	int32_t ___firstCharacterIndex_5;
	int32_t ___firstVisibleCharacterIndex_6;
	int32_t ___lastCharacterIndex_7;
	int32_t ___lastVisibleCharIndex_8;
	int32_t ___lineNumber_9;
	float ___maxCapHeight_10;
	float ___maxAscender_11;
	float ___maxDescender_12;
	float ___maxLineAscender_13;
	float ___maxLineDescender_14;
	float ___previousLineAscender_15;
	float ___xAdvance_16;
	float ___preferredWidth_17;
	float ___preferredHeight_18;
	float ___previousLineScale_19;
	int32_t ___wordCount_20;
	int32_t ___fontStyle_21;
	float ___fontScale_22;
	float ___fontScaleMultiplier_23;
	float ___currentFontSize_24;
	float ___baselineOffset_25;
	float ___lineOffset_26;
	TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 * ___textInfo_27;
	TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442  ___lineInfo_28;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___vertexColor_29;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___underlineColor_30;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___strikethroughColor_31;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___highlightColor_32;
	TMP_BasicXmlTagStack_tC5F410B9A4A8A8DE6351FA5F0FBF92F8E2CC8BF8  ___basicStyleStack_33;
	TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314  ___colorStack_34;
	TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314  ___underlineColorStack_35;
	TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314  ___strikethroughColorStack_36;
	TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314  ___highlightColorStack_37;
	TMP_XmlTagStack_1_t357AC63D4EC290220DBE5C99C30F9FD6FEF05063  ___colorGradientStack_38;
	TMP_XmlTagStack_1_t316D6C0717F14C09A6AC717AB85B452FC6ECBDE5  ___sizeStack_39;
	TMP_XmlTagStack_1_t316D6C0717F14C09A6AC717AB85B452FC6ECBDE5  ___indentStack_40;
	TMP_XmlTagStack_1_tF92254A81B8213FD65E61782291A45206F461683  ___fontWeightStack_41;
	TMP_XmlTagStack_1_tF92254A81B8213FD65E61782291A45206F461683  ___styleStack_42;
	TMP_XmlTagStack_1_t316D6C0717F14C09A6AC717AB85B452FC6ECBDE5  ___baselineStack_43;
	TMP_XmlTagStack_1_tF92254A81B8213FD65E61782291A45206F461683  ___actionStack_44;
	TMP_XmlTagStack_1_tE3F1A1DBF5C066777F7FD28E182B6A14FCCC8A99  ___materialReferenceStack_45;
	TMP_XmlTagStack_1_t79B32594D8EFF8CB8078E16347606D8C097A225C  ___lineJustificationStack_46;
	int32_t ___spriteAnimationID_47;
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___currentFontAsset_48;
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * ___currentSpriteAsset_49;
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___currentMaterial_50;
	int32_t ___currentMaterialIndex_51;
	Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3  ___meshExtents_52;
	int32_t ___tagNoParsing_53;
	int32_t ___isNonBreakingSpace_54;
};
// Native definition for COM marshalling of TMPro.WordWrapState
struct WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557_marshaled_com
{
	int32_t ___previous_WordBreak_0;
	int32_t ___total_CharacterCount_1;
	int32_t ___visible_CharacterCount_2;
	int32_t ___visible_SpriteCount_3;
	int32_t ___visible_LinkCount_4;
	int32_t ___firstCharacterIndex_5;
	int32_t ___firstVisibleCharacterIndex_6;
	int32_t ___lastCharacterIndex_7;
	int32_t ___lastVisibleCharIndex_8;
	int32_t ___lineNumber_9;
	float ___maxCapHeight_10;
	float ___maxAscender_11;
	float ___maxDescender_12;
	float ___maxLineAscender_13;
	float ___maxLineDescender_14;
	float ___previousLineAscender_15;
	float ___xAdvance_16;
	float ___preferredWidth_17;
	float ___preferredHeight_18;
	float ___previousLineScale_19;
	int32_t ___wordCount_20;
	int32_t ___fontStyle_21;
	float ___fontScale_22;
	float ___fontScaleMultiplier_23;
	float ___currentFontSize_24;
	float ___baselineOffset_25;
	float ___lineOffset_26;
	TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 * ___textInfo_27;
	TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442  ___lineInfo_28;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___vertexColor_29;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___underlineColor_30;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___strikethroughColor_31;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___highlightColor_32;
	TMP_BasicXmlTagStack_tC5F410B9A4A8A8DE6351FA5F0FBF92F8E2CC8BF8  ___basicStyleStack_33;
	TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314  ___colorStack_34;
	TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314  ___underlineColorStack_35;
	TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314  ___strikethroughColorStack_36;
	TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314  ___highlightColorStack_37;
	TMP_XmlTagStack_1_t357AC63D4EC290220DBE5C99C30F9FD6FEF05063  ___colorGradientStack_38;
	TMP_XmlTagStack_1_t316D6C0717F14C09A6AC717AB85B452FC6ECBDE5  ___sizeStack_39;
	TMP_XmlTagStack_1_t316D6C0717F14C09A6AC717AB85B452FC6ECBDE5  ___indentStack_40;
	TMP_XmlTagStack_1_tF92254A81B8213FD65E61782291A45206F461683  ___fontWeightStack_41;
	TMP_XmlTagStack_1_tF92254A81B8213FD65E61782291A45206F461683  ___styleStack_42;
	TMP_XmlTagStack_1_t316D6C0717F14C09A6AC717AB85B452FC6ECBDE5  ___baselineStack_43;
	TMP_XmlTagStack_1_tF92254A81B8213FD65E61782291A45206F461683  ___actionStack_44;
	TMP_XmlTagStack_1_tE3F1A1DBF5C066777F7FD28E182B6A14FCCC8A99  ___materialReferenceStack_45;
	TMP_XmlTagStack_1_t79B32594D8EFF8CB8078E16347606D8C097A225C  ___lineJustificationStack_46;
	int32_t ___spriteAnimationID_47;
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___currentFontAsset_48;
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * ___currentSpriteAsset_49;
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___currentMaterial_50;
	int32_t ___currentMaterialIndex_51;
	Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3  ___meshExtents_52;
	int32_t ___tagNoParsing_53;
	int32_t ___isNonBreakingSpace_54;
};
#endif // WORDWRAPSTATE_T415B8622774DD094A9CD7447D298B33B7365A557_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef ADJUSTTIMESCALE_T727E1C2C5087BF85DD8A30E8BB94C8F86F220405_H
#define ADJUSTTIMESCALE_T727E1C2C5087BF85DD8A30E8BB94C8F86F220405_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AdjustTimeScale
struct  AdjustTimeScale_t727E1C2C5087BF85DD8A30E8BB94C8F86F220405  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// TMPro.TextMeshProUGUI AdjustTimeScale::textMesh
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ___textMesh_4;

public:
	inline static int32_t get_offset_of_textMesh_4() { return static_cast<int32_t>(offsetof(AdjustTimeScale_t727E1C2C5087BF85DD8A30E8BB94C8F86F220405, ___textMesh_4)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get_textMesh_4() const { return ___textMesh_4; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of_textMesh_4() { return &___textMesh_4; }
	inline void set_textMesh_4(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		___textMesh_4 = value;
		Il2CppCodeGenWriteBarrier((&___textMesh_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADJUSTTIMESCALE_T727E1C2C5087BF85DD8A30E8BB94C8F86F220405_H
#ifndef CAMERACONTROLLER_TC58777A506A1D4F1F1794E5D666E619C1DA073BB_H
#define CAMERACONTROLLER_TC58777A506A1D4F1F1794E5D666E619C1DA073BB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraController
struct  CameraController_tC58777A506A1D4F1F1794E5D666E619C1DA073BB  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject CameraController::player
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___player_4;
	// UnityEngine.Vector3 CameraController::offset
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___offset_5;

public:
	inline static int32_t get_offset_of_player_4() { return static_cast<int32_t>(offsetof(CameraController_tC58777A506A1D4F1F1794E5D666E619C1DA073BB, ___player_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_player_4() const { return ___player_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_player_4() { return &___player_4; }
	inline void set_player_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___player_4 = value;
		Il2CppCodeGenWriteBarrier((&___player_4), value);
	}

	inline static int32_t get_offset_of_offset_5() { return static_cast<int32_t>(offsetof(CameraController_tC58777A506A1D4F1F1794E5D666E619C1DA073BB, ___offset_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_offset_5() const { return ___offset_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_offset_5() { return &___offset_5; }
	inline void set_offset_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___offset_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERACONTROLLER_TC58777A506A1D4F1F1794E5D666E619C1DA073BB_H
#ifndef PLAYERCONTROLLER_T4CE339054014370D89B89922EDC0EA2766589C85_H
#define PLAYERCONTROLLER_T4CE339054014370D89B89922EDC0EA2766589C85_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerController
struct  PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single PlayerController::speed
	float ___speed_4;
	// UnityEngine.UI.Text PlayerController::countText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___countText_5;
	// UnityEngine.Rigidbody PlayerController::rb
	Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * ___rb_6;
	// System.Int32 PlayerController::count
	int32_t ___count_7;
	// System.String[] PlayerController::messages
	StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* ___messages_8;
	// UnityEngine.ParticleSystem PlayerController::BigExplosion
	ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * ___BigExplosion_9;

public:
	inline static int32_t get_offset_of_speed_4() { return static_cast<int32_t>(offsetof(PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85, ___speed_4)); }
	inline float get_speed_4() const { return ___speed_4; }
	inline float* get_address_of_speed_4() { return &___speed_4; }
	inline void set_speed_4(float value)
	{
		___speed_4 = value;
	}

	inline static int32_t get_offset_of_countText_5() { return static_cast<int32_t>(offsetof(PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85, ___countText_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_countText_5() const { return ___countText_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_countText_5() { return &___countText_5; }
	inline void set_countText_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___countText_5 = value;
		Il2CppCodeGenWriteBarrier((&___countText_5), value);
	}

	inline static int32_t get_offset_of_rb_6() { return static_cast<int32_t>(offsetof(PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85, ___rb_6)); }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * get_rb_6() const { return ___rb_6; }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 ** get_address_of_rb_6() { return &___rb_6; }
	inline void set_rb_6(Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * value)
	{
		___rb_6 = value;
		Il2CppCodeGenWriteBarrier((&___rb_6), value);
	}

	inline static int32_t get_offset_of_count_7() { return static_cast<int32_t>(offsetof(PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85, ___count_7)); }
	inline int32_t get_count_7() const { return ___count_7; }
	inline int32_t* get_address_of_count_7() { return &___count_7; }
	inline void set_count_7(int32_t value)
	{
		___count_7 = value;
	}

	inline static int32_t get_offset_of_messages_8() { return static_cast<int32_t>(offsetof(PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85, ___messages_8)); }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* get_messages_8() const { return ___messages_8; }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B** get_address_of_messages_8() { return &___messages_8; }
	inline void set_messages_8(StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* value)
	{
		___messages_8 = value;
		Il2CppCodeGenWriteBarrier((&___messages_8), value);
	}

	inline static int32_t get_offset_of_BigExplosion_9() { return static_cast<int32_t>(offsetof(PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85, ___BigExplosion_9)); }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * get_BigExplosion_9() const { return ___BigExplosion_9; }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D ** get_address_of_BigExplosion_9() { return &___BigExplosion_9; }
	inline void set_BigExplosion_9(ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * value)
	{
		___BigExplosion_9 = value;
		Il2CppCodeGenWriteBarrier((&___BigExplosion_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERCONTROLLER_T4CE339054014370D89B89922EDC0EA2766589C85_H
#ifndef PROXIMITYACTIVATE_T403C541F04433D4393C70B64CF490E9E6C5189F4_H
#define PROXIMITYACTIVATE_T403C541F04433D4393C70B64CF490E9E6C5189F4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ProximityActivate
struct  ProximityActivate_t403C541F04433D4393C70B64CF490E9E6C5189F4  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Transform ProximityActivate::distanceActivator
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___distanceActivator_4;
	// UnityEngine.Transform ProximityActivate::lookAtActivator
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___lookAtActivator_5;
	// System.Single ProximityActivate::distance
	float ___distance_6;
	// UnityEngine.Transform ProximityActivate::activator
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___activator_7;
	// System.Boolean ProximityActivate::activeState
	bool ___activeState_8;
	// UnityEngine.CanvasGroup ProximityActivate::target
	CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * ___target_9;
	// System.Boolean ProximityActivate::lookAtCamera
	bool ___lookAtCamera_10;
	// System.Boolean ProximityActivate::enableInfoPanel
	bool ___enableInfoPanel_11;
	// UnityEngine.GameObject ProximityActivate::infoIcon
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___infoIcon_12;
	// System.Single ProximityActivate::alpha
	float ___alpha_13;
	// UnityEngine.CanvasGroup ProximityActivate::infoPanel
	CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * ___infoPanel_14;
	// UnityEngine.Quaternion ProximityActivate::originRotation
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___originRotation_15;
	// UnityEngine.Quaternion ProximityActivate::targetRotation
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___targetRotation_16;

public:
	inline static int32_t get_offset_of_distanceActivator_4() { return static_cast<int32_t>(offsetof(ProximityActivate_t403C541F04433D4393C70B64CF490E9E6C5189F4, ___distanceActivator_4)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_distanceActivator_4() const { return ___distanceActivator_4; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_distanceActivator_4() { return &___distanceActivator_4; }
	inline void set_distanceActivator_4(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___distanceActivator_4 = value;
		Il2CppCodeGenWriteBarrier((&___distanceActivator_4), value);
	}

	inline static int32_t get_offset_of_lookAtActivator_5() { return static_cast<int32_t>(offsetof(ProximityActivate_t403C541F04433D4393C70B64CF490E9E6C5189F4, ___lookAtActivator_5)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_lookAtActivator_5() const { return ___lookAtActivator_5; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_lookAtActivator_5() { return &___lookAtActivator_5; }
	inline void set_lookAtActivator_5(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___lookAtActivator_5 = value;
		Il2CppCodeGenWriteBarrier((&___lookAtActivator_5), value);
	}

	inline static int32_t get_offset_of_distance_6() { return static_cast<int32_t>(offsetof(ProximityActivate_t403C541F04433D4393C70B64CF490E9E6C5189F4, ___distance_6)); }
	inline float get_distance_6() const { return ___distance_6; }
	inline float* get_address_of_distance_6() { return &___distance_6; }
	inline void set_distance_6(float value)
	{
		___distance_6 = value;
	}

	inline static int32_t get_offset_of_activator_7() { return static_cast<int32_t>(offsetof(ProximityActivate_t403C541F04433D4393C70B64CF490E9E6C5189F4, ___activator_7)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_activator_7() const { return ___activator_7; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_activator_7() { return &___activator_7; }
	inline void set_activator_7(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___activator_7 = value;
		Il2CppCodeGenWriteBarrier((&___activator_7), value);
	}

	inline static int32_t get_offset_of_activeState_8() { return static_cast<int32_t>(offsetof(ProximityActivate_t403C541F04433D4393C70B64CF490E9E6C5189F4, ___activeState_8)); }
	inline bool get_activeState_8() const { return ___activeState_8; }
	inline bool* get_address_of_activeState_8() { return &___activeState_8; }
	inline void set_activeState_8(bool value)
	{
		___activeState_8 = value;
	}

	inline static int32_t get_offset_of_target_9() { return static_cast<int32_t>(offsetof(ProximityActivate_t403C541F04433D4393C70B64CF490E9E6C5189F4, ___target_9)); }
	inline CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * get_target_9() const { return ___target_9; }
	inline CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 ** get_address_of_target_9() { return &___target_9; }
	inline void set_target_9(CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * value)
	{
		___target_9 = value;
		Il2CppCodeGenWriteBarrier((&___target_9), value);
	}

	inline static int32_t get_offset_of_lookAtCamera_10() { return static_cast<int32_t>(offsetof(ProximityActivate_t403C541F04433D4393C70B64CF490E9E6C5189F4, ___lookAtCamera_10)); }
	inline bool get_lookAtCamera_10() const { return ___lookAtCamera_10; }
	inline bool* get_address_of_lookAtCamera_10() { return &___lookAtCamera_10; }
	inline void set_lookAtCamera_10(bool value)
	{
		___lookAtCamera_10 = value;
	}

	inline static int32_t get_offset_of_enableInfoPanel_11() { return static_cast<int32_t>(offsetof(ProximityActivate_t403C541F04433D4393C70B64CF490E9E6C5189F4, ___enableInfoPanel_11)); }
	inline bool get_enableInfoPanel_11() const { return ___enableInfoPanel_11; }
	inline bool* get_address_of_enableInfoPanel_11() { return &___enableInfoPanel_11; }
	inline void set_enableInfoPanel_11(bool value)
	{
		___enableInfoPanel_11 = value;
	}

	inline static int32_t get_offset_of_infoIcon_12() { return static_cast<int32_t>(offsetof(ProximityActivate_t403C541F04433D4393C70B64CF490E9E6C5189F4, ___infoIcon_12)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_infoIcon_12() const { return ___infoIcon_12; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_infoIcon_12() { return &___infoIcon_12; }
	inline void set_infoIcon_12(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___infoIcon_12 = value;
		Il2CppCodeGenWriteBarrier((&___infoIcon_12), value);
	}

	inline static int32_t get_offset_of_alpha_13() { return static_cast<int32_t>(offsetof(ProximityActivate_t403C541F04433D4393C70B64CF490E9E6C5189F4, ___alpha_13)); }
	inline float get_alpha_13() const { return ___alpha_13; }
	inline float* get_address_of_alpha_13() { return &___alpha_13; }
	inline void set_alpha_13(float value)
	{
		___alpha_13 = value;
	}

	inline static int32_t get_offset_of_infoPanel_14() { return static_cast<int32_t>(offsetof(ProximityActivate_t403C541F04433D4393C70B64CF490E9E6C5189F4, ___infoPanel_14)); }
	inline CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * get_infoPanel_14() const { return ___infoPanel_14; }
	inline CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 ** get_address_of_infoPanel_14() { return &___infoPanel_14; }
	inline void set_infoPanel_14(CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * value)
	{
		___infoPanel_14 = value;
		Il2CppCodeGenWriteBarrier((&___infoPanel_14), value);
	}

	inline static int32_t get_offset_of_originRotation_15() { return static_cast<int32_t>(offsetof(ProximityActivate_t403C541F04433D4393C70B64CF490E9E6C5189F4, ___originRotation_15)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_originRotation_15() const { return ___originRotation_15; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_originRotation_15() { return &___originRotation_15; }
	inline void set_originRotation_15(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___originRotation_15 = value;
	}

	inline static int32_t get_offset_of_targetRotation_16() { return static_cast<int32_t>(offsetof(ProximityActivate_t403C541F04433D4393C70B64CF490E9E6C5189F4, ___targetRotation_16)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_targetRotation_16() const { return ___targetRotation_16; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_targetRotation_16() { return &___targetRotation_16; }
	inline void set_targetRotation_16(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___targetRotation_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROXIMITYACTIVATE_T403C541F04433D4393C70B64CF490E9E6C5189F4_H
#ifndef ROTATOR_T8E318BD1376632CD4EE2113010F16B4BC26E15A8_H
#define ROTATOR_T8E318BD1376632CD4EE2113010F16B4BC26E15A8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rotator
struct  Rotator_t8E318BD1376632CD4EE2113010F16B4BC26E15A8  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATOR_T8E318BD1376632CD4EE2113010F16B4BC26E15A8_H
#ifndef SIMPLECHARACTERMOTOR_TEDEE0463060199C0D5ED227BF71228F737E86897_H
#define SIMPLECHARACTERMOTOR_TEDEE0463060199C0D5ED227BF71228F737E86897_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleCharacterMotor
struct  SimpleCharacterMotor_tEDEE0463060199C0D5ED227BF71228F737E86897  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.CursorLockMode SimpleCharacterMotor::cursorLockMode
	int32_t ___cursorLockMode_4;
	// System.Boolean SimpleCharacterMotor::cursorVisible
	bool ___cursorVisible_5;
	// System.Single SimpleCharacterMotor::walkSpeed
	float ___walkSpeed_6;
	// System.Single SimpleCharacterMotor::runSpeed
	float ___runSpeed_7;
	// System.Single SimpleCharacterMotor::gravity
	float ___gravity_8;
	// UnityEngine.Transform SimpleCharacterMotor::cameraPivot
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___cameraPivot_9;
	// System.Single SimpleCharacterMotor::lookSpeed
	float ___lookSpeed_10;
	// System.Boolean SimpleCharacterMotor::invertY
	bool ___invertY_11;
	// System.Single SimpleCharacterMotor::movementAcceleration
	float ___movementAcceleration_12;
	// UnityEngine.CharacterController SimpleCharacterMotor::controller
	CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E * ___controller_13;
	// UnityEngine.Vector3 SimpleCharacterMotor::movement
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___movement_14;
	// UnityEngine.Vector3 SimpleCharacterMotor::finalMovement
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___finalMovement_15;
	// System.Single SimpleCharacterMotor::speed
	float ___speed_16;
	// UnityEngine.Quaternion SimpleCharacterMotor::targetRotation
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___targetRotation_17;
	// UnityEngine.Quaternion SimpleCharacterMotor::targetPivotRotation
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___targetPivotRotation_18;

public:
	inline static int32_t get_offset_of_cursorLockMode_4() { return static_cast<int32_t>(offsetof(SimpleCharacterMotor_tEDEE0463060199C0D5ED227BF71228F737E86897, ___cursorLockMode_4)); }
	inline int32_t get_cursorLockMode_4() const { return ___cursorLockMode_4; }
	inline int32_t* get_address_of_cursorLockMode_4() { return &___cursorLockMode_4; }
	inline void set_cursorLockMode_4(int32_t value)
	{
		___cursorLockMode_4 = value;
	}

	inline static int32_t get_offset_of_cursorVisible_5() { return static_cast<int32_t>(offsetof(SimpleCharacterMotor_tEDEE0463060199C0D5ED227BF71228F737E86897, ___cursorVisible_5)); }
	inline bool get_cursorVisible_5() const { return ___cursorVisible_5; }
	inline bool* get_address_of_cursorVisible_5() { return &___cursorVisible_5; }
	inline void set_cursorVisible_5(bool value)
	{
		___cursorVisible_5 = value;
	}

	inline static int32_t get_offset_of_walkSpeed_6() { return static_cast<int32_t>(offsetof(SimpleCharacterMotor_tEDEE0463060199C0D5ED227BF71228F737E86897, ___walkSpeed_6)); }
	inline float get_walkSpeed_6() const { return ___walkSpeed_6; }
	inline float* get_address_of_walkSpeed_6() { return &___walkSpeed_6; }
	inline void set_walkSpeed_6(float value)
	{
		___walkSpeed_6 = value;
	}

	inline static int32_t get_offset_of_runSpeed_7() { return static_cast<int32_t>(offsetof(SimpleCharacterMotor_tEDEE0463060199C0D5ED227BF71228F737E86897, ___runSpeed_7)); }
	inline float get_runSpeed_7() const { return ___runSpeed_7; }
	inline float* get_address_of_runSpeed_7() { return &___runSpeed_7; }
	inline void set_runSpeed_7(float value)
	{
		___runSpeed_7 = value;
	}

	inline static int32_t get_offset_of_gravity_8() { return static_cast<int32_t>(offsetof(SimpleCharacterMotor_tEDEE0463060199C0D5ED227BF71228F737E86897, ___gravity_8)); }
	inline float get_gravity_8() const { return ___gravity_8; }
	inline float* get_address_of_gravity_8() { return &___gravity_8; }
	inline void set_gravity_8(float value)
	{
		___gravity_8 = value;
	}

	inline static int32_t get_offset_of_cameraPivot_9() { return static_cast<int32_t>(offsetof(SimpleCharacterMotor_tEDEE0463060199C0D5ED227BF71228F737E86897, ___cameraPivot_9)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_cameraPivot_9() const { return ___cameraPivot_9; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_cameraPivot_9() { return &___cameraPivot_9; }
	inline void set_cameraPivot_9(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___cameraPivot_9 = value;
		Il2CppCodeGenWriteBarrier((&___cameraPivot_9), value);
	}

	inline static int32_t get_offset_of_lookSpeed_10() { return static_cast<int32_t>(offsetof(SimpleCharacterMotor_tEDEE0463060199C0D5ED227BF71228F737E86897, ___lookSpeed_10)); }
	inline float get_lookSpeed_10() const { return ___lookSpeed_10; }
	inline float* get_address_of_lookSpeed_10() { return &___lookSpeed_10; }
	inline void set_lookSpeed_10(float value)
	{
		___lookSpeed_10 = value;
	}

	inline static int32_t get_offset_of_invertY_11() { return static_cast<int32_t>(offsetof(SimpleCharacterMotor_tEDEE0463060199C0D5ED227BF71228F737E86897, ___invertY_11)); }
	inline bool get_invertY_11() const { return ___invertY_11; }
	inline bool* get_address_of_invertY_11() { return &___invertY_11; }
	inline void set_invertY_11(bool value)
	{
		___invertY_11 = value;
	}

	inline static int32_t get_offset_of_movementAcceleration_12() { return static_cast<int32_t>(offsetof(SimpleCharacterMotor_tEDEE0463060199C0D5ED227BF71228F737E86897, ___movementAcceleration_12)); }
	inline float get_movementAcceleration_12() const { return ___movementAcceleration_12; }
	inline float* get_address_of_movementAcceleration_12() { return &___movementAcceleration_12; }
	inline void set_movementAcceleration_12(float value)
	{
		___movementAcceleration_12 = value;
	}

	inline static int32_t get_offset_of_controller_13() { return static_cast<int32_t>(offsetof(SimpleCharacterMotor_tEDEE0463060199C0D5ED227BF71228F737E86897, ___controller_13)); }
	inline CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E * get_controller_13() const { return ___controller_13; }
	inline CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E ** get_address_of_controller_13() { return &___controller_13; }
	inline void set_controller_13(CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E * value)
	{
		___controller_13 = value;
		Il2CppCodeGenWriteBarrier((&___controller_13), value);
	}

	inline static int32_t get_offset_of_movement_14() { return static_cast<int32_t>(offsetof(SimpleCharacterMotor_tEDEE0463060199C0D5ED227BF71228F737E86897, ___movement_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_movement_14() const { return ___movement_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_movement_14() { return &___movement_14; }
	inline void set_movement_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___movement_14 = value;
	}

	inline static int32_t get_offset_of_finalMovement_15() { return static_cast<int32_t>(offsetof(SimpleCharacterMotor_tEDEE0463060199C0D5ED227BF71228F737E86897, ___finalMovement_15)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_finalMovement_15() const { return ___finalMovement_15; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_finalMovement_15() { return &___finalMovement_15; }
	inline void set_finalMovement_15(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___finalMovement_15 = value;
	}

	inline static int32_t get_offset_of_speed_16() { return static_cast<int32_t>(offsetof(SimpleCharacterMotor_tEDEE0463060199C0D5ED227BF71228F737E86897, ___speed_16)); }
	inline float get_speed_16() const { return ___speed_16; }
	inline float* get_address_of_speed_16() { return &___speed_16; }
	inline void set_speed_16(float value)
	{
		___speed_16 = value;
	}

	inline static int32_t get_offset_of_targetRotation_17() { return static_cast<int32_t>(offsetof(SimpleCharacterMotor_tEDEE0463060199C0D5ED227BF71228F737E86897, ___targetRotation_17)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_targetRotation_17() const { return ___targetRotation_17; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_targetRotation_17() { return &___targetRotation_17; }
	inline void set_targetRotation_17(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___targetRotation_17 = value;
	}

	inline static int32_t get_offset_of_targetPivotRotation_18() { return static_cast<int32_t>(offsetof(SimpleCharacterMotor_tEDEE0463060199C0D5ED227BF71228F737E86897, ___targetPivotRotation_18)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_targetPivotRotation_18() const { return ___targetPivotRotation_18; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_targetPivotRotation_18() { return &___targetPivotRotation_18; }
	inline void set_targetPivotRotation_18(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___targetPivotRotation_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLECHARACTERMOTOR_TEDEE0463060199C0D5ED227BF71228F737E86897_H
#ifndef SPAWNEFFECT_TCD0A8BA5BBCD5562194C5B10E340FEFC6A2C663D_H
#define SPAWNEFFECT_TCD0A8BA5BBCD5562194C5B10E340FEFC6A2C663D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpawnEffect
struct  SpawnEffect_tCD0A8BA5BBCD5562194C5B10E340FEFC6A2C663D  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single SpawnEffect::spawnEffectTime
	float ___spawnEffectTime_4;
	// System.Single SpawnEffect::pause
	float ___pause_5;
	// UnityEngine.AnimationCurve SpawnEffect::fadeIn
	AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * ___fadeIn_6;
	// UnityEngine.ParticleSystem SpawnEffect::ps
	ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * ___ps_7;
	// System.Single SpawnEffect::timer
	float ___timer_8;
	// UnityEngine.Renderer SpawnEffect::_renderer
	Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * ____renderer_9;
	// System.Int32 SpawnEffect::shaderProperty
	int32_t ___shaderProperty_10;

public:
	inline static int32_t get_offset_of_spawnEffectTime_4() { return static_cast<int32_t>(offsetof(SpawnEffect_tCD0A8BA5BBCD5562194C5B10E340FEFC6A2C663D, ___spawnEffectTime_4)); }
	inline float get_spawnEffectTime_4() const { return ___spawnEffectTime_4; }
	inline float* get_address_of_spawnEffectTime_4() { return &___spawnEffectTime_4; }
	inline void set_spawnEffectTime_4(float value)
	{
		___spawnEffectTime_4 = value;
	}

	inline static int32_t get_offset_of_pause_5() { return static_cast<int32_t>(offsetof(SpawnEffect_tCD0A8BA5BBCD5562194C5B10E340FEFC6A2C663D, ___pause_5)); }
	inline float get_pause_5() const { return ___pause_5; }
	inline float* get_address_of_pause_5() { return &___pause_5; }
	inline void set_pause_5(float value)
	{
		___pause_5 = value;
	}

	inline static int32_t get_offset_of_fadeIn_6() { return static_cast<int32_t>(offsetof(SpawnEffect_tCD0A8BA5BBCD5562194C5B10E340FEFC6A2C663D, ___fadeIn_6)); }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * get_fadeIn_6() const { return ___fadeIn_6; }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C ** get_address_of_fadeIn_6() { return &___fadeIn_6; }
	inline void set_fadeIn_6(AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * value)
	{
		___fadeIn_6 = value;
		Il2CppCodeGenWriteBarrier((&___fadeIn_6), value);
	}

	inline static int32_t get_offset_of_ps_7() { return static_cast<int32_t>(offsetof(SpawnEffect_tCD0A8BA5BBCD5562194C5B10E340FEFC6A2C663D, ___ps_7)); }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * get_ps_7() const { return ___ps_7; }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D ** get_address_of_ps_7() { return &___ps_7; }
	inline void set_ps_7(ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * value)
	{
		___ps_7 = value;
		Il2CppCodeGenWriteBarrier((&___ps_7), value);
	}

	inline static int32_t get_offset_of_timer_8() { return static_cast<int32_t>(offsetof(SpawnEffect_tCD0A8BA5BBCD5562194C5B10E340FEFC6A2C663D, ___timer_8)); }
	inline float get_timer_8() const { return ___timer_8; }
	inline float* get_address_of_timer_8() { return &___timer_8; }
	inline void set_timer_8(float value)
	{
		___timer_8 = value;
	}

	inline static int32_t get_offset_of__renderer_9() { return static_cast<int32_t>(offsetof(SpawnEffect_tCD0A8BA5BBCD5562194C5B10E340FEFC6A2C663D, ____renderer_9)); }
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * get__renderer_9() const { return ____renderer_9; }
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 ** get_address_of__renderer_9() { return &____renderer_9; }
	inline void set__renderer_9(Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * value)
	{
		____renderer_9 = value;
		Il2CppCodeGenWriteBarrier((&____renderer_9), value);
	}

	inline static int32_t get_offset_of_shaderProperty_10() { return static_cast<int32_t>(offsetof(SpawnEffect_tCD0A8BA5BBCD5562194C5B10E340FEFC6A2C663D, ___shaderProperty_10)); }
	inline int32_t get_shaderProperty_10() const { return ___shaderProperty_10; }
	inline int32_t* get_address_of_shaderProperty_10() { return &___shaderProperty_10; }
	inline void set_shaderProperty_10(int32_t value)
	{
		___shaderProperty_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPAWNEFFECT_TCD0A8BA5BBCD5562194C5B10E340FEFC6A2C663D_H
#ifndef SIMPLECAMERACONTROLLER_T362F9BE95366F2B6BF1D5369EA29E331DDFDCC42_H
#define SIMPLECAMERACONTROLLER_T362F9BE95366F2B6BF1D5369EA29E331DDFDCC42_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityTemplateProjects.SimpleCameraController
struct  SimpleCameraController_t362F9BE95366F2B6BF1D5369EA29E331DDFDCC42  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityTemplateProjects.SimpleCameraController/CameraState UnityTemplateProjects.SimpleCameraController::m_TargetCameraState
	CameraState_t67204999380EA316F8E1B6C902D4330646DE6AE7 * ___m_TargetCameraState_4;
	// UnityTemplateProjects.SimpleCameraController/CameraState UnityTemplateProjects.SimpleCameraController::m_InterpolatingCameraState
	CameraState_t67204999380EA316F8E1B6C902D4330646DE6AE7 * ___m_InterpolatingCameraState_5;
	// System.Single UnityTemplateProjects.SimpleCameraController::boost
	float ___boost_6;
	// System.Single UnityTemplateProjects.SimpleCameraController::positionLerpTime
	float ___positionLerpTime_7;
	// UnityEngine.AnimationCurve UnityTemplateProjects.SimpleCameraController::mouseSensitivityCurve
	AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * ___mouseSensitivityCurve_8;
	// System.Single UnityTemplateProjects.SimpleCameraController::rotationLerpTime
	float ___rotationLerpTime_9;
	// System.Boolean UnityTemplateProjects.SimpleCameraController::invertY
	bool ___invertY_10;

public:
	inline static int32_t get_offset_of_m_TargetCameraState_4() { return static_cast<int32_t>(offsetof(SimpleCameraController_t362F9BE95366F2B6BF1D5369EA29E331DDFDCC42, ___m_TargetCameraState_4)); }
	inline CameraState_t67204999380EA316F8E1B6C902D4330646DE6AE7 * get_m_TargetCameraState_4() const { return ___m_TargetCameraState_4; }
	inline CameraState_t67204999380EA316F8E1B6C902D4330646DE6AE7 ** get_address_of_m_TargetCameraState_4() { return &___m_TargetCameraState_4; }
	inline void set_m_TargetCameraState_4(CameraState_t67204999380EA316F8E1B6C902D4330646DE6AE7 * value)
	{
		___m_TargetCameraState_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_TargetCameraState_4), value);
	}

	inline static int32_t get_offset_of_m_InterpolatingCameraState_5() { return static_cast<int32_t>(offsetof(SimpleCameraController_t362F9BE95366F2B6BF1D5369EA29E331DDFDCC42, ___m_InterpolatingCameraState_5)); }
	inline CameraState_t67204999380EA316F8E1B6C902D4330646DE6AE7 * get_m_InterpolatingCameraState_5() const { return ___m_InterpolatingCameraState_5; }
	inline CameraState_t67204999380EA316F8E1B6C902D4330646DE6AE7 ** get_address_of_m_InterpolatingCameraState_5() { return &___m_InterpolatingCameraState_5; }
	inline void set_m_InterpolatingCameraState_5(CameraState_t67204999380EA316F8E1B6C902D4330646DE6AE7 * value)
	{
		___m_InterpolatingCameraState_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_InterpolatingCameraState_5), value);
	}

	inline static int32_t get_offset_of_boost_6() { return static_cast<int32_t>(offsetof(SimpleCameraController_t362F9BE95366F2B6BF1D5369EA29E331DDFDCC42, ___boost_6)); }
	inline float get_boost_6() const { return ___boost_6; }
	inline float* get_address_of_boost_6() { return &___boost_6; }
	inline void set_boost_6(float value)
	{
		___boost_6 = value;
	}

	inline static int32_t get_offset_of_positionLerpTime_7() { return static_cast<int32_t>(offsetof(SimpleCameraController_t362F9BE95366F2B6BF1D5369EA29E331DDFDCC42, ___positionLerpTime_7)); }
	inline float get_positionLerpTime_7() const { return ___positionLerpTime_7; }
	inline float* get_address_of_positionLerpTime_7() { return &___positionLerpTime_7; }
	inline void set_positionLerpTime_7(float value)
	{
		___positionLerpTime_7 = value;
	}

	inline static int32_t get_offset_of_mouseSensitivityCurve_8() { return static_cast<int32_t>(offsetof(SimpleCameraController_t362F9BE95366F2B6BF1D5369EA29E331DDFDCC42, ___mouseSensitivityCurve_8)); }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * get_mouseSensitivityCurve_8() const { return ___mouseSensitivityCurve_8; }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C ** get_address_of_mouseSensitivityCurve_8() { return &___mouseSensitivityCurve_8; }
	inline void set_mouseSensitivityCurve_8(AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * value)
	{
		___mouseSensitivityCurve_8 = value;
		Il2CppCodeGenWriteBarrier((&___mouseSensitivityCurve_8), value);
	}

	inline static int32_t get_offset_of_rotationLerpTime_9() { return static_cast<int32_t>(offsetof(SimpleCameraController_t362F9BE95366F2B6BF1D5369EA29E331DDFDCC42, ___rotationLerpTime_9)); }
	inline float get_rotationLerpTime_9() const { return ___rotationLerpTime_9; }
	inline float* get_address_of_rotationLerpTime_9() { return &___rotationLerpTime_9; }
	inline void set_rotationLerpTime_9(float value)
	{
		___rotationLerpTime_9 = value;
	}

	inline static int32_t get_offset_of_invertY_10() { return static_cast<int32_t>(offsetof(SimpleCameraController_t362F9BE95366F2B6BF1D5369EA29E331DDFDCC42, ___invertY_10)); }
	inline bool get_invertY_10() const { return ___invertY_10; }
	inline bool* get_address_of_invertY_10() { return &___invertY_10; }
	inline void set_invertY_10(bool value)
	{
		___invertY_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLECAMERACONTROLLER_T362F9BE95366F2B6BF1D5369EA29E331DDFDCC42_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2200 = { sizeof (TMP_BasicXmlTagStack_tC5F410B9A4A8A8DE6351FA5F0FBF92F8E2CC8BF8)+ sizeof (RuntimeObject), sizeof(TMP_BasicXmlTagStack_tC5F410B9A4A8A8DE6351FA5F0FBF92F8E2CC8BF8 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2200[10] = 
{
	TMP_BasicXmlTagStack_tC5F410B9A4A8A8DE6351FA5F0FBF92F8E2CC8BF8::get_offset_of_bold_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_BasicXmlTagStack_tC5F410B9A4A8A8DE6351FA5F0FBF92F8E2CC8BF8::get_offset_of_italic_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_BasicXmlTagStack_tC5F410B9A4A8A8DE6351FA5F0FBF92F8E2CC8BF8::get_offset_of_underline_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_BasicXmlTagStack_tC5F410B9A4A8A8DE6351FA5F0FBF92F8E2CC8BF8::get_offset_of_strikethrough_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_BasicXmlTagStack_tC5F410B9A4A8A8DE6351FA5F0FBF92F8E2CC8BF8::get_offset_of_highlight_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_BasicXmlTagStack_tC5F410B9A4A8A8DE6351FA5F0FBF92F8E2CC8BF8::get_offset_of_superscript_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_BasicXmlTagStack_tC5F410B9A4A8A8DE6351FA5F0FBF92F8E2CC8BF8::get_offset_of_subscript_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_BasicXmlTagStack_tC5F410B9A4A8A8DE6351FA5F0FBF92F8E2CC8BF8::get_offset_of_uppercase_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_BasicXmlTagStack_tC5F410B9A4A8A8DE6351FA5F0FBF92F8E2CC8BF8::get_offset_of_lowercase_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_BasicXmlTagStack_tC5F410B9A4A8A8DE6351FA5F0FBF92F8E2CC8BF8::get_offset_of_smallcaps_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2201 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2201[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2202 = { sizeof (Compute_DistanceTransform_EventTypes_t7D7D951983E29A63EA0B4AE2EBC021B801291E50)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2202[3] = 
{
	Compute_DistanceTransform_EventTypes_t7D7D951983E29A63EA0B4AE2EBC021B801291E50::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2203 = { sizeof (TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D), -1, sizeof(TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2203[13] = 
{
	TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields::get_offset_of_COMPUTE_DT_EVENT_0(),
	TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields::get_offset_of_MATERIAL_PROPERTY_EVENT_1(),
	TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields::get_offset_of_FONT_PROPERTY_EVENT_2(),
	TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields::get_offset_of_SPRITE_ASSET_PROPERTY_EVENT_3(),
	TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields::get_offset_of_TEXTMESHPRO_PROPERTY_EVENT_4(),
	TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields::get_offset_of_DRAG_AND_DROP_MATERIAL_EVENT_5(),
	TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields::get_offset_of_TEXT_STYLE_PROPERTY_EVENT_6(),
	TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields::get_offset_of_COLOR_GRADIENT_PROPERTY_EVENT_7(),
	TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields::get_offset_of_TMP_SETTINGS_PROPERTY_EVENT_8(),
	TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields::get_offset_of_RESOURCE_LOAD_EVENT_9(),
	TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields::get_offset_of_TEXTMESHPRO_UGUI_PROPERTY_EVENT_10(),
	TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields::get_offset_of_OnPreRenderObject_Event_11(),
	TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields::get_offset_of_TEXT_CHANGED_EVENT_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2204 = { sizeof (Compute_DT_EventArgs_t17A56E99E621F8C211A13BE81BEAE18806DA7B11), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2204[3] = 
{
	Compute_DT_EventArgs_t17A56E99E621F8C211A13BE81BEAE18806DA7B11::get_offset_of_EventType_0(),
	Compute_DT_EventArgs_t17A56E99E621F8C211A13BE81BEAE18806DA7B11::get_offset_of_ProgressPercentage_1(),
	Compute_DT_EventArgs_t17A56E99E621F8C211A13BE81BEAE18806DA7B11::get_offset_of_Colors_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2205 = { sizeof (TMPro_ExtensionMethods_t2A24D41EAD2F3568C5617941DE7558790B9B1835), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2206 = { sizeof (TMP_Math_t074FB253662A213E8905642B8B29473EC794FE44), -1, sizeof(TMP_Math_t074FB253662A213E8905642B8B29473EC794FE44_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2206[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	TMP_Math_t074FB253662A213E8905642B8B29473EC794FE44_StaticFields::get_offset_of_MAX_16BIT_6(),
	TMP_Math_t074FB253662A213E8905642B8B29473EC794FE44_StaticFields::get_offset_of_MIN_16BIT_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2207 = { sizeof (FaceInfo_t43812B1A171B1AE8E3591EADED098DE81264F6A5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2207[21] = 
{
	FaceInfo_t43812B1A171B1AE8E3591EADED098DE81264F6A5::get_offset_of_Name_0(),
	FaceInfo_t43812B1A171B1AE8E3591EADED098DE81264F6A5::get_offset_of_PointSize_1(),
	FaceInfo_t43812B1A171B1AE8E3591EADED098DE81264F6A5::get_offset_of_Scale_2(),
	FaceInfo_t43812B1A171B1AE8E3591EADED098DE81264F6A5::get_offset_of_CharacterCount_3(),
	FaceInfo_t43812B1A171B1AE8E3591EADED098DE81264F6A5::get_offset_of_LineHeight_4(),
	FaceInfo_t43812B1A171B1AE8E3591EADED098DE81264F6A5::get_offset_of_Baseline_5(),
	FaceInfo_t43812B1A171B1AE8E3591EADED098DE81264F6A5::get_offset_of_Ascender_6(),
	FaceInfo_t43812B1A171B1AE8E3591EADED098DE81264F6A5::get_offset_of_CapHeight_7(),
	FaceInfo_t43812B1A171B1AE8E3591EADED098DE81264F6A5::get_offset_of_Descender_8(),
	FaceInfo_t43812B1A171B1AE8E3591EADED098DE81264F6A5::get_offset_of_CenterLine_9(),
	FaceInfo_t43812B1A171B1AE8E3591EADED098DE81264F6A5::get_offset_of_SuperscriptOffset_10(),
	FaceInfo_t43812B1A171B1AE8E3591EADED098DE81264F6A5::get_offset_of_SubscriptOffset_11(),
	FaceInfo_t43812B1A171B1AE8E3591EADED098DE81264F6A5::get_offset_of_SubSize_12(),
	FaceInfo_t43812B1A171B1AE8E3591EADED098DE81264F6A5::get_offset_of_Underline_13(),
	FaceInfo_t43812B1A171B1AE8E3591EADED098DE81264F6A5::get_offset_of_UnderlineThickness_14(),
	FaceInfo_t43812B1A171B1AE8E3591EADED098DE81264F6A5::get_offset_of_strikethrough_15(),
	FaceInfo_t43812B1A171B1AE8E3591EADED098DE81264F6A5::get_offset_of_strikethroughThickness_16(),
	FaceInfo_t43812B1A171B1AE8E3591EADED098DE81264F6A5::get_offset_of_TabWidth_17(),
	FaceInfo_t43812B1A171B1AE8E3591EADED098DE81264F6A5::get_offset_of_Padding_18(),
	FaceInfo_t43812B1A171B1AE8E3591EADED098DE81264F6A5::get_offset_of_AtlasWidth_19(),
	FaceInfo_t43812B1A171B1AE8E3591EADED098DE81264F6A5::get_offset_of_AtlasHeight_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2208 = { sizeof (TMP_Glyph_tCAA5E0C262A3DAF50537C03D9EA90B51B05BA62C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2209 = { sizeof (FontAssetCreationSettings_tC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4)+ sizeof (RuntimeObject), sizeof(FontAssetCreationSettings_tC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2209[16] = 
{
	FontAssetCreationSettings_tC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4::get_offset_of_sourceFontFileName_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontAssetCreationSettings_tC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4::get_offset_of_sourceFontFileGUID_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontAssetCreationSettings_tC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4::get_offset_of_pointSizeSamplingMode_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontAssetCreationSettings_tC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4::get_offset_of_pointSize_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontAssetCreationSettings_tC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4::get_offset_of_padding_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontAssetCreationSettings_tC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4::get_offset_of_packingMode_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontAssetCreationSettings_tC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4::get_offset_of_atlasWidth_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontAssetCreationSettings_tC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4::get_offset_of_atlasHeight_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontAssetCreationSettings_tC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4::get_offset_of_characterSetSelectionMode_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontAssetCreationSettings_tC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4::get_offset_of_characterSequence_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontAssetCreationSettings_tC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4::get_offset_of_referencedFontAssetGUID_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontAssetCreationSettings_tC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4::get_offset_of_referencedTextAssetGUID_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontAssetCreationSettings_tC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4::get_offset_of_fontStyle_12() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontAssetCreationSettings_tC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4::get_offset_of_fontStyleModifier_13() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontAssetCreationSettings_tC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4::get_offset_of_renderMode_14() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontAssetCreationSettings_tC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4::get_offset_of_includeFontFeatures_15() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2210 = { sizeof (KerningPairKey_tE33705E4E3EF2AA3DB9BCAA0AD581A46A70135D3)+ sizeof (RuntimeObject), sizeof(KerningPairKey_tE33705E4E3EF2AA3DB9BCAA0AD581A46A70135D3 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2210[3] = 
{
	KerningPairKey_tE33705E4E3EF2AA3DB9BCAA0AD581A46A70135D3::get_offset_of_ascii_Left_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	KerningPairKey_tE33705E4E3EF2AA3DB9BCAA0AD581A46A70135D3::get_offset_of_ascii_Right_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	KerningPairKey_tE33705E4E3EF2AA3DB9BCAA0AD581A46A70135D3::get_offset_of_key_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2211 = { sizeof (GlyphValueRecord_t978E7932FA4A5D424E6CFBB65E98B16D02BE18DD)+ sizeof (RuntimeObject), sizeof(GlyphValueRecord_t978E7932FA4A5D424E6CFBB65E98B16D02BE18DD ), 0, 0 };
extern const int32_t g_FieldOffsetTable2211[4] = 
{
	GlyphValueRecord_t978E7932FA4A5D424E6CFBB65E98B16D02BE18DD::get_offset_of_xPlacement_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GlyphValueRecord_t978E7932FA4A5D424E6CFBB65E98B16D02BE18DD::get_offset_of_yPlacement_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GlyphValueRecord_t978E7932FA4A5D424E6CFBB65E98B16D02BE18DD::get_offset_of_xAdvance_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GlyphValueRecord_t978E7932FA4A5D424E6CFBB65E98B16D02BE18DD::get_offset_of_yAdvance_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2212 = { sizeof (KerningPair_t4FCF2540D9DBE27046ED4A8643B7AA644ED0DAAD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2212[5] = 
{
	KerningPair_t4FCF2540D9DBE27046ED4A8643B7AA644ED0DAAD::get_offset_of_m_FirstGlyph_0(),
	KerningPair_t4FCF2540D9DBE27046ED4A8643B7AA644ED0DAAD::get_offset_of_m_FirstGlyphAdjustments_1(),
	KerningPair_t4FCF2540D9DBE27046ED4A8643B7AA644ED0DAAD::get_offset_of_m_SecondGlyph_2(),
	KerningPair_t4FCF2540D9DBE27046ED4A8643B7AA644ED0DAAD::get_offset_of_m_SecondGlyphAdjustments_3(),
	KerningPair_t4FCF2540D9DBE27046ED4A8643B7AA644ED0DAAD::get_offset_of_xOffset_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2213 = { sizeof (KerningTable_tAF8D2AABDC878598EFE90D838BAAD285FA8CE05F), -1, sizeof(KerningTable_tAF8D2AABDC878598EFE90D838BAAD285FA8CE05F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2213[3] = 
{
	KerningTable_tAF8D2AABDC878598EFE90D838BAAD285FA8CE05F::get_offset_of_kerningPairs_0(),
	KerningTable_tAF8D2AABDC878598EFE90D838BAAD285FA8CE05F_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_1(),
	KerningTable_tAF8D2AABDC878598EFE90D838BAAD285FA8CE05F_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2214 = { sizeof (U3CAddKerningPairU3Ec__AnonStorey0_t7580E224467E79EE3F9EF6CCD33D8198582777F8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2214[2] = 
{
	U3CAddKerningPairU3Ec__AnonStorey0_t7580E224467E79EE3F9EF6CCD33D8198582777F8::get_offset_of_first_0(),
	U3CAddKerningPairU3Ec__AnonStorey0_t7580E224467E79EE3F9EF6CCD33D8198582777F8::get_offset_of_second_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2215 = { sizeof (U3CAddGlyphPairAdjustmentRecordU3Ec__AnonStorey1_t343265D67166E73A018132EE036FCAAFDD7D56B4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2215[2] = 
{
	U3CAddGlyphPairAdjustmentRecordU3Ec__AnonStorey1_t343265D67166E73A018132EE036FCAAFDD7D56B4::get_offset_of_first_0(),
	U3CAddGlyphPairAdjustmentRecordU3Ec__AnonStorey1_t343265D67166E73A018132EE036FCAAFDD7D56B4::get_offset_of_second_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2216 = { sizeof (U3CRemoveKerningPairU3Ec__AnonStorey2_t26315506D3EDB64F137B8DA549CF82E5B01FABD8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2216[2] = 
{
	U3CRemoveKerningPairU3Ec__AnonStorey2_t26315506D3EDB64F137B8DA549CF82E5B01FABD8::get_offset_of_left_0(),
	U3CRemoveKerningPairU3Ec__AnonStorey2_t26315506D3EDB64F137B8DA549CF82E5B01FABD8::get_offset_of_right_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2217 = { sizeof (TMP_FontUtilities_tEAE7AD89B734C4BFCC635A65A05ED0BEB690935F), -1, sizeof(TMP_FontUtilities_tEAE7AD89B734C4BFCC635A65A05ED0BEB690935F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2217[1] = 
{
	TMP_FontUtilities_tEAE7AD89B734C4BFCC635A65A05ED0BEB690935F_StaticFields::get_offset_of_k_searchedFontAssets_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2218 = { sizeof (TMP_VertexDataUpdateFlags_tBEFA6E84F629CD5F4B1B040D55F7AB11B1AD6142)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2218[8] = 
{
	TMP_VertexDataUpdateFlags_tBEFA6E84F629CD5F4B1B040D55F7AB11B1AD6142::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2219 = { sizeof (TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2219[35] = 
{
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_character_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_index_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_elementType_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_textElement_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_fontAsset_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_spriteAsset_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_spriteIndex_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_material_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_materialReferenceIndex_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_isUsingAlternateTypeface_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_pointSize_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_lineNumber_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_pageNumber_12() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_vertexIndex_13() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_vertex_TL_14() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_vertex_BL_15() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_vertex_TR_16() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_vertex_BR_17() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_topLeft_18() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_bottomLeft_19() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_topRight_20() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_bottomRight_21() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_origin_22() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_ascender_23() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_baseLine_24() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_descender_25() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_xAdvance_26() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_aspectRatio_27() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_scale_28() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_color_29() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_underlineColor_30() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_strikethroughColor_31() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_highlightColor_32() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_style_33() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_isVisible_34() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2220 = { sizeof (TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0)+ sizeof (RuntimeObject), sizeof(TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2220[5] = 
{
	TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0::get_offset_of_position_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0::get_offset_of_uv_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0::get_offset_of_uv2_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0::get_offset_of_uv4_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0::get_offset_of_color_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2221 = { sizeof (VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A)+ sizeof (RuntimeObject), sizeof(VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A ), 0, 0 };
extern const int32_t g_FieldOffsetTable2221[4] = 
{
	VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A::get_offset_of_topLeft_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A::get_offset_of_topRight_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A::get_offset_of_bottomLeft_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A::get_offset_of_bottomRight_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2222 = { sizeof (TMP_PageInfo_t5D305B11116379997CA9649E8D87B3D7162ABB24)+ sizeof (RuntimeObject), sizeof(TMP_PageInfo_t5D305B11116379997CA9649E8D87B3D7162ABB24 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2222[5] = 
{
	TMP_PageInfo_t5D305B11116379997CA9649E8D87B3D7162ABB24::get_offset_of_firstCharacterIndex_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_PageInfo_t5D305B11116379997CA9649E8D87B3D7162ABB24::get_offset_of_lastCharacterIndex_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_PageInfo_t5D305B11116379997CA9649E8D87B3D7162ABB24::get_offset_of_ascender_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_PageInfo_t5D305B11116379997CA9649E8D87B3D7162ABB24::get_offset_of_baseLine_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_PageInfo_t5D305B11116379997CA9649E8D87B3D7162ABB24::get_offset_of_descender_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2223 = { sizeof (TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2223[7] = 
{
	TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468::get_offset_of_textComponent_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468::get_offset_of_hashCode_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468::get_offset_of_linkIdFirstCharacterIndex_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468::get_offset_of_linkIdLength_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468::get_offset_of_linkTextfirstCharacterIndex_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468::get_offset_of_linkTextLength_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468::get_offset_of_linkID_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2224 = { sizeof (TMP_WordInfo_t856E4994B49881E370B28E1D0C35EEDA56120D90)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2224[4] = 
{
	TMP_WordInfo_t856E4994B49881E370B28E1D0C35EEDA56120D90::get_offset_of_textComponent_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_WordInfo_t856E4994B49881E370B28E1D0C35EEDA56120D90::get_offset_of_firstCharacterIndex_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_WordInfo_t856E4994B49881E370B28E1D0C35EEDA56120D90::get_offset_of_lastCharacterIndex_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_WordInfo_t856E4994B49881E370B28E1D0C35EEDA56120D90::get_offset_of_characterCount_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2225 = { sizeof (TMP_SpriteInfo_t55432612FE0D00F32826D0F817E8462F66CBABBB)+ sizeof (RuntimeObject), sizeof(TMP_SpriteInfo_t55432612FE0D00F32826D0F817E8462F66CBABBB ), 0, 0 };
extern const int32_t g_FieldOffsetTable2225[3] = 
{
	TMP_SpriteInfo_t55432612FE0D00F32826D0F817E8462F66CBABBB::get_offset_of_spriteIndex_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_SpriteInfo_t55432612FE0D00F32826D0F817E8462F66CBABBB::get_offset_of_characterIndex_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_SpriteInfo_t55432612FE0D00F32826D0F817E8462F66CBABBB::get_offset_of_vertexIndex_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2226 = { sizeof (Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3)+ sizeof (RuntimeObject), sizeof(Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2226[2] = 
{
	Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3::get_offset_of_min_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3::get_offset_of_max_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2227 = { sizeof (Mesh_Extents_t4A76BB0AD89A24EA8AE86F4EC31081DB86F7E7E8)+ sizeof (RuntimeObject), sizeof(Mesh_Extents_t4A76BB0AD89A24EA8AE86F4EC31081DB86F7E7E8 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2227[2] = 
{
	Mesh_Extents_t4A76BB0AD89A24EA8AE86F4EC31081DB86F7E7E8::get_offset_of_min_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Mesh_Extents_t4A76BB0AD89A24EA8AE86F4EC31081DB86F7E7E8::get_offset_of_max_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2228 = { sizeof (WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2228[55] = 
{
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_previous_WordBreak_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_total_CharacterCount_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_visible_CharacterCount_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_visible_SpriteCount_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_visible_LinkCount_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_firstCharacterIndex_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_firstVisibleCharacterIndex_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_lastCharacterIndex_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_lastVisibleCharIndex_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_lineNumber_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_maxCapHeight_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_maxAscender_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_maxDescender_12() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_maxLineAscender_13() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_maxLineDescender_14() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_previousLineAscender_15() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_xAdvance_16() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_preferredWidth_17() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_preferredHeight_18() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_previousLineScale_19() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_wordCount_20() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_fontStyle_21() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_fontScale_22() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_fontScaleMultiplier_23() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_currentFontSize_24() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_baselineOffset_25() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_lineOffset_26() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_textInfo_27() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_lineInfo_28() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_vertexColor_29() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_underlineColor_30() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_strikethroughColor_31() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_highlightColor_32() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_basicStyleStack_33() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_colorStack_34() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_underlineColorStack_35() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_strikethroughColorStack_36() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_highlightColorStack_37() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_colorGradientStack_38() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_sizeStack_39() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_indentStack_40() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_fontWeightStack_41() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_styleStack_42() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_baselineStack_43() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_actionStack_44() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_materialReferenceStack_45() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_lineJustificationStack_46() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_spriteAnimationID_47() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_currentFontAsset_48() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_currentSpriteAsset_49() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_currentMaterial_50() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_currentMaterialIndex_51() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_meshExtents_52() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_tagNoParsing_53() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_isNonBreakingSpace_54() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2229 = { sizeof (TagAttribute_t76F6882B9F1A6E98098CFBB2D4B933BE36B543E5)+ sizeof (RuntimeObject), sizeof(TagAttribute_t76F6882B9F1A6E98098CFBB2D4B933BE36B543E5 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2229[3] = 
{
	TagAttribute_t76F6882B9F1A6E98098CFBB2D4B933BE36B543E5::get_offset_of_startIndex_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TagAttribute_t76F6882B9F1A6E98098CFBB2D4B933BE36B543E5::get_offset_of_length_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TagAttribute_t76F6882B9F1A6E98098CFBB2D4B933BE36B543E5::get_offset_of_hashCode_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2230 = { sizeof (XML_TagAttribute_tE129B1DB312EBE65B24529E9CD5BCBE1DB75BF8E)+ sizeof (RuntimeObject), sizeof(XML_TagAttribute_tE129B1DB312EBE65B24529E9CD5BCBE1DB75BF8E ), 0, 0 };
extern const int32_t g_FieldOffsetTable2230[5] = 
{
	XML_TagAttribute_tE129B1DB312EBE65B24529E9CD5BCBE1DB75BF8E::get_offset_of_nameHashCode_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XML_TagAttribute_tE129B1DB312EBE65B24529E9CD5BCBE1DB75BF8E::get_offset_of_valueType_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XML_TagAttribute_tE129B1DB312EBE65B24529E9CD5BCBE1DB75BF8E::get_offset_of_valueStartIndex_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XML_TagAttribute_tE129B1DB312EBE65B24529E9CD5BCBE1DB75BF8E::get_offset_of_valueLength_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XML_TagAttribute_tE129B1DB312EBE65B24529E9CD5BCBE1DB75BF8E::get_offset_of_valueHashCode_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2231 = { sizeof (ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8), -1, sizeof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2231[60] = 
{
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_MainTex_0(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_FaceTex_1(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_FaceColor_2(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_FaceDilate_3(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_Shininess_4(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_UnderlayColor_5(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_UnderlayOffsetX_6(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_UnderlayOffsetY_7(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_UnderlayDilate_8(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_UnderlaySoftness_9(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_WeightNormal_10(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_WeightBold_11(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_OutlineTex_12(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_OutlineWidth_13(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_OutlineSoftness_14(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_OutlineColor_15(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_Padding_16(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_GradientScale_17(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_ScaleX_18(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_ScaleY_19(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_PerspectiveFilter_20(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_TextureWidth_21(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_TextureHeight_22(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_BevelAmount_23(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_GlowColor_24(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_GlowOffset_25(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_GlowPower_26(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_GlowOuter_27(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_LightAngle_28(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_EnvMap_29(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_EnvMatrix_30(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_EnvMatrixRotation_31(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_MaskCoord_32(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_ClipRect_33(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_MaskSoftnessX_34(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_MaskSoftnessY_35(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_VertexOffsetX_36(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_VertexOffsetY_37(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_UseClipRect_38(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_StencilID_39(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_StencilOp_40(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_StencilComp_41(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_StencilReadMask_42(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_StencilWriteMask_43(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_ShaderFlags_44(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_ScaleRatio_A_45(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_ScaleRatio_B_46(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_ScaleRatio_C_47(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_Keyword_Bevel_48(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_Keyword_Glow_49(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_Keyword_Underlay_50(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_Keyword_Ratios_51(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_Keyword_MASK_SOFT_52(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_Keyword_MASK_HARD_53(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_Keyword_MASK_TEX_54(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_Keyword_Outline_55(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ShaderTag_ZTestMode_56(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ShaderTag_CullMode_57(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_m_clamp_58(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_isInitialized_59(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2232 = { sizeof (U3CPrivateImplementationDetailsU3E_t2BDD5686E87BA22996C7749A9C7A7F86EB589380), -1, sizeof(U3CPrivateImplementationDetailsU3E_t2BDD5686E87BA22996C7749A9C7A7F86EB589380_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2232[2] = 
{
	U3CPrivateImplementationDetailsU3E_t2BDD5686E87BA22996C7749A9C7A7F86EB589380_StaticFields::get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(),
	U3CPrivateImplementationDetailsU3E_t2BDD5686E87BA22996C7749A9C7A7F86EB589380_StaticFields::get_offset_of_U24fieldU2D9E6378168821DBABB7EE3D0154346480FAC8AEF1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2233 = { sizeof (U24ArrayTypeU3D12_tBFF9DDDDE6A030848A3B927BF2701B3B97F56317)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D12_tBFF9DDDDE6A030848A3B927BF2701B3B97F56317 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2234 = { sizeof (U24ArrayTypeU3D40_tD3EB1EDB8340E3E2EFCDE746D3F6D7664617AA91)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D40_tD3EB1EDB8340E3E2EFCDE746D3F6D7664617AA91 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2235 = { sizeof (U3CModuleU3E_t6CDDDF959E7E18A6744E43B613F41CDAC780256A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2236 = { sizeof (CameraController_tC58777A506A1D4F1F1794E5D666E619C1DA073BB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2236[2] = 
{
	CameraController_tC58777A506A1D4F1F1794E5D666E619C1DA073BB::get_offset_of_player_4(),
	CameraController_tC58777A506A1D4F1F1794E5D666E619C1DA073BB::get_offset_of_offset_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2237 = { sizeof (PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2237[6] = 
{
	PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85::get_offset_of_speed_4(),
	PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85::get_offset_of_countText_5(),
	PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85::get_offset_of_rb_6(),
	PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85::get_offset_of_count_7(),
	PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85::get_offset_of_messages_8(),
	PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85::get_offset_of_BigExplosion_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2238 = { sizeof (Rotator_t8E318BD1376632CD4EE2113010F16B4BC26E15A8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2239 = { sizeof (SpawnEffect_tCD0A8BA5BBCD5562194C5B10E340FEFC6A2C663D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2239[7] = 
{
	SpawnEffect_tCD0A8BA5BBCD5562194C5B10E340FEFC6A2C663D::get_offset_of_spawnEffectTime_4(),
	SpawnEffect_tCD0A8BA5BBCD5562194C5B10E340FEFC6A2C663D::get_offset_of_pause_5(),
	SpawnEffect_tCD0A8BA5BBCD5562194C5B10E340FEFC6A2C663D::get_offset_of_fadeIn_6(),
	SpawnEffect_tCD0A8BA5BBCD5562194C5B10E340FEFC6A2C663D::get_offset_of_ps_7(),
	SpawnEffect_tCD0A8BA5BBCD5562194C5B10E340FEFC6A2C663D::get_offset_of_timer_8(),
	SpawnEffect_tCD0A8BA5BBCD5562194C5B10E340FEFC6A2C663D::get_offset_of__renderer_9(),
	SpawnEffect_tCD0A8BA5BBCD5562194C5B10E340FEFC6A2C663D::get_offset_of_shaderProperty_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2240 = { sizeof (RampAsset_t6D58ED782B43A6FC5555C2A22E5F767100D34C8E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2240[4] = 
{
	RampAsset_t6D58ED782B43A6FC5555C2A22E5F767100D34C8E::get_offset_of_gradient_4(),
	RampAsset_t6D58ED782B43A6FC5555C2A22E5F767100D34C8E::get_offset_of_size_5(),
	RampAsset_t6D58ED782B43A6FC5555C2A22E5F767100D34C8E::get_offset_of_up_6(),
	RampAsset_t6D58ED782B43A6FC5555C2A22E5F767100D34C8E::get_offset_of_overwriteExisting_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2241 = { sizeof (AdjustTimeScale_t727E1C2C5087BF85DD8A30E8BB94C8F86F220405), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2241[1] = 
{
	AdjustTimeScale_t727E1C2C5087BF85DD8A30E8BB94C8F86F220405::get_offset_of_textMesh_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2242 = { sizeof (ProximityActivate_t403C541F04433D4393C70B64CF490E9E6C5189F4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2242[13] = 
{
	ProximityActivate_t403C541F04433D4393C70B64CF490E9E6C5189F4::get_offset_of_distanceActivator_4(),
	ProximityActivate_t403C541F04433D4393C70B64CF490E9E6C5189F4::get_offset_of_lookAtActivator_5(),
	ProximityActivate_t403C541F04433D4393C70B64CF490E9E6C5189F4::get_offset_of_distance_6(),
	ProximityActivate_t403C541F04433D4393C70B64CF490E9E6C5189F4::get_offset_of_activator_7(),
	ProximityActivate_t403C541F04433D4393C70B64CF490E9E6C5189F4::get_offset_of_activeState_8(),
	ProximityActivate_t403C541F04433D4393C70B64CF490E9E6C5189F4::get_offset_of_target_9(),
	ProximityActivate_t403C541F04433D4393C70B64CF490E9E6C5189F4::get_offset_of_lookAtCamera_10(),
	ProximityActivate_t403C541F04433D4393C70B64CF490E9E6C5189F4::get_offset_of_enableInfoPanel_11(),
	ProximityActivate_t403C541F04433D4393C70B64CF490E9E6C5189F4::get_offset_of_infoIcon_12(),
	ProximityActivate_t403C541F04433D4393C70B64CF490E9E6C5189F4::get_offset_of_alpha_13(),
	ProximityActivate_t403C541F04433D4393C70B64CF490E9E6C5189F4::get_offset_of_infoPanel_14(),
	ProximityActivate_t403C541F04433D4393C70B64CF490E9E6C5189F4::get_offset_of_originRotation_15(),
	ProximityActivate_t403C541F04433D4393C70B64CF490E9E6C5189F4::get_offset_of_targetRotation_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2243 = { sizeof (SimpleCameraController_t362F9BE95366F2B6BF1D5369EA29E331DDFDCC42), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2243[7] = 
{
	SimpleCameraController_t362F9BE95366F2B6BF1D5369EA29E331DDFDCC42::get_offset_of_m_TargetCameraState_4(),
	SimpleCameraController_t362F9BE95366F2B6BF1D5369EA29E331DDFDCC42::get_offset_of_m_InterpolatingCameraState_5(),
	SimpleCameraController_t362F9BE95366F2B6BF1D5369EA29E331DDFDCC42::get_offset_of_boost_6(),
	SimpleCameraController_t362F9BE95366F2B6BF1D5369EA29E331DDFDCC42::get_offset_of_positionLerpTime_7(),
	SimpleCameraController_t362F9BE95366F2B6BF1D5369EA29E331DDFDCC42::get_offset_of_mouseSensitivityCurve_8(),
	SimpleCameraController_t362F9BE95366F2B6BF1D5369EA29E331DDFDCC42::get_offset_of_rotationLerpTime_9(),
	SimpleCameraController_t362F9BE95366F2B6BF1D5369EA29E331DDFDCC42::get_offset_of_invertY_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2244 = { sizeof (CameraState_t67204999380EA316F8E1B6C902D4330646DE6AE7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2244[6] = 
{
	CameraState_t67204999380EA316F8E1B6C902D4330646DE6AE7::get_offset_of_yaw_0(),
	CameraState_t67204999380EA316F8E1B6C902D4330646DE6AE7::get_offset_of_pitch_1(),
	CameraState_t67204999380EA316F8E1B6C902D4330646DE6AE7::get_offset_of_roll_2(),
	CameraState_t67204999380EA316F8E1B6C902D4330646DE6AE7::get_offset_of_x_3(),
	CameraState_t67204999380EA316F8E1B6C902D4330646DE6AE7::get_offset_of_y_4(),
	CameraState_t67204999380EA316F8E1B6C902D4330646DE6AE7::get_offset_of_z_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2245 = { sizeof (SimpleCharacterMotor_tEDEE0463060199C0D5ED227BF71228F737E86897), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2245[15] = 
{
	SimpleCharacterMotor_tEDEE0463060199C0D5ED227BF71228F737E86897::get_offset_of_cursorLockMode_4(),
	SimpleCharacterMotor_tEDEE0463060199C0D5ED227BF71228F737E86897::get_offset_of_cursorVisible_5(),
	SimpleCharacterMotor_tEDEE0463060199C0D5ED227BF71228F737E86897::get_offset_of_walkSpeed_6(),
	SimpleCharacterMotor_tEDEE0463060199C0D5ED227BF71228F737E86897::get_offset_of_runSpeed_7(),
	SimpleCharacterMotor_tEDEE0463060199C0D5ED227BF71228F737E86897::get_offset_of_gravity_8(),
	SimpleCharacterMotor_tEDEE0463060199C0D5ED227BF71228F737E86897::get_offset_of_cameraPivot_9(),
	SimpleCharacterMotor_tEDEE0463060199C0D5ED227BF71228F737E86897::get_offset_of_lookSpeed_10(),
	SimpleCharacterMotor_tEDEE0463060199C0D5ED227BF71228F737E86897::get_offset_of_invertY_11(),
	SimpleCharacterMotor_tEDEE0463060199C0D5ED227BF71228F737E86897::get_offset_of_movementAcceleration_12(),
	SimpleCharacterMotor_tEDEE0463060199C0D5ED227BF71228F737E86897::get_offset_of_controller_13(),
	SimpleCharacterMotor_tEDEE0463060199C0D5ED227BF71228F737E86897::get_offset_of_movement_14(),
	SimpleCharacterMotor_tEDEE0463060199C0D5ED227BF71228F737E86897::get_offset_of_finalMovement_15(),
	SimpleCharacterMotor_tEDEE0463060199C0D5ED227BF71228F737E86897::get_offset_of_speed_16(),
	SimpleCharacterMotor_tEDEE0463060199C0D5ED227BF71228F737E86897::get_offset_of_targetRotation_17(),
	SimpleCharacterMotor_tEDEE0463060199C0D5ED227BF71228F737E86897::get_offset_of_targetPivotRotation_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2246 = { sizeof (Readme_t3B6022BA9CBD2C56BD12C781DB20C5B34B6C4E46), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2246[4] = 
{
	Readme_t3B6022BA9CBD2C56BD12C781DB20C5B34B6C4E46::get_offset_of_icon_4(),
	Readme_t3B6022BA9CBD2C56BD12C781DB20C5B34B6C4E46::get_offset_of_title_5(),
	Readme_t3B6022BA9CBD2C56BD12C781DB20C5B34B6C4E46::get_offset_of_sections_6(),
	Readme_t3B6022BA9CBD2C56BD12C781DB20C5B34B6C4E46::get_offset_of_loadedLayout_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2247 = { sizeof (Section_tDCE8FED365051651AC2BFB27F2230DDC5603C985), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2247[4] = 
{
	Section_tDCE8FED365051651AC2BFB27F2230DDC5603C985::get_offset_of_heading_0(),
	Section_tDCE8FED365051651AC2BFB27F2230DDC5603C985::get_offset_of_text_1(),
	Section_tDCE8FED365051651AC2BFB27F2230DDC5603C985::get_offset_of_linkText_2(),
	Section_tDCE8FED365051651AC2BFB27F2230DDC5603C985::get_offset_of_url_3(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
