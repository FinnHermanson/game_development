import CommonLayers
import GameServer
import cocos
from cocos.scenes.transitions import JumpZoomTransition
import pyglet
import sys

from cocos.director import director
from cocos.audio.effect import Effect

class Game(object):
    """
    """
    
    default_title = "Dead of Night"
    default_window_width = 700
    default_window_height = 700

    def __init__(self):
        """ """
        super( Game, self ).__init__()

        director_width = Game.default_window_width
        director_height = Game.default_window_height

        caption = Game.default_title + ' ' + \
            CommonLayers.PlayLayer.ownID
        window = cocos.director.director.init(
            director_width, director_height,
            caption = caption, fullscreen=False)
        print(window.get_viewport_size())
        
        intro_layer = CommonLayers.PlayLayer()
        intro_layer.anchor_x = director_width * 0.5
        intro_layer.anchor_y = director_height * 0.5
        intro_layer.addZombies(5)

        intro_menu = IntroMenu(self)
        intro_layer.add(intro_menu)
        
        self.intro_scene = cocos.scene.Scene(intro_layer)
    
    def run(self, host=None, port=None):
        """ """
        self.host = host
        self.port = port
        cocos.director.director.set_show_FPS(True)
        cocos.director.director.run (self.intro_scene)
        director.init(audio_backend='sdl')
        effect = Effect('Game Assets/space_graveyard.wav')
        effect.play()

    def on_join_game( self ):
        """ """
        gameInstance = GameServer.GameServer()
        cocos.director.director.replace(JumpZoomTransition(
            gameInstance.get_scene(), 2))
        gameInstance.start()

    def on_quit( self ):
        """ """
        pyglet.app.exit()


class IntroMenu(cocos.menu.Menu):
    """
    """
    def __init__( self, game ):
        """ """
        super( IntroMenu, self ).__init__("DEAD of NIGHT")
        self.game = game
        self.font_item = {
            'font_name': 'Desdemona',
            'font_size': 32,
            'bold': True,
            'color': (220, 200, 0, 100),
        }
        self.font_item_selected = {
            'font_name': 'Desdemona',
            'font_size': 52,
            'bold': True,
            'color': (255, 255, 0, 255),
        }

        l = []
        l.append( cocos.menu.MenuItem('Play',
            self.game.on_join_game ) )
        l.append( cocos.menu.MenuItem('Quit', self.game.on_quit ) )
        
        

        self.create_menu( l )


if __name__ == "__main__":
    game = Game()
    if len(sys.argv) == 2:
        host, port = sys.argv[1].split(":")
        print(host, port)
        game.run(host, int(port))
    else:
        game.run()
        

